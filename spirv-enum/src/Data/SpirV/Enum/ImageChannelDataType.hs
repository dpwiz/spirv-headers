{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ImageChannelDataType where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype ImageChannelDataType = ImageChannelDataType Int32
  deriving newtype (Eq, Ord, Storable)

instance Show ImageChannelDataType where
  showsPrec p (ImageChannelDataType v) = case v of
    0 -> showString "SnormInt8"
    1 -> showString "SnormInt16"
    2 -> showString "UnormInt8"
    3 -> showString "UnormInt16"
    4 -> showString "UnormShort565"
    5 -> showString "UnormShort555"
    6 -> showString "UnormInt101010"
    7 -> showString "SignedInt8"
    8 -> showString "SignedInt16"
    9 -> showString "SignedInt32"
    10 -> showString "UnsignedInt8"
    11 -> showString "UnsignedInt16"
    12 -> showString "UnsignedInt32"
    13 -> showString "HalfFloat"
    14 -> showString "Float"
    15 -> showString "UnormInt24"
    16 -> showString "UnormInt101010_2"
    19 -> showString "UnsignedIntRaw10EXT"
    20 -> showString "UnsignedIntRaw12EXT"
    x -> showParen (p > 10) $ showString "ImageChannelDataType " . showsPrec (p + 1) x

pattern SnormInt8 :: ImageChannelDataType
pattern SnormInt8 = ImageChannelDataType 0

pattern SnormInt16 :: ImageChannelDataType
pattern SnormInt16 = ImageChannelDataType 1

pattern UnormInt8 :: ImageChannelDataType
pattern UnormInt8 = ImageChannelDataType 2

pattern UnormInt16 :: ImageChannelDataType
pattern UnormInt16 = ImageChannelDataType 3

pattern UnormShort565 :: ImageChannelDataType
pattern UnormShort565 = ImageChannelDataType 4

pattern UnormShort555 :: ImageChannelDataType
pattern UnormShort555 = ImageChannelDataType 5

pattern UnormInt101010 :: ImageChannelDataType
pattern UnormInt101010 = ImageChannelDataType 6

pattern SignedInt8 :: ImageChannelDataType
pattern SignedInt8 = ImageChannelDataType 7

pattern SignedInt16 :: ImageChannelDataType
pattern SignedInt16 = ImageChannelDataType 8

pattern SignedInt32 :: ImageChannelDataType
pattern SignedInt32 = ImageChannelDataType 9

pattern UnsignedInt8 :: ImageChannelDataType
pattern UnsignedInt8 = ImageChannelDataType 10

pattern UnsignedInt16 :: ImageChannelDataType
pattern UnsignedInt16 = ImageChannelDataType 11

pattern UnsignedInt32 :: ImageChannelDataType
pattern UnsignedInt32 = ImageChannelDataType 12

pattern HalfFloat :: ImageChannelDataType
pattern HalfFloat = ImageChannelDataType 13

pattern Float :: ImageChannelDataType
pattern Float = ImageChannelDataType 14

pattern UnormInt24 :: ImageChannelDataType
pattern UnormInt24 = ImageChannelDataType 15

pattern UnormInt101010_2 :: ImageChannelDataType
pattern UnormInt101010_2 = ImageChannelDataType 16

pattern UnsignedIntRaw10EXT :: ImageChannelDataType
pattern UnsignedIntRaw10EXT = ImageChannelDataType 19

pattern UnsignedIntRaw12EXT :: ImageChannelDataType
pattern UnsignedIntRaw12EXT = ImageChannelDataType 20