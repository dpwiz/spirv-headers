{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.GroupOperation where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype GroupOperation = GroupOperation Int32
  deriving newtype (Eq, Ord, Storable)

instance Show GroupOperation where
  showsPrec p (GroupOperation v) = case v of
    0 -> showString "Reduce"
    1 -> showString "InclusiveScan"
    2 -> showString "ExclusiveScan"
    3 -> showString "ClusteredReduce"
    6 -> showString "PartitionedReduceNV"
    7 -> showString "PartitionedInclusiveScanNV"
    8 -> showString "PartitionedExclusiveScanNV"
    x -> showParen (p > 10) $ showString "GroupOperation " . showsPrec (p + 1) x

pattern Reduce :: GroupOperation
pattern Reduce = GroupOperation 0

pattern InclusiveScan :: GroupOperation
pattern InclusiveScan = GroupOperation 1

pattern ExclusiveScan :: GroupOperation
pattern ExclusiveScan = GroupOperation 2

pattern ClusteredReduce :: GroupOperation
pattern ClusteredReduce = GroupOperation 3

pattern PartitionedReduceNV :: GroupOperation
pattern PartitionedReduceNV = GroupOperation 6

pattern PartitionedInclusiveScanNV :: GroupOperation
pattern PartitionedInclusiveScanNV = GroupOperation 7

pattern PartitionedExclusiveScanNV :: GroupOperation
pattern PartitionedExclusiveScanNV = GroupOperation 8