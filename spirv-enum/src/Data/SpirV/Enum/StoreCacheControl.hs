{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.StoreCacheControl where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype StoreCacheControl = StoreCacheControl Int32
  deriving newtype (Eq, Ord, Storable)

instance Show StoreCacheControl where
  showsPrec p (StoreCacheControl v) = case v of
    0 -> showString "UncachedINTEL"
    1 -> showString "WriteThroughINTEL"
    2 -> showString "WriteBackINTEL"
    3 -> showString "StreamingINTEL"
    x -> showParen (p > 10) $ showString "StoreCacheControl " . showsPrec (p + 1) x

pattern UncachedINTEL :: StoreCacheControl
pattern UncachedINTEL = StoreCacheControl 0

pattern WriteThroughINTEL :: StoreCacheControl
pattern WriteThroughINTEL = StoreCacheControl 1

pattern WriteBackINTEL :: StoreCacheControl
pattern WriteBackINTEL = StoreCacheControl 2

pattern StreamingINTEL :: StoreCacheControl
pattern StreamingINTEL = StoreCacheControl 3