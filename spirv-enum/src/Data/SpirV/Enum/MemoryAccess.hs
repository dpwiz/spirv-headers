{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.MemoryAccess where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type MemoryAccess = MemoryAccessBits

newtype MemoryAccessBits = MemoryAccessBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup MemoryAccess where
  (MemoryAccessBits a) <> (MemoryAccessBits b) = MemoryAccessBits (a .|. b)

instance Monoid MemoryAccess where
  mempty = MemoryAccessBits 0

pattern Volatile :: MemoryAccessBits
pattern Volatile = MemoryAccessBits 0x00000001

pattern Aligned :: MemoryAccessBits
pattern Aligned = MemoryAccessBits 0x00000002

pattern Nontemporal :: MemoryAccessBits
pattern Nontemporal = MemoryAccessBits 0x00000004

pattern MakePointerAvailable :: MemoryAccessBits
pattern MakePointerAvailable = MemoryAccessBits 0x00000008

pattern MakePointerAvailableKHR :: MemoryAccessBits
pattern MakePointerAvailableKHR = MemoryAccessBits 0x00000008

pattern MakePointerVisible :: MemoryAccessBits
pattern MakePointerVisible = MemoryAccessBits 0x00000010

pattern MakePointerVisibleKHR :: MemoryAccessBits
pattern MakePointerVisibleKHR = MemoryAccessBits 0x00000010

pattern NonPrivatePointer :: MemoryAccessBits
pattern NonPrivatePointer = MemoryAccessBits 0x00000020

pattern NonPrivatePointerKHR :: MemoryAccessBits
pattern NonPrivatePointerKHR = MemoryAccessBits 0x00000020

pattern AliasScopeINTELMask :: MemoryAccessBits
pattern AliasScopeINTELMask = MemoryAccessBits 0x00010000

pattern NoAliasINTELMask :: MemoryAccessBits
pattern NoAliasINTELMask = MemoryAccessBits 0x00020000