{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.QuantizationModes where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype QuantizationModes = QuantizationModes Int32
  deriving newtype (Eq, Ord, Storable)

instance Show QuantizationModes where
  showsPrec p (QuantizationModes v) = case v of
    0 -> showString "TRN"
    1 -> showString "TRN_ZERO"
    2 -> showString "RND"
    3 -> showString "RND_ZERO"
    4 -> showString "RND_INF"
    5 -> showString "RND_MIN_INF"
    6 -> showString "RND_CONV"
    7 -> showString "RND_CONV_ODD"
    x -> showParen (p > 10) $ showString "QuantizationModes " . showsPrec (p + 1) x

pattern TRN :: QuantizationModes
pattern TRN = QuantizationModes 0

pattern TRN_ZERO :: QuantizationModes
pattern TRN_ZERO = QuantizationModes 1

pattern RND :: QuantizationModes
pattern RND = QuantizationModes 2

pattern RND_ZERO :: QuantizationModes
pattern RND_ZERO = QuantizationModes 3

pattern RND_INF :: QuantizationModes
pattern RND_INF = QuantizationModes 4

pattern RND_MIN_INF :: QuantizationModes
pattern RND_MIN_INF = QuantizationModes 5

pattern RND_CONV :: QuantizationModes
pattern RND_CONV = QuantizationModes 6

pattern RND_CONV_ODD :: QuantizationModes
pattern RND_CONV_ODD = QuantizationModes 7