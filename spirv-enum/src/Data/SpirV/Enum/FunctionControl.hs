{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FunctionControl where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type FunctionControl = FunctionControlBits

newtype FunctionControlBits = FunctionControlBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup FunctionControl where
  (FunctionControlBits a) <> (FunctionControlBits b) = FunctionControlBits (a .|. b)

instance Monoid FunctionControl where
  mempty = FunctionControlBits 0

pattern Inline :: FunctionControlBits
pattern Inline = FunctionControlBits 0x00000001

pattern DontInline :: FunctionControlBits
pattern DontInline = FunctionControlBits 0x00000002

pattern Pure :: FunctionControlBits
pattern Pure = FunctionControlBits 0x00000004

pattern Const :: FunctionControlBits
pattern Const = FunctionControlBits 0x00000008

pattern OptNoneINTEL :: FunctionControlBits
pattern OptNoneINTEL = FunctionControlBits 0x00010000