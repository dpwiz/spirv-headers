{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.Op where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype Op = Op Int32
  deriving newtype (Eq, Ord, Storable)

instance Show Op where
  showsPrec p (Op v) = case v of
    0 -> showString "OpNop"
    1 -> showString "OpUndef"
    2 -> showString "OpSourceContinued"
    3 -> showString "OpSource"
    4 -> showString "OpSourceExtension"
    5 -> showString "OpName"
    6 -> showString "OpMemberName"
    7 -> showString "OpString"
    8 -> showString "OpLine"
    10 -> showString "OpExtension"
    11 -> showString "OpExtInstImport"
    12 -> showString "OpExtInst"
    14 -> showString "OpMemoryModel"
    15 -> showString "OpEntryPoint"
    16 -> showString "OpExecutionMode"
    17 -> showString "OpCapability"
    19 -> showString "OpTypeVoid"
    20 -> showString "OpTypeBool"
    21 -> showString "OpTypeInt"
    22 -> showString "OpTypeFloat"
    23 -> showString "OpTypeVector"
    24 -> showString "OpTypeMatrix"
    25 -> showString "OpTypeImage"
    26 -> showString "OpTypeSampler"
    27 -> showString "OpTypeSampledImage"
    28 -> showString "OpTypeArray"
    29 -> showString "OpTypeRuntimeArray"
    30 -> showString "OpTypeStruct"
    31 -> showString "OpTypeOpaque"
    32 -> showString "OpTypePointer"
    33 -> showString "OpTypeFunction"
    34 -> showString "OpTypeEvent"
    35 -> showString "OpTypeDeviceEvent"
    36 -> showString "OpTypeReserveId"
    37 -> showString "OpTypeQueue"
    38 -> showString "OpTypePipe"
    39 -> showString "OpTypeForwardPointer"
    41 -> showString "OpConstantTrue"
    42 -> showString "OpConstantFalse"
    43 -> showString "OpConstant"
    44 -> showString "OpConstantComposite"
    45 -> showString "OpConstantSampler"
    46 -> showString "OpConstantNull"
    48 -> showString "OpSpecConstantTrue"
    49 -> showString "OpSpecConstantFalse"
    50 -> showString "OpSpecConstant"
    51 -> showString "OpSpecConstantComposite"
    52 -> showString "OpSpecConstantOp"
    54 -> showString "OpFunction"
    55 -> showString "OpFunctionParameter"
    56 -> showString "OpFunctionEnd"
    57 -> showString "OpFunctionCall"
    59 -> showString "OpVariable"
    60 -> showString "OpImageTexelPointer"
    61 -> showString "OpLoad"
    62 -> showString "OpStore"
    63 -> showString "OpCopyMemory"
    64 -> showString "OpCopyMemorySized"
    65 -> showString "OpAccessChain"
    66 -> showString "OpInBoundsAccessChain"
    67 -> showString "OpPtrAccessChain"
    68 -> showString "OpArrayLength"
    69 -> showString "OpGenericPtrMemSemantics"
    70 -> showString "OpInBoundsPtrAccessChain"
    71 -> showString "OpDecorate"
    72 -> showString "OpMemberDecorate"
    73 -> showString "OpDecorationGroup"
    74 -> showString "OpGroupDecorate"
    75 -> showString "OpGroupMemberDecorate"
    77 -> showString "OpVectorExtractDynamic"
    78 -> showString "OpVectorInsertDynamic"
    79 -> showString "OpVectorShuffle"
    80 -> showString "OpCompositeConstruct"
    81 -> showString "OpCompositeExtract"
    82 -> showString "OpCompositeInsert"
    83 -> showString "OpCopyObject"
    84 -> showString "OpTranspose"
    86 -> showString "OpSampledImage"
    87 -> showString "OpImageSampleImplicitLod"
    88 -> showString "OpImageSampleExplicitLod"
    89 -> showString "OpImageSampleDrefImplicitLod"
    90 -> showString "OpImageSampleDrefExplicitLod"
    91 -> showString "OpImageSampleProjImplicitLod"
    92 -> showString "OpImageSampleProjExplicitLod"
    93 -> showString "OpImageSampleProjDrefImplicitLod"
    94 -> showString "OpImageSampleProjDrefExplicitLod"
    95 -> showString "OpImageFetch"
    96 -> showString "OpImageGather"
    97 -> showString "OpImageDrefGather"
    98 -> showString "OpImageRead"
    99 -> showString "OpImageWrite"
    100 -> showString "OpImage"
    101 -> showString "OpImageQueryFormat"
    102 -> showString "OpImageQueryOrder"
    103 -> showString "OpImageQuerySizeLod"
    104 -> showString "OpImageQuerySize"
    105 -> showString "OpImageQueryLod"
    106 -> showString "OpImageQueryLevels"
    107 -> showString "OpImageQuerySamples"
    109 -> showString "OpConvertFToU"
    110 -> showString "OpConvertFToS"
    111 -> showString "OpConvertSToF"
    112 -> showString "OpConvertUToF"
    113 -> showString "OpUConvert"
    114 -> showString "OpSConvert"
    115 -> showString "OpFConvert"
    116 -> showString "OpQuantizeToF16"
    117 -> showString "OpConvertPtrToU"
    118 -> showString "OpSatConvertSToU"
    119 -> showString "OpSatConvertUToS"
    120 -> showString "OpConvertUToPtr"
    121 -> showString "OpPtrCastToGeneric"
    122 -> showString "OpGenericCastToPtr"
    123 -> showString "OpGenericCastToPtrExplicit"
    124 -> showString "OpBitcast"
    126 -> showString "OpSNegate"
    127 -> showString "OpFNegate"
    128 -> showString "OpIAdd"
    129 -> showString "OpFAdd"
    130 -> showString "OpISub"
    131 -> showString "OpFSub"
    132 -> showString "OpIMul"
    133 -> showString "OpFMul"
    134 -> showString "OpUDiv"
    135 -> showString "OpSDiv"
    136 -> showString "OpFDiv"
    137 -> showString "OpUMod"
    138 -> showString "OpSRem"
    139 -> showString "OpSMod"
    140 -> showString "OpFRem"
    141 -> showString "OpFMod"
    142 -> showString "OpVectorTimesScalar"
    143 -> showString "OpMatrixTimesScalar"
    144 -> showString "OpVectorTimesMatrix"
    145 -> showString "OpMatrixTimesVector"
    146 -> showString "OpMatrixTimesMatrix"
    147 -> showString "OpOuterProduct"
    148 -> showString "OpDot"
    149 -> showString "OpIAddCarry"
    150 -> showString "OpISubBorrow"
    151 -> showString "OpUMulExtended"
    152 -> showString "OpSMulExtended"
    154 -> showString "OpAny"
    155 -> showString "OpAll"
    156 -> showString "OpIsNan"
    157 -> showString "OpIsInf"
    158 -> showString "OpIsFinite"
    159 -> showString "OpIsNormal"
    160 -> showString "OpSignBitSet"
    161 -> showString "OpLessOrGreater"
    162 -> showString "OpOrdered"
    163 -> showString "OpUnordered"
    164 -> showString "OpLogicalEqual"
    165 -> showString "OpLogicalNotEqual"
    166 -> showString "OpLogicalOr"
    167 -> showString "OpLogicalAnd"
    168 -> showString "OpLogicalNot"
    169 -> showString "OpSelect"
    170 -> showString "OpIEqual"
    171 -> showString "OpINotEqual"
    172 -> showString "OpUGreaterThan"
    173 -> showString "OpSGreaterThan"
    174 -> showString "OpUGreaterThanEqual"
    175 -> showString "OpSGreaterThanEqual"
    176 -> showString "OpULessThan"
    177 -> showString "OpSLessThan"
    178 -> showString "OpULessThanEqual"
    179 -> showString "OpSLessThanEqual"
    180 -> showString "OpFOrdEqual"
    181 -> showString "OpFUnordEqual"
    182 -> showString "OpFOrdNotEqual"
    183 -> showString "OpFUnordNotEqual"
    184 -> showString "OpFOrdLessThan"
    185 -> showString "OpFUnordLessThan"
    186 -> showString "OpFOrdGreaterThan"
    187 -> showString "OpFUnordGreaterThan"
    188 -> showString "OpFOrdLessThanEqual"
    189 -> showString "OpFUnordLessThanEqual"
    190 -> showString "OpFOrdGreaterThanEqual"
    191 -> showString "OpFUnordGreaterThanEqual"
    194 -> showString "OpShiftRightLogical"
    195 -> showString "OpShiftRightArithmetic"
    196 -> showString "OpShiftLeftLogical"
    197 -> showString "OpBitwiseOr"
    198 -> showString "OpBitwiseXor"
    199 -> showString "OpBitwiseAnd"
    200 -> showString "OpNot"
    201 -> showString "OpBitFieldInsert"
    202 -> showString "OpBitFieldSExtract"
    203 -> showString "OpBitFieldUExtract"
    204 -> showString "OpBitReverse"
    205 -> showString "OpBitCount"
    207 -> showString "OpDPdx"
    208 -> showString "OpDPdy"
    209 -> showString "OpFwidth"
    210 -> showString "OpDPdxFine"
    211 -> showString "OpDPdyFine"
    212 -> showString "OpFwidthFine"
    213 -> showString "OpDPdxCoarse"
    214 -> showString "OpDPdyCoarse"
    215 -> showString "OpFwidthCoarse"
    218 -> showString "OpEmitVertex"
    219 -> showString "OpEndPrimitive"
    220 -> showString "OpEmitStreamVertex"
    221 -> showString "OpEndStreamPrimitive"
    224 -> showString "OpControlBarrier"
    225 -> showString "OpMemoryBarrier"
    227 -> showString "OpAtomicLoad"
    228 -> showString "OpAtomicStore"
    229 -> showString "OpAtomicExchange"
    230 -> showString "OpAtomicCompareExchange"
    231 -> showString "OpAtomicCompareExchangeWeak"
    232 -> showString "OpAtomicIIncrement"
    233 -> showString "OpAtomicIDecrement"
    234 -> showString "OpAtomicIAdd"
    235 -> showString "OpAtomicISub"
    236 -> showString "OpAtomicSMin"
    237 -> showString "OpAtomicUMin"
    238 -> showString "OpAtomicSMax"
    239 -> showString "OpAtomicUMax"
    240 -> showString "OpAtomicAnd"
    241 -> showString "OpAtomicOr"
    242 -> showString "OpAtomicXor"
    245 -> showString "OpPhi"
    246 -> showString "OpLoopMerge"
    247 -> showString "OpSelectionMerge"
    248 -> showString "OpLabel"
    249 -> showString "OpBranch"
    250 -> showString "OpBranchConditional"
    251 -> showString "OpSwitch"
    252 -> showString "OpKill"
    253 -> showString "OpReturn"
    254 -> showString "OpReturnValue"
    255 -> showString "OpUnreachable"
    256 -> showString "OpLifetimeStart"
    257 -> showString "OpLifetimeStop"
    259 -> showString "OpGroupAsyncCopy"
    260 -> showString "OpGroupWaitEvents"
    261 -> showString "OpGroupAll"
    262 -> showString "OpGroupAny"
    263 -> showString "OpGroupBroadcast"
    264 -> showString "OpGroupIAdd"
    265 -> showString "OpGroupFAdd"
    266 -> showString "OpGroupFMin"
    267 -> showString "OpGroupUMin"
    268 -> showString "OpGroupSMin"
    269 -> showString "OpGroupFMax"
    270 -> showString "OpGroupUMax"
    271 -> showString "OpGroupSMax"
    274 -> showString "OpReadPipe"
    275 -> showString "OpWritePipe"
    276 -> showString "OpReservedReadPipe"
    277 -> showString "OpReservedWritePipe"
    278 -> showString "OpReserveReadPipePackets"
    279 -> showString "OpReserveWritePipePackets"
    280 -> showString "OpCommitReadPipe"
    281 -> showString "OpCommitWritePipe"
    282 -> showString "OpIsValidReserveId"
    283 -> showString "OpGetNumPipePackets"
    284 -> showString "OpGetMaxPipePackets"
    285 -> showString "OpGroupReserveReadPipePackets"
    286 -> showString "OpGroupReserveWritePipePackets"
    287 -> showString "OpGroupCommitReadPipe"
    288 -> showString "OpGroupCommitWritePipe"
    291 -> showString "OpEnqueueMarker"
    292 -> showString "OpEnqueueKernel"
    293 -> showString "OpGetKernelNDrangeSubGroupCount"
    294 -> showString "OpGetKernelNDrangeMaxSubGroupSize"
    295 -> showString "OpGetKernelWorkGroupSize"
    296 -> showString "OpGetKernelPreferredWorkGroupSizeMultiple"
    297 -> showString "OpRetainEvent"
    298 -> showString "OpReleaseEvent"
    299 -> showString "OpCreateUserEvent"
    300 -> showString "OpIsValidEvent"
    301 -> showString "OpSetUserEventStatus"
    302 -> showString "OpCaptureEventProfilingInfo"
    303 -> showString "OpGetDefaultQueue"
    304 -> showString "OpBuildNDRange"
    305 -> showString "OpImageSparseSampleImplicitLod"
    306 -> showString "OpImageSparseSampleExplicitLod"
    307 -> showString "OpImageSparseSampleDrefImplicitLod"
    308 -> showString "OpImageSparseSampleDrefExplicitLod"
    309 -> showString "OpImageSparseSampleProjImplicitLod"
    310 -> showString "OpImageSparseSampleProjExplicitLod"
    311 -> showString "OpImageSparseSampleProjDrefImplicitLod"
    312 -> showString "OpImageSparseSampleProjDrefExplicitLod"
    313 -> showString "OpImageSparseFetch"
    314 -> showString "OpImageSparseGather"
    315 -> showString "OpImageSparseDrefGather"
    316 -> showString "OpImageSparseTexelsResident"
    317 -> showString "OpNoLine"
    318 -> showString "OpAtomicFlagTestAndSet"
    319 -> showString "OpAtomicFlagClear"
    320 -> showString "OpImageSparseRead"
    321 -> showString "OpSizeOf"
    322 -> showString "OpTypePipeStorage"
    323 -> showString "OpConstantPipeStorage"
    324 -> showString "OpCreatePipeFromPipeStorage"
    325 -> showString "OpGetKernelLocalSizeForSubgroupCount"
    326 -> showString "OpGetKernelMaxNumSubgroups"
    327 -> showString "OpTypeNamedBarrier"
    328 -> showString "OpNamedBarrierInitialize"
    329 -> showString "OpMemoryNamedBarrier"
    330 -> showString "OpModuleProcessed"
    331 -> showString "OpExecutionModeId"
    332 -> showString "OpDecorateId"
    333 -> showString "OpGroupNonUniformElect"
    334 -> showString "OpGroupNonUniformAll"
    335 -> showString "OpGroupNonUniformAny"
    336 -> showString "OpGroupNonUniformAllEqual"
    337 -> showString "OpGroupNonUniformBroadcast"
    338 -> showString "OpGroupNonUniformBroadcastFirst"
    339 -> showString "OpGroupNonUniformBallot"
    340 -> showString "OpGroupNonUniformInverseBallot"
    341 -> showString "OpGroupNonUniformBallotBitExtract"
    342 -> showString "OpGroupNonUniformBallotBitCount"
    343 -> showString "OpGroupNonUniformBallotFindLSB"
    344 -> showString "OpGroupNonUniformBallotFindMSB"
    345 -> showString "OpGroupNonUniformShuffle"
    346 -> showString "OpGroupNonUniformShuffleXor"
    347 -> showString "OpGroupNonUniformShuffleUp"
    348 -> showString "OpGroupNonUniformShuffleDown"
    349 -> showString "OpGroupNonUniformIAdd"
    350 -> showString "OpGroupNonUniformFAdd"
    351 -> showString "OpGroupNonUniformIMul"
    352 -> showString "OpGroupNonUniformFMul"
    353 -> showString "OpGroupNonUniformSMin"
    354 -> showString "OpGroupNonUniformUMin"
    355 -> showString "OpGroupNonUniformFMin"
    356 -> showString "OpGroupNonUniformSMax"
    357 -> showString "OpGroupNonUniformUMax"
    358 -> showString "OpGroupNonUniformFMax"
    359 -> showString "OpGroupNonUniformBitwiseAnd"
    360 -> showString "OpGroupNonUniformBitwiseOr"
    361 -> showString "OpGroupNonUniformBitwiseXor"
    362 -> showString "OpGroupNonUniformLogicalAnd"
    363 -> showString "OpGroupNonUniformLogicalOr"
    364 -> showString "OpGroupNonUniformLogicalXor"
    365 -> showString "OpGroupNonUniformQuadBroadcast"
    366 -> showString "OpGroupNonUniformQuadSwap"
    400 -> showString "OpCopyLogical"
    401 -> showString "OpPtrEqual"
    402 -> showString "OpPtrNotEqual"
    403 -> showString "OpPtrDiff"
    4160 -> showString "OpColorAttachmentReadEXT"
    4161 -> showString "OpDepthAttachmentReadEXT"
    4162 -> showString "OpStencilAttachmentReadEXT"
    4416 -> showString "OpTerminateInvocation"
    4421 -> showString "OpSubgroupBallotKHR"
    4422 -> showString "OpSubgroupFirstInvocationKHR"
    4428 -> showString "OpSubgroupAllKHR"
    4429 -> showString "OpSubgroupAnyKHR"
    4430 -> showString "OpSubgroupAllEqualKHR"
    4431 -> showString "OpGroupNonUniformRotateKHR"
    4432 -> showString "OpSubgroupReadInvocationKHR"
    4433 -> showString "OpExtInstWithForwardRefsKHR"
    4445 -> showString "OpTraceRayKHR"
    4446 -> showString "OpExecuteCallableKHR"
    4447 -> showString "OpConvertUToAccelerationStructureKHR"
    4448 -> showString "OpIgnoreIntersectionKHR"
    4449 -> showString "OpTerminateRayKHR"
    4450 -> showString "OpSDot"
    4451 -> showString "OpUDot"
    4452 -> showString "OpSUDot"
    4453 -> showString "OpSDotAccSat"
    4454 -> showString "OpUDotAccSat"
    4455 -> showString "OpSUDotAccSat"
    4456 -> showString "OpTypeCooperativeMatrixKHR"
    4457 -> showString "OpCooperativeMatrixLoadKHR"
    4458 -> showString "OpCooperativeMatrixStoreKHR"
    4459 -> showString "OpCooperativeMatrixMulAddKHR"
    4460 -> showString "OpCooperativeMatrixLengthKHR"
    4461 -> showString "OpConstantCompositeReplicateEXT"
    4462 -> showString "OpSpecConstantCompositeReplicateEXT"
    4463 -> showString "OpCompositeConstructReplicateEXT"
    4472 -> showString "OpTypeRayQueryKHR"
    4473 -> showString "OpRayQueryInitializeKHR"
    4474 -> showString "OpRayQueryTerminateKHR"
    4475 -> showString "OpRayQueryGenerateIntersectionKHR"
    4476 -> showString "OpRayQueryConfirmIntersectionKHR"
    4477 -> showString "OpRayQueryProceedKHR"
    4479 -> showString "OpRayQueryGetIntersectionTypeKHR"
    4480 -> showString "OpImageSampleWeightedQCOM"
    4481 -> showString "OpImageBoxFilterQCOM"
    4482 -> showString "OpImageBlockMatchSSDQCOM"
    4483 -> showString "OpImageBlockMatchSADQCOM"
    4500 -> showString "OpImageBlockMatchWindowSSDQCOM"
    4501 -> showString "OpImageBlockMatchWindowSADQCOM"
    4502 -> showString "OpImageBlockMatchGatherSSDQCOM"
    4503 -> showString "OpImageBlockMatchGatherSADQCOM"
    5000 -> showString "OpGroupIAddNonUniformAMD"
    5001 -> showString "OpGroupFAddNonUniformAMD"
    5002 -> showString "OpGroupFMinNonUniformAMD"
    5003 -> showString "OpGroupUMinNonUniformAMD"
    5004 -> showString "OpGroupSMinNonUniformAMD"
    5005 -> showString "OpGroupFMaxNonUniformAMD"
    5006 -> showString "OpGroupUMaxNonUniformAMD"
    5007 -> showString "OpGroupSMaxNonUniformAMD"
    5011 -> showString "OpFragmentMaskFetchAMD"
    5012 -> showString "OpFragmentFetchAMD"
    5056 -> showString "OpReadClockKHR"
    5075 -> showString "OpFinalizeNodePayloadsAMDX"
    5078 -> showString "OpFinishWritingNodePayloadAMDX"
    5090 -> showString "OpInitializeNodePayloadsAMDX"
    5110 -> showString "OpGroupNonUniformQuadAllKHR"
    5111 -> showString "OpGroupNonUniformQuadAnyKHR"
    5249 -> showString "OpHitObjectRecordHitMotionNV"
    5250 -> showString "OpHitObjectRecordHitWithIndexMotionNV"
    5251 -> showString "OpHitObjectRecordMissMotionNV"
    5252 -> showString "OpHitObjectGetWorldToObjectNV"
    5253 -> showString "OpHitObjectGetObjectToWorldNV"
    5254 -> showString "OpHitObjectGetObjectRayDirectionNV"
    5255 -> showString "OpHitObjectGetObjectRayOriginNV"
    5256 -> showString "OpHitObjectTraceRayMotionNV"
    5257 -> showString "OpHitObjectGetShaderRecordBufferHandleNV"
    5258 -> showString "OpHitObjectGetShaderBindingTableRecordIndexNV"
    5259 -> showString "OpHitObjectRecordEmptyNV"
    5260 -> showString "OpHitObjectTraceRayNV"
    5261 -> showString "OpHitObjectRecordHitNV"
    5262 -> showString "OpHitObjectRecordHitWithIndexNV"
    5263 -> showString "OpHitObjectRecordMissNV"
    5264 -> showString "OpHitObjectExecuteShaderNV"
    5265 -> showString "OpHitObjectGetCurrentTimeNV"
    5266 -> showString "OpHitObjectGetAttributesNV"
    5267 -> showString "OpHitObjectGetHitKindNV"
    5268 -> showString "OpHitObjectGetPrimitiveIndexNV"
    5269 -> showString "OpHitObjectGetGeometryIndexNV"
    5270 -> showString "OpHitObjectGetInstanceIdNV"
    5271 -> showString "OpHitObjectGetInstanceCustomIndexNV"
    5272 -> showString "OpHitObjectGetWorldRayDirectionNV"
    5273 -> showString "OpHitObjectGetWorldRayOriginNV"
    5274 -> showString "OpHitObjectGetRayTMaxNV"
    5275 -> showString "OpHitObjectGetRayTMinNV"
    5276 -> showString "OpHitObjectIsEmptyNV"
    5277 -> showString "OpHitObjectIsHitNV"
    5278 -> showString "OpHitObjectIsMissNV"
    5279 -> showString "OpReorderThreadWithHitObjectNV"
    5280 -> showString "OpReorderThreadWithHintNV"
    5281 -> showString "OpTypeHitObjectNV"
    5283 -> showString "OpImageSampleFootprintNV"
    5294 -> showString "OpEmitMeshTasksEXT"
    5295 -> showString "OpSetMeshOutputsEXT"
    5296 -> showString "OpGroupNonUniformPartitionNV"
    5299 -> showString "OpWritePackedPrimitiveIndices4x8NV"
    5300 -> showString "OpFetchMicroTriangleVertexPositionNV"
    5301 -> showString "OpFetchMicroTriangleVertexBarycentricNV"
    5334 -> showString "OpReportIntersectionKHR"
    5335 -> showString "OpIgnoreIntersectionNV"
    5336 -> showString "OpTerminateRayNV"
    5337 -> showString "OpTraceNV"
    5338 -> showString "OpTraceMotionNV"
    5339 -> showString "OpTraceRayMotionNV"
    5340 -> showString "OpRayQueryGetIntersectionTriangleVertexPositionsKHR"
    5341 -> showString "OpTypeAccelerationStructureKHR"
    5344 -> showString "OpExecuteCallableNV"
    5358 -> showString "OpTypeCooperativeMatrixNV"
    5359 -> showString "OpCooperativeMatrixLoadNV"
    5360 -> showString "OpCooperativeMatrixStoreNV"
    5361 -> showString "OpCooperativeMatrixMulAddNV"
    5362 -> showString "OpCooperativeMatrixLengthNV"
    5364 -> showString "OpBeginInvocationInterlockEXT"
    5365 -> showString "OpEndInvocationInterlockEXT"
    5380 -> showString "OpDemoteToHelperInvocation"
    5381 -> showString "OpIsHelperInvocationEXT"
    5391 -> showString "OpConvertUToImageNV"
    5392 -> showString "OpConvertUToSamplerNV"
    5393 -> showString "OpConvertImageToUNV"
    5394 -> showString "OpConvertSamplerToUNV"
    5395 -> showString "OpConvertUToSampledImageNV"
    5396 -> showString "OpConvertSampledImageToUNV"
    5397 -> showString "OpSamplerImageAddressingModeNV"
    5398 -> showString "OpRawAccessChainNV"
    5571 -> showString "OpSubgroupShuffleINTEL"
    5572 -> showString "OpSubgroupShuffleDownINTEL"
    5573 -> showString "OpSubgroupShuffleUpINTEL"
    5574 -> showString "OpSubgroupShuffleXorINTEL"
    5575 -> showString "OpSubgroupBlockReadINTEL"
    5576 -> showString "OpSubgroupBlockWriteINTEL"
    5577 -> showString "OpSubgroupImageBlockReadINTEL"
    5578 -> showString "OpSubgroupImageBlockWriteINTEL"
    5580 -> showString "OpSubgroupImageMediaBlockReadINTEL"
    5581 -> showString "OpSubgroupImageMediaBlockWriteINTEL"
    5585 -> showString "OpUCountLeadingZerosINTEL"
    5586 -> showString "OpUCountTrailingZerosINTEL"
    5587 -> showString "OpAbsISubINTEL"
    5588 -> showString "OpAbsUSubINTEL"
    5589 -> showString "OpIAddSatINTEL"
    5590 -> showString "OpUAddSatINTEL"
    5591 -> showString "OpIAverageINTEL"
    5592 -> showString "OpUAverageINTEL"
    5593 -> showString "OpIAverageRoundedINTEL"
    5594 -> showString "OpUAverageRoundedINTEL"
    5595 -> showString "OpISubSatINTEL"
    5596 -> showString "OpUSubSatINTEL"
    5597 -> showString "OpIMul32x16INTEL"
    5598 -> showString "OpUMul32x16INTEL"
    5600 -> showString "OpConstantFunctionPointerINTEL"
    5601 -> showString "OpFunctionPointerCallINTEL"
    5609 -> showString "OpAsmTargetINTEL"
    5610 -> showString "OpAsmINTEL"
    5611 -> showString "OpAsmCallINTEL"
    5614 -> showString "OpAtomicFMinEXT"
    5615 -> showString "OpAtomicFMaxEXT"
    5630 -> showString "OpAssumeTrueKHR"
    5631 -> showString "OpExpectKHR"
    5632 -> showString "OpDecorateString"
    5633 -> showString "OpMemberDecorateString"
    5699 -> showString "OpVmeImageINTEL"
    5700 -> showString "OpTypeVmeImageINTEL"
    5701 -> showString "OpTypeAvcImePayloadINTEL"
    5702 -> showString "OpTypeAvcRefPayloadINTEL"
    5703 -> showString "OpTypeAvcSicPayloadINTEL"
    5704 -> showString "OpTypeAvcMcePayloadINTEL"
    5705 -> showString "OpTypeAvcMceResultINTEL"
    5706 -> showString "OpTypeAvcImeResultINTEL"
    5707 -> showString "OpTypeAvcImeResultSingleReferenceStreamoutINTEL"
    5708 -> showString "OpTypeAvcImeResultDualReferenceStreamoutINTEL"
    5709 -> showString "OpTypeAvcImeSingleReferenceStreaminINTEL"
    5710 -> showString "OpTypeAvcImeDualReferenceStreaminINTEL"
    5711 -> showString "OpTypeAvcRefResultINTEL"
    5712 -> showString "OpTypeAvcSicResultINTEL"
    5713 -> showString "OpSubgroupAvcMceGetDefaultInterBaseMultiReferencePenaltyINTEL"
    5714 -> showString "OpSubgroupAvcMceSetInterBaseMultiReferencePenaltyINTEL"
    5715 -> showString "OpSubgroupAvcMceGetDefaultInterShapePenaltyINTEL"
    5716 -> showString "OpSubgroupAvcMceSetInterShapePenaltyINTEL"
    5717 -> showString "OpSubgroupAvcMceGetDefaultInterDirectionPenaltyINTEL"
    5718 -> showString "OpSubgroupAvcMceSetInterDirectionPenaltyINTEL"
    5719 -> showString "OpSubgroupAvcMceGetDefaultIntraLumaShapePenaltyINTEL"
    5720 -> showString "OpSubgroupAvcMceGetDefaultInterMotionVectorCostTableINTEL"
    5721 -> showString "OpSubgroupAvcMceGetDefaultHighPenaltyCostTableINTEL"
    5722 -> showString "OpSubgroupAvcMceGetDefaultMediumPenaltyCostTableINTEL"
    5723 -> showString "OpSubgroupAvcMceGetDefaultLowPenaltyCostTableINTEL"
    5724 -> showString "OpSubgroupAvcMceSetMotionVectorCostFunctionINTEL"
    5725 -> showString "OpSubgroupAvcMceGetDefaultIntraLumaModePenaltyINTEL"
    5726 -> showString "OpSubgroupAvcMceGetDefaultNonDcLumaIntraPenaltyINTEL"
    5727 -> showString "OpSubgroupAvcMceGetDefaultIntraChromaModeBasePenaltyINTEL"
    5728 -> showString "OpSubgroupAvcMceSetAcOnlyHaarINTEL"
    5729 -> showString "OpSubgroupAvcMceSetSourceInterlacedFieldPolarityINTEL"
    5730 -> showString "OpSubgroupAvcMceSetSingleReferenceInterlacedFieldPolarityINTEL"
    5731 -> showString "OpSubgroupAvcMceSetDualReferenceInterlacedFieldPolaritiesINTEL"
    5732 -> showString "OpSubgroupAvcMceConvertToImePayloadINTEL"
    5733 -> showString "OpSubgroupAvcMceConvertToImeResultINTEL"
    5734 -> showString "OpSubgroupAvcMceConvertToRefPayloadINTEL"
    5735 -> showString "OpSubgroupAvcMceConvertToRefResultINTEL"
    5736 -> showString "OpSubgroupAvcMceConvertToSicPayloadINTEL"
    5737 -> showString "OpSubgroupAvcMceConvertToSicResultINTEL"
    5738 -> showString "OpSubgroupAvcMceGetMotionVectorsINTEL"
    5739 -> showString "OpSubgroupAvcMceGetInterDistortionsINTEL"
    5740 -> showString "OpSubgroupAvcMceGetBestInterDistortionsINTEL"
    5741 -> showString "OpSubgroupAvcMceGetInterMajorShapeINTEL"
    5742 -> showString "OpSubgroupAvcMceGetInterMinorShapeINTEL"
    5743 -> showString "OpSubgroupAvcMceGetInterDirectionsINTEL"
    5744 -> showString "OpSubgroupAvcMceGetInterMotionVectorCountINTEL"
    5745 -> showString "OpSubgroupAvcMceGetInterReferenceIdsINTEL"
    5746 -> showString "OpSubgroupAvcMceGetInterReferenceInterlacedFieldPolaritiesINTEL"
    5747 -> showString "OpSubgroupAvcImeInitializeINTEL"
    5748 -> showString "OpSubgroupAvcImeSetSingleReferenceINTEL"
    5749 -> showString "OpSubgroupAvcImeSetDualReferenceINTEL"
    5750 -> showString "OpSubgroupAvcImeRefWindowSizeINTEL"
    5751 -> showString "OpSubgroupAvcImeAdjustRefOffsetINTEL"
    5752 -> showString "OpSubgroupAvcImeConvertToMcePayloadINTEL"
    5753 -> showString "OpSubgroupAvcImeSetMaxMotionVectorCountINTEL"
    5754 -> showString "OpSubgroupAvcImeSetUnidirectionalMixDisableINTEL"
    5755 -> showString "OpSubgroupAvcImeSetEarlySearchTerminationThresholdINTEL"
    5756 -> showString "OpSubgroupAvcImeSetWeightedSadINTEL"
    5757 -> showString "OpSubgroupAvcImeEvaluateWithSingleReferenceINTEL"
    5758 -> showString "OpSubgroupAvcImeEvaluateWithDualReferenceINTEL"
    5759 -> showString "OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminINTEL"
    5760 -> showString "OpSubgroupAvcImeEvaluateWithDualReferenceStreaminINTEL"
    5761 -> showString "OpSubgroupAvcImeEvaluateWithSingleReferenceStreamoutINTEL"
    5762 -> showString "OpSubgroupAvcImeEvaluateWithDualReferenceStreamoutINTEL"
    5763 -> showString "OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminoutINTEL"
    5764 -> showString "OpSubgroupAvcImeEvaluateWithDualReferenceStreaminoutINTEL"
    5765 -> showString "OpSubgroupAvcImeConvertToMceResultINTEL"
    5766 -> showString "OpSubgroupAvcImeGetSingleReferenceStreaminINTEL"
    5767 -> showString "OpSubgroupAvcImeGetDualReferenceStreaminINTEL"
    5768 -> showString "OpSubgroupAvcImeStripSingleReferenceStreamoutINTEL"
    5769 -> showString "OpSubgroupAvcImeStripDualReferenceStreamoutINTEL"
    5770 -> showString "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeMotionVectorsINTEL"
    5771 -> showString "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeDistortionsINTEL"
    5772 -> showString "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeReferenceIdsINTEL"
    5773 -> showString "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeMotionVectorsINTEL"
    5774 -> showString "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeDistortionsINTEL"
    5775 -> showString "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeReferenceIdsINTEL"
    5776 -> showString "OpSubgroupAvcImeGetBorderReachedINTEL"
    5777 -> showString "OpSubgroupAvcImeGetTruncatedSearchIndicationINTEL"
    5778 -> showString "OpSubgroupAvcImeGetUnidirectionalEarlySearchTerminationINTEL"
    5779 -> showString "OpSubgroupAvcImeGetWeightingPatternMinimumMotionVectorINTEL"
    5780 -> showString "OpSubgroupAvcImeGetWeightingPatternMinimumDistortionINTEL"
    5781 -> showString "OpSubgroupAvcFmeInitializeINTEL"
    5782 -> showString "OpSubgroupAvcBmeInitializeINTEL"
    5783 -> showString "OpSubgroupAvcRefConvertToMcePayloadINTEL"
    5784 -> showString "OpSubgroupAvcRefSetBidirectionalMixDisableINTEL"
    5785 -> showString "OpSubgroupAvcRefSetBilinearFilterEnableINTEL"
    5786 -> showString "OpSubgroupAvcRefEvaluateWithSingleReferenceINTEL"
    5787 -> showString "OpSubgroupAvcRefEvaluateWithDualReferenceINTEL"
    5788 -> showString "OpSubgroupAvcRefEvaluateWithMultiReferenceINTEL"
    5789 -> showString "OpSubgroupAvcRefEvaluateWithMultiReferenceInterlacedINTEL"
    5790 -> showString "OpSubgroupAvcRefConvertToMceResultINTEL"
    5791 -> showString "OpSubgroupAvcSicInitializeINTEL"
    5792 -> showString "OpSubgroupAvcSicConfigureSkcINTEL"
    5793 -> showString "OpSubgroupAvcSicConfigureIpeLumaINTEL"
    5794 -> showString "OpSubgroupAvcSicConfigureIpeLumaChromaINTEL"
    5795 -> showString "OpSubgroupAvcSicGetMotionVectorMaskINTEL"
    5796 -> showString "OpSubgroupAvcSicConvertToMcePayloadINTEL"
    5797 -> showString "OpSubgroupAvcSicSetIntraLumaShapePenaltyINTEL"
    5798 -> showString "OpSubgroupAvcSicSetIntraLumaModeCostFunctionINTEL"
    5799 -> showString "OpSubgroupAvcSicSetIntraChromaModeCostFunctionINTEL"
    5800 -> showString "OpSubgroupAvcSicSetBilinearFilterEnableINTEL"
    5801 -> showString "OpSubgroupAvcSicSetSkcForwardTransformEnableINTEL"
    5802 -> showString "OpSubgroupAvcSicSetBlockBasedRawSkipSadINTEL"
    5803 -> showString "OpSubgroupAvcSicEvaluateIpeINTEL"
    5804 -> showString "OpSubgroupAvcSicEvaluateWithSingleReferenceINTEL"
    5805 -> showString "OpSubgroupAvcSicEvaluateWithDualReferenceINTEL"
    5806 -> showString "OpSubgroupAvcSicEvaluateWithMultiReferenceINTEL"
    5807 -> showString "OpSubgroupAvcSicEvaluateWithMultiReferenceInterlacedINTEL"
    5808 -> showString "OpSubgroupAvcSicConvertToMceResultINTEL"
    5809 -> showString "OpSubgroupAvcSicGetIpeLumaShapeINTEL"
    5810 -> showString "OpSubgroupAvcSicGetBestIpeLumaDistortionINTEL"
    5811 -> showString "OpSubgroupAvcSicGetBestIpeChromaDistortionINTEL"
    5812 -> showString "OpSubgroupAvcSicGetPackedIpeLumaModesINTEL"
    5813 -> showString "OpSubgroupAvcSicGetIpeChromaModeINTEL"
    5814 -> showString "OpSubgroupAvcSicGetPackedSkcLumaCountThresholdINTEL"
    5815 -> showString "OpSubgroupAvcSicGetPackedSkcLumaSumThresholdINTEL"
    5816 -> showString "OpSubgroupAvcSicGetInterRawSadsINTEL"
    5818 -> showString "OpVariableLengthArrayINTEL"
    5819 -> showString "OpSaveMemoryINTEL"
    5820 -> showString "OpRestoreMemoryINTEL"
    5840 -> showString "OpArbitraryFloatSinCosPiINTEL"
    5841 -> showString "OpArbitraryFloatCastINTEL"
    5842 -> showString "OpArbitraryFloatCastFromIntINTEL"
    5843 -> showString "OpArbitraryFloatCastToIntINTEL"
    5846 -> showString "OpArbitraryFloatAddINTEL"
    5847 -> showString "OpArbitraryFloatSubINTEL"
    5848 -> showString "OpArbitraryFloatMulINTEL"
    5849 -> showString "OpArbitraryFloatDivINTEL"
    5850 -> showString "OpArbitraryFloatGTINTEL"
    5851 -> showString "OpArbitraryFloatGEINTEL"
    5852 -> showString "OpArbitraryFloatLTINTEL"
    5853 -> showString "OpArbitraryFloatLEINTEL"
    5854 -> showString "OpArbitraryFloatEQINTEL"
    5855 -> showString "OpArbitraryFloatRecipINTEL"
    5856 -> showString "OpArbitraryFloatRSqrtINTEL"
    5857 -> showString "OpArbitraryFloatCbrtINTEL"
    5858 -> showString "OpArbitraryFloatHypotINTEL"
    5859 -> showString "OpArbitraryFloatSqrtINTEL"
    5860 -> showString "OpArbitraryFloatLogINTEL"
    5861 -> showString "OpArbitraryFloatLog2INTEL"
    5862 -> showString "OpArbitraryFloatLog10INTEL"
    5863 -> showString "OpArbitraryFloatLog1pINTEL"
    5864 -> showString "OpArbitraryFloatExpINTEL"
    5865 -> showString "OpArbitraryFloatExp2INTEL"
    5866 -> showString "OpArbitraryFloatExp10INTEL"
    5867 -> showString "OpArbitraryFloatExpm1INTEL"
    5868 -> showString "OpArbitraryFloatSinINTEL"
    5869 -> showString "OpArbitraryFloatCosINTEL"
    5870 -> showString "OpArbitraryFloatSinCosINTEL"
    5871 -> showString "OpArbitraryFloatSinPiINTEL"
    5872 -> showString "OpArbitraryFloatCosPiINTEL"
    5873 -> showString "OpArbitraryFloatASinINTEL"
    5874 -> showString "OpArbitraryFloatASinPiINTEL"
    5875 -> showString "OpArbitraryFloatACosINTEL"
    5876 -> showString "OpArbitraryFloatACosPiINTEL"
    5877 -> showString "OpArbitraryFloatATanINTEL"
    5878 -> showString "OpArbitraryFloatATanPiINTEL"
    5879 -> showString "OpArbitraryFloatATan2INTEL"
    5880 -> showString "OpArbitraryFloatPowINTEL"
    5881 -> showString "OpArbitraryFloatPowRINTEL"
    5882 -> showString "OpArbitraryFloatPowNINTEL"
    5887 -> showString "OpLoopControlINTEL"
    5911 -> showString "OpAliasDomainDeclINTEL"
    5912 -> showString "OpAliasScopeDeclINTEL"
    5913 -> showString "OpAliasScopeListDeclINTEL"
    5923 -> showString "OpFixedSqrtINTEL"
    5924 -> showString "OpFixedRecipINTEL"
    5925 -> showString "OpFixedRsqrtINTEL"
    5926 -> showString "OpFixedSinINTEL"
    5927 -> showString "OpFixedCosINTEL"
    5928 -> showString "OpFixedSinCosINTEL"
    5929 -> showString "OpFixedSinPiINTEL"
    5930 -> showString "OpFixedCosPiINTEL"
    5931 -> showString "OpFixedSinCosPiINTEL"
    5932 -> showString "OpFixedLogINTEL"
    5933 -> showString "OpFixedExpINTEL"
    5934 -> showString "OpPtrCastToCrossWorkgroupINTEL"
    5938 -> showString "OpCrossWorkgroupCastToPtrINTEL"
    5946 -> showString "OpReadPipeBlockingINTEL"
    5947 -> showString "OpWritePipeBlockingINTEL"
    5949 -> showString "OpFPGARegINTEL"
    6016 -> showString "OpRayQueryGetRayTMinKHR"
    6017 -> showString "OpRayQueryGetRayFlagsKHR"
    6018 -> showString "OpRayQueryGetIntersectionTKHR"
    6019 -> showString "OpRayQueryGetIntersectionInstanceCustomIndexKHR"
    6020 -> showString "OpRayQueryGetIntersectionInstanceIdKHR"
    6021 -> showString "OpRayQueryGetIntersectionInstanceShaderBindingTableRecordOffsetKHR"
    6022 -> showString "OpRayQueryGetIntersectionGeometryIndexKHR"
    6023 -> showString "OpRayQueryGetIntersectionPrimitiveIndexKHR"
    6024 -> showString "OpRayQueryGetIntersectionBarycentricsKHR"
    6025 -> showString "OpRayQueryGetIntersectionFrontFaceKHR"
    6026 -> showString "OpRayQueryGetIntersectionCandidateAABBOpaqueKHR"
    6027 -> showString "OpRayQueryGetIntersectionObjectRayDirectionKHR"
    6028 -> showString "OpRayQueryGetIntersectionObjectRayOriginKHR"
    6029 -> showString "OpRayQueryGetWorldRayDirectionKHR"
    6030 -> showString "OpRayQueryGetWorldRayOriginKHR"
    6031 -> showString "OpRayQueryGetIntersectionObjectToWorldKHR"
    6032 -> showString "OpRayQueryGetIntersectionWorldToObjectKHR"
    6035 -> showString "OpAtomicFAddEXT"
    6086 -> showString "OpTypeBufferSurfaceINTEL"
    6090 -> showString "OpTypeStructContinuedINTEL"
    6091 -> showString "OpConstantCompositeContinuedINTEL"
    6092 -> showString "OpSpecConstantCompositeContinuedINTEL"
    6096 -> showString "OpCompositeConstructContinuedINTEL"
    6116 -> showString "OpConvertFToBF16INTEL"
    6117 -> showString "OpConvertBF16ToFINTEL"
    6142 -> showString "OpControlBarrierArriveINTEL"
    6143 -> showString "OpControlBarrierWaitINTEL"
    6401 -> showString "OpGroupIMulKHR"
    6402 -> showString "OpGroupFMulKHR"
    6403 -> showString "OpGroupBitwiseAndKHR"
    6404 -> showString "OpGroupBitwiseOrKHR"
    6405 -> showString "OpGroupBitwiseXorKHR"
    6406 -> showString "OpGroupLogicalAndKHR"
    6407 -> showString "OpGroupLogicalOrKHR"
    6408 -> showString "OpGroupLogicalXorKHR"
    6428 -> showString "OpMaskedGatherINTEL"
    6429 -> showString "OpMaskedScatterINTEL"
    x -> showParen (p > 10) $ showString "Op " . showsPrec (p + 1) x

pattern OpNop :: Op
pattern OpNop = Op 0

pattern OpUndef :: Op
pattern OpUndef = Op 1

pattern OpSourceContinued :: Op
pattern OpSourceContinued = Op 2

pattern OpSource :: Op
pattern OpSource = Op 3

pattern OpSourceExtension :: Op
pattern OpSourceExtension = Op 4

pattern OpName :: Op
pattern OpName = Op 5

pattern OpMemberName :: Op
pattern OpMemberName = Op 6

pattern OpString :: Op
pattern OpString = Op 7

pattern OpLine :: Op
pattern OpLine = Op 8

pattern OpExtension :: Op
pattern OpExtension = Op 10

pattern OpExtInstImport :: Op
pattern OpExtInstImport = Op 11

pattern OpExtInst :: Op
pattern OpExtInst = Op 12

pattern OpMemoryModel :: Op
pattern OpMemoryModel = Op 14

pattern OpEntryPoint :: Op
pattern OpEntryPoint = Op 15

pattern OpExecutionMode :: Op
pattern OpExecutionMode = Op 16

pattern OpCapability :: Op
pattern OpCapability = Op 17

pattern OpTypeVoid :: Op
pattern OpTypeVoid = Op 19

pattern OpTypeBool :: Op
pattern OpTypeBool = Op 20

pattern OpTypeInt :: Op
pattern OpTypeInt = Op 21

pattern OpTypeFloat :: Op
pattern OpTypeFloat = Op 22

pattern OpTypeVector :: Op
pattern OpTypeVector = Op 23

pattern OpTypeMatrix :: Op
pattern OpTypeMatrix = Op 24

pattern OpTypeImage :: Op
pattern OpTypeImage = Op 25

pattern OpTypeSampler :: Op
pattern OpTypeSampler = Op 26

pattern OpTypeSampledImage :: Op
pattern OpTypeSampledImage = Op 27

pattern OpTypeArray :: Op
pattern OpTypeArray = Op 28

pattern OpTypeRuntimeArray :: Op
pattern OpTypeRuntimeArray = Op 29

pattern OpTypeStruct :: Op
pattern OpTypeStruct = Op 30

pattern OpTypeOpaque :: Op
pattern OpTypeOpaque = Op 31

pattern OpTypePointer :: Op
pattern OpTypePointer = Op 32

pattern OpTypeFunction :: Op
pattern OpTypeFunction = Op 33

pattern OpTypeEvent :: Op
pattern OpTypeEvent = Op 34

pattern OpTypeDeviceEvent :: Op
pattern OpTypeDeviceEvent = Op 35

pattern OpTypeReserveId :: Op
pattern OpTypeReserveId = Op 36

pattern OpTypeQueue :: Op
pattern OpTypeQueue = Op 37

pattern OpTypePipe :: Op
pattern OpTypePipe = Op 38

pattern OpTypeForwardPointer :: Op
pattern OpTypeForwardPointer = Op 39

pattern OpConstantTrue :: Op
pattern OpConstantTrue = Op 41

pattern OpConstantFalse :: Op
pattern OpConstantFalse = Op 42

pattern OpConstant :: Op
pattern OpConstant = Op 43

pattern OpConstantComposite :: Op
pattern OpConstantComposite = Op 44

pattern OpConstantSampler :: Op
pattern OpConstantSampler = Op 45

pattern OpConstantNull :: Op
pattern OpConstantNull = Op 46

pattern OpSpecConstantTrue :: Op
pattern OpSpecConstantTrue = Op 48

pattern OpSpecConstantFalse :: Op
pattern OpSpecConstantFalse = Op 49

pattern OpSpecConstant :: Op
pattern OpSpecConstant = Op 50

pattern OpSpecConstantComposite :: Op
pattern OpSpecConstantComposite = Op 51

pattern OpSpecConstantOp :: Op
pattern OpSpecConstantOp = Op 52

pattern OpFunction :: Op
pattern OpFunction = Op 54

pattern OpFunctionParameter :: Op
pattern OpFunctionParameter = Op 55

pattern OpFunctionEnd :: Op
pattern OpFunctionEnd = Op 56

pattern OpFunctionCall :: Op
pattern OpFunctionCall = Op 57

pattern OpVariable :: Op
pattern OpVariable = Op 59

pattern OpImageTexelPointer :: Op
pattern OpImageTexelPointer = Op 60

pattern OpLoad :: Op
pattern OpLoad = Op 61

pattern OpStore :: Op
pattern OpStore = Op 62

pattern OpCopyMemory :: Op
pattern OpCopyMemory = Op 63

pattern OpCopyMemorySized :: Op
pattern OpCopyMemorySized = Op 64

pattern OpAccessChain :: Op
pattern OpAccessChain = Op 65

pattern OpInBoundsAccessChain :: Op
pattern OpInBoundsAccessChain = Op 66

pattern OpPtrAccessChain :: Op
pattern OpPtrAccessChain = Op 67

pattern OpArrayLength :: Op
pattern OpArrayLength = Op 68

pattern OpGenericPtrMemSemantics :: Op
pattern OpGenericPtrMemSemantics = Op 69

pattern OpInBoundsPtrAccessChain :: Op
pattern OpInBoundsPtrAccessChain = Op 70

pattern OpDecorate :: Op
pattern OpDecorate = Op 71

pattern OpMemberDecorate :: Op
pattern OpMemberDecorate = Op 72

pattern OpDecorationGroup :: Op
pattern OpDecorationGroup = Op 73

pattern OpGroupDecorate :: Op
pattern OpGroupDecorate = Op 74

pattern OpGroupMemberDecorate :: Op
pattern OpGroupMemberDecorate = Op 75

pattern OpVectorExtractDynamic :: Op
pattern OpVectorExtractDynamic = Op 77

pattern OpVectorInsertDynamic :: Op
pattern OpVectorInsertDynamic = Op 78

pattern OpVectorShuffle :: Op
pattern OpVectorShuffle = Op 79

pattern OpCompositeConstruct :: Op
pattern OpCompositeConstruct = Op 80

pattern OpCompositeExtract :: Op
pattern OpCompositeExtract = Op 81

pattern OpCompositeInsert :: Op
pattern OpCompositeInsert = Op 82

pattern OpCopyObject :: Op
pattern OpCopyObject = Op 83

pattern OpTranspose :: Op
pattern OpTranspose = Op 84

pattern OpSampledImage :: Op
pattern OpSampledImage = Op 86

pattern OpImageSampleImplicitLod :: Op
pattern OpImageSampleImplicitLod = Op 87

pattern OpImageSampleExplicitLod :: Op
pattern OpImageSampleExplicitLod = Op 88

pattern OpImageSampleDrefImplicitLod :: Op
pattern OpImageSampleDrefImplicitLod = Op 89

pattern OpImageSampleDrefExplicitLod :: Op
pattern OpImageSampleDrefExplicitLod = Op 90

pattern OpImageSampleProjImplicitLod :: Op
pattern OpImageSampleProjImplicitLod = Op 91

pattern OpImageSampleProjExplicitLod :: Op
pattern OpImageSampleProjExplicitLod = Op 92

pattern OpImageSampleProjDrefImplicitLod :: Op
pattern OpImageSampleProjDrefImplicitLod = Op 93

pattern OpImageSampleProjDrefExplicitLod :: Op
pattern OpImageSampleProjDrefExplicitLod = Op 94

pattern OpImageFetch :: Op
pattern OpImageFetch = Op 95

pattern OpImageGather :: Op
pattern OpImageGather = Op 96

pattern OpImageDrefGather :: Op
pattern OpImageDrefGather = Op 97

pattern OpImageRead :: Op
pattern OpImageRead = Op 98

pattern OpImageWrite :: Op
pattern OpImageWrite = Op 99

pattern OpImage :: Op
pattern OpImage = Op 100

pattern OpImageQueryFormat :: Op
pattern OpImageQueryFormat = Op 101

pattern OpImageQueryOrder :: Op
pattern OpImageQueryOrder = Op 102

pattern OpImageQuerySizeLod :: Op
pattern OpImageQuerySizeLod = Op 103

pattern OpImageQuerySize :: Op
pattern OpImageQuerySize = Op 104

pattern OpImageQueryLod :: Op
pattern OpImageQueryLod = Op 105

pattern OpImageQueryLevels :: Op
pattern OpImageQueryLevels = Op 106

pattern OpImageQuerySamples :: Op
pattern OpImageQuerySamples = Op 107

pattern OpConvertFToU :: Op
pattern OpConvertFToU = Op 109

pattern OpConvertFToS :: Op
pattern OpConvertFToS = Op 110

pattern OpConvertSToF :: Op
pattern OpConvertSToF = Op 111

pattern OpConvertUToF :: Op
pattern OpConvertUToF = Op 112

pattern OpUConvert :: Op
pattern OpUConvert = Op 113

pattern OpSConvert :: Op
pattern OpSConvert = Op 114

pattern OpFConvert :: Op
pattern OpFConvert = Op 115

pattern OpQuantizeToF16 :: Op
pattern OpQuantizeToF16 = Op 116

pattern OpConvertPtrToU :: Op
pattern OpConvertPtrToU = Op 117

pattern OpSatConvertSToU :: Op
pattern OpSatConvertSToU = Op 118

pattern OpSatConvertUToS :: Op
pattern OpSatConvertUToS = Op 119

pattern OpConvertUToPtr :: Op
pattern OpConvertUToPtr = Op 120

pattern OpPtrCastToGeneric :: Op
pattern OpPtrCastToGeneric = Op 121

pattern OpGenericCastToPtr :: Op
pattern OpGenericCastToPtr = Op 122

pattern OpGenericCastToPtrExplicit :: Op
pattern OpGenericCastToPtrExplicit = Op 123

pattern OpBitcast :: Op
pattern OpBitcast = Op 124

pattern OpSNegate :: Op
pattern OpSNegate = Op 126

pattern OpFNegate :: Op
pattern OpFNegate = Op 127

pattern OpIAdd :: Op
pattern OpIAdd = Op 128

pattern OpFAdd :: Op
pattern OpFAdd = Op 129

pattern OpISub :: Op
pattern OpISub = Op 130

pattern OpFSub :: Op
pattern OpFSub = Op 131

pattern OpIMul :: Op
pattern OpIMul = Op 132

pattern OpFMul :: Op
pattern OpFMul = Op 133

pattern OpUDiv :: Op
pattern OpUDiv = Op 134

pattern OpSDiv :: Op
pattern OpSDiv = Op 135

pattern OpFDiv :: Op
pattern OpFDiv = Op 136

pattern OpUMod :: Op
pattern OpUMod = Op 137

pattern OpSRem :: Op
pattern OpSRem = Op 138

pattern OpSMod :: Op
pattern OpSMod = Op 139

pattern OpFRem :: Op
pattern OpFRem = Op 140

pattern OpFMod :: Op
pattern OpFMod = Op 141

pattern OpVectorTimesScalar :: Op
pattern OpVectorTimesScalar = Op 142

pattern OpMatrixTimesScalar :: Op
pattern OpMatrixTimesScalar = Op 143

pattern OpVectorTimesMatrix :: Op
pattern OpVectorTimesMatrix = Op 144

pattern OpMatrixTimesVector :: Op
pattern OpMatrixTimesVector = Op 145

pattern OpMatrixTimesMatrix :: Op
pattern OpMatrixTimesMatrix = Op 146

pattern OpOuterProduct :: Op
pattern OpOuterProduct = Op 147

pattern OpDot :: Op
pattern OpDot = Op 148

pattern OpIAddCarry :: Op
pattern OpIAddCarry = Op 149

pattern OpISubBorrow :: Op
pattern OpISubBorrow = Op 150

pattern OpUMulExtended :: Op
pattern OpUMulExtended = Op 151

pattern OpSMulExtended :: Op
pattern OpSMulExtended = Op 152

pattern OpAny :: Op
pattern OpAny = Op 154

pattern OpAll :: Op
pattern OpAll = Op 155

pattern OpIsNan :: Op
pattern OpIsNan = Op 156

pattern OpIsInf :: Op
pattern OpIsInf = Op 157

pattern OpIsFinite :: Op
pattern OpIsFinite = Op 158

pattern OpIsNormal :: Op
pattern OpIsNormal = Op 159

pattern OpSignBitSet :: Op
pattern OpSignBitSet = Op 160

pattern OpLessOrGreater :: Op
pattern OpLessOrGreater = Op 161

pattern OpOrdered :: Op
pattern OpOrdered = Op 162

pattern OpUnordered :: Op
pattern OpUnordered = Op 163

pattern OpLogicalEqual :: Op
pattern OpLogicalEqual = Op 164

pattern OpLogicalNotEqual :: Op
pattern OpLogicalNotEqual = Op 165

pattern OpLogicalOr :: Op
pattern OpLogicalOr = Op 166

pattern OpLogicalAnd :: Op
pattern OpLogicalAnd = Op 167

pattern OpLogicalNot :: Op
pattern OpLogicalNot = Op 168

pattern OpSelect :: Op
pattern OpSelect = Op 169

pattern OpIEqual :: Op
pattern OpIEqual = Op 170

pattern OpINotEqual :: Op
pattern OpINotEqual = Op 171

pattern OpUGreaterThan :: Op
pattern OpUGreaterThan = Op 172

pattern OpSGreaterThan :: Op
pattern OpSGreaterThan = Op 173

pattern OpUGreaterThanEqual :: Op
pattern OpUGreaterThanEqual = Op 174

pattern OpSGreaterThanEqual :: Op
pattern OpSGreaterThanEqual = Op 175

pattern OpULessThan :: Op
pattern OpULessThan = Op 176

pattern OpSLessThan :: Op
pattern OpSLessThan = Op 177

pattern OpULessThanEqual :: Op
pattern OpULessThanEqual = Op 178

pattern OpSLessThanEqual :: Op
pattern OpSLessThanEqual = Op 179

pattern OpFOrdEqual :: Op
pattern OpFOrdEqual = Op 180

pattern OpFUnordEqual :: Op
pattern OpFUnordEqual = Op 181

pattern OpFOrdNotEqual :: Op
pattern OpFOrdNotEqual = Op 182

pattern OpFUnordNotEqual :: Op
pattern OpFUnordNotEqual = Op 183

pattern OpFOrdLessThan :: Op
pattern OpFOrdLessThan = Op 184

pattern OpFUnordLessThan :: Op
pattern OpFUnordLessThan = Op 185

pattern OpFOrdGreaterThan :: Op
pattern OpFOrdGreaterThan = Op 186

pattern OpFUnordGreaterThan :: Op
pattern OpFUnordGreaterThan = Op 187

pattern OpFOrdLessThanEqual :: Op
pattern OpFOrdLessThanEqual = Op 188

pattern OpFUnordLessThanEqual :: Op
pattern OpFUnordLessThanEqual = Op 189

pattern OpFOrdGreaterThanEqual :: Op
pattern OpFOrdGreaterThanEqual = Op 190

pattern OpFUnordGreaterThanEqual :: Op
pattern OpFUnordGreaterThanEqual = Op 191

pattern OpShiftRightLogical :: Op
pattern OpShiftRightLogical = Op 194

pattern OpShiftRightArithmetic :: Op
pattern OpShiftRightArithmetic = Op 195

pattern OpShiftLeftLogical :: Op
pattern OpShiftLeftLogical = Op 196

pattern OpBitwiseOr :: Op
pattern OpBitwiseOr = Op 197

pattern OpBitwiseXor :: Op
pattern OpBitwiseXor = Op 198

pattern OpBitwiseAnd :: Op
pattern OpBitwiseAnd = Op 199

pattern OpNot :: Op
pattern OpNot = Op 200

pattern OpBitFieldInsert :: Op
pattern OpBitFieldInsert = Op 201

pattern OpBitFieldSExtract :: Op
pattern OpBitFieldSExtract = Op 202

pattern OpBitFieldUExtract :: Op
pattern OpBitFieldUExtract = Op 203

pattern OpBitReverse :: Op
pattern OpBitReverse = Op 204

pattern OpBitCount :: Op
pattern OpBitCount = Op 205

pattern OpDPdx :: Op
pattern OpDPdx = Op 207

pattern OpDPdy :: Op
pattern OpDPdy = Op 208

pattern OpFwidth :: Op
pattern OpFwidth = Op 209

pattern OpDPdxFine :: Op
pattern OpDPdxFine = Op 210

pattern OpDPdyFine :: Op
pattern OpDPdyFine = Op 211

pattern OpFwidthFine :: Op
pattern OpFwidthFine = Op 212

pattern OpDPdxCoarse :: Op
pattern OpDPdxCoarse = Op 213

pattern OpDPdyCoarse :: Op
pattern OpDPdyCoarse = Op 214

pattern OpFwidthCoarse :: Op
pattern OpFwidthCoarse = Op 215

pattern OpEmitVertex :: Op
pattern OpEmitVertex = Op 218

pattern OpEndPrimitive :: Op
pattern OpEndPrimitive = Op 219

pattern OpEmitStreamVertex :: Op
pattern OpEmitStreamVertex = Op 220

pattern OpEndStreamPrimitive :: Op
pattern OpEndStreamPrimitive = Op 221

pattern OpControlBarrier :: Op
pattern OpControlBarrier = Op 224

pattern OpMemoryBarrier :: Op
pattern OpMemoryBarrier = Op 225

pattern OpAtomicLoad :: Op
pattern OpAtomicLoad = Op 227

pattern OpAtomicStore :: Op
pattern OpAtomicStore = Op 228

pattern OpAtomicExchange :: Op
pattern OpAtomicExchange = Op 229

pattern OpAtomicCompareExchange :: Op
pattern OpAtomicCompareExchange = Op 230

pattern OpAtomicCompareExchangeWeak :: Op
pattern OpAtomicCompareExchangeWeak = Op 231

pattern OpAtomicIIncrement :: Op
pattern OpAtomicIIncrement = Op 232

pattern OpAtomicIDecrement :: Op
pattern OpAtomicIDecrement = Op 233

pattern OpAtomicIAdd :: Op
pattern OpAtomicIAdd = Op 234

pattern OpAtomicISub :: Op
pattern OpAtomicISub = Op 235

pattern OpAtomicSMin :: Op
pattern OpAtomicSMin = Op 236

pattern OpAtomicUMin :: Op
pattern OpAtomicUMin = Op 237

pattern OpAtomicSMax :: Op
pattern OpAtomicSMax = Op 238

pattern OpAtomicUMax :: Op
pattern OpAtomicUMax = Op 239

pattern OpAtomicAnd :: Op
pattern OpAtomicAnd = Op 240

pattern OpAtomicOr :: Op
pattern OpAtomicOr = Op 241

pattern OpAtomicXor :: Op
pattern OpAtomicXor = Op 242

pattern OpPhi :: Op
pattern OpPhi = Op 245

pattern OpLoopMerge :: Op
pattern OpLoopMerge = Op 246

pattern OpSelectionMerge :: Op
pattern OpSelectionMerge = Op 247

pattern OpLabel :: Op
pattern OpLabel = Op 248

pattern OpBranch :: Op
pattern OpBranch = Op 249

pattern OpBranchConditional :: Op
pattern OpBranchConditional = Op 250

pattern OpSwitch :: Op
pattern OpSwitch = Op 251

pattern OpKill :: Op
pattern OpKill = Op 252

pattern OpReturn :: Op
pattern OpReturn = Op 253

pattern OpReturnValue :: Op
pattern OpReturnValue = Op 254

pattern OpUnreachable :: Op
pattern OpUnreachable = Op 255

pattern OpLifetimeStart :: Op
pattern OpLifetimeStart = Op 256

pattern OpLifetimeStop :: Op
pattern OpLifetimeStop = Op 257

pattern OpGroupAsyncCopy :: Op
pattern OpGroupAsyncCopy = Op 259

pattern OpGroupWaitEvents :: Op
pattern OpGroupWaitEvents = Op 260

pattern OpGroupAll :: Op
pattern OpGroupAll = Op 261

pattern OpGroupAny :: Op
pattern OpGroupAny = Op 262

pattern OpGroupBroadcast :: Op
pattern OpGroupBroadcast = Op 263

pattern OpGroupIAdd :: Op
pattern OpGroupIAdd = Op 264

pattern OpGroupFAdd :: Op
pattern OpGroupFAdd = Op 265

pattern OpGroupFMin :: Op
pattern OpGroupFMin = Op 266

pattern OpGroupUMin :: Op
pattern OpGroupUMin = Op 267

pattern OpGroupSMin :: Op
pattern OpGroupSMin = Op 268

pattern OpGroupFMax :: Op
pattern OpGroupFMax = Op 269

pattern OpGroupUMax :: Op
pattern OpGroupUMax = Op 270

pattern OpGroupSMax :: Op
pattern OpGroupSMax = Op 271

pattern OpReadPipe :: Op
pattern OpReadPipe = Op 274

pattern OpWritePipe :: Op
pattern OpWritePipe = Op 275

pattern OpReservedReadPipe :: Op
pattern OpReservedReadPipe = Op 276

pattern OpReservedWritePipe :: Op
pattern OpReservedWritePipe = Op 277

pattern OpReserveReadPipePackets :: Op
pattern OpReserveReadPipePackets = Op 278

pattern OpReserveWritePipePackets :: Op
pattern OpReserveWritePipePackets = Op 279

pattern OpCommitReadPipe :: Op
pattern OpCommitReadPipe = Op 280

pattern OpCommitWritePipe :: Op
pattern OpCommitWritePipe = Op 281

pattern OpIsValidReserveId :: Op
pattern OpIsValidReserveId = Op 282

pattern OpGetNumPipePackets :: Op
pattern OpGetNumPipePackets = Op 283

pattern OpGetMaxPipePackets :: Op
pattern OpGetMaxPipePackets = Op 284

pattern OpGroupReserveReadPipePackets :: Op
pattern OpGroupReserveReadPipePackets = Op 285

pattern OpGroupReserveWritePipePackets :: Op
pattern OpGroupReserveWritePipePackets = Op 286

pattern OpGroupCommitReadPipe :: Op
pattern OpGroupCommitReadPipe = Op 287

pattern OpGroupCommitWritePipe :: Op
pattern OpGroupCommitWritePipe = Op 288

pattern OpEnqueueMarker :: Op
pattern OpEnqueueMarker = Op 291

pattern OpEnqueueKernel :: Op
pattern OpEnqueueKernel = Op 292

pattern OpGetKernelNDrangeSubGroupCount :: Op
pattern OpGetKernelNDrangeSubGroupCount = Op 293

pattern OpGetKernelNDrangeMaxSubGroupSize :: Op
pattern OpGetKernelNDrangeMaxSubGroupSize = Op 294

pattern OpGetKernelWorkGroupSize :: Op
pattern OpGetKernelWorkGroupSize = Op 295

pattern OpGetKernelPreferredWorkGroupSizeMultiple :: Op
pattern OpGetKernelPreferredWorkGroupSizeMultiple = Op 296

pattern OpRetainEvent :: Op
pattern OpRetainEvent = Op 297

pattern OpReleaseEvent :: Op
pattern OpReleaseEvent = Op 298

pattern OpCreateUserEvent :: Op
pattern OpCreateUserEvent = Op 299

pattern OpIsValidEvent :: Op
pattern OpIsValidEvent = Op 300

pattern OpSetUserEventStatus :: Op
pattern OpSetUserEventStatus = Op 301

pattern OpCaptureEventProfilingInfo :: Op
pattern OpCaptureEventProfilingInfo = Op 302

pattern OpGetDefaultQueue :: Op
pattern OpGetDefaultQueue = Op 303

pattern OpBuildNDRange :: Op
pattern OpBuildNDRange = Op 304

pattern OpImageSparseSampleImplicitLod :: Op
pattern OpImageSparseSampleImplicitLod = Op 305

pattern OpImageSparseSampleExplicitLod :: Op
pattern OpImageSparseSampleExplicitLod = Op 306

pattern OpImageSparseSampleDrefImplicitLod :: Op
pattern OpImageSparseSampleDrefImplicitLod = Op 307

pattern OpImageSparseSampleDrefExplicitLod :: Op
pattern OpImageSparseSampleDrefExplicitLod = Op 308

pattern OpImageSparseSampleProjImplicitLod :: Op
pattern OpImageSparseSampleProjImplicitLod = Op 309

pattern OpImageSparseSampleProjExplicitLod :: Op
pattern OpImageSparseSampleProjExplicitLod = Op 310

pattern OpImageSparseSampleProjDrefImplicitLod :: Op
pattern OpImageSparseSampleProjDrefImplicitLod = Op 311

pattern OpImageSparseSampleProjDrefExplicitLod :: Op
pattern OpImageSparseSampleProjDrefExplicitLod = Op 312

pattern OpImageSparseFetch :: Op
pattern OpImageSparseFetch = Op 313

pattern OpImageSparseGather :: Op
pattern OpImageSparseGather = Op 314

pattern OpImageSparseDrefGather :: Op
pattern OpImageSparseDrefGather = Op 315

pattern OpImageSparseTexelsResident :: Op
pattern OpImageSparseTexelsResident = Op 316

pattern OpNoLine :: Op
pattern OpNoLine = Op 317

pattern OpAtomicFlagTestAndSet :: Op
pattern OpAtomicFlagTestAndSet = Op 318

pattern OpAtomicFlagClear :: Op
pattern OpAtomicFlagClear = Op 319

pattern OpImageSparseRead :: Op
pattern OpImageSparseRead = Op 320

pattern OpSizeOf :: Op
pattern OpSizeOf = Op 321

pattern OpTypePipeStorage :: Op
pattern OpTypePipeStorage = Op 322

pattern OpConstantPipeStorage :: Op
pattern OpConstantPipeStorage = Op 323

pattern OpCreatePipeFromPipeStorage :: Op
pattern OpCreatePipeFromPipeStorage = Op 324

pattern OpGetKernelLocalSizeForSubgroupCount :: Op
pattern OpGetKernelLocalSizeForSubgroupCount = Op 325

pattern OpGetKernelMaxNumSubgroups :: Op
pattern OpGetKernelMaxNumSubgroups = Op 326

pattern OpTypeNamedBarrier :: Op
pattern OpTypeNamedBarrier = Op 327

pattern OpNamedBarrierInitialize :: Op
pattern OpNamedBarrierInitialize = Op 328

pattern OpMemoryNamedBarrier :: Op
pattern OpMemoryNamedBarrier = Op 329

pattern OpModuleProcessed :: Op
pattern OpModuleProcessed = Op 330

pattern OpExecutionModeId :: Op
pattern OpExecutionModeId = Op 331

pattern OpDecorateId :: Op
pattern OpDecorateId = Op 332

pattern OpGroupNonUniformElect :: Op
pattern OpGroupNonUniformElect = Op 333

pattern OpGroupNonUniformAll :: Op
pattern OpGroupNonUniformAll = Op 334

pattern OpGroupNonUniformAny :: Op
pattern OpGroupNonUniformAny = Op 335

pattern OpGroupNonUniformAllEqual :: Op
pattern OpGroupNonUniformAllEqual = Op 336

pattern OpGroupNonUniformBroadcast :: Op
pattern OpGroupNonUniformBroadcast = Op 337

pattern OpGroupNonUniformBroadcastFirst :: Op
pattern OpGroupNonUniformBroadcastFirst = Op 338

pattern OpGroupNonUniformBallot :: Op
pattern OpGroupNonUniformBallot = Op 339

pattern OpGroupNonUniformInverseBallot :: Op
pattern OpGroupNonUniformInverseBallot = Op 340

pattern OpGroupNonUniformBallotBitExtract :: Op
pattern OpGroupNonUniformBallotBitExtract = Op 341

pattern OpGroupNonUniformBallotBitCount :: Op
pattern OpGroupNonUniformBallotBitCount = Op 342

pattern OpGroupNonUniformBallotFindLSB :: Op
pattern OpGroupNonUniformBallotFindLSB = Op 343

pattern OpGroupNonUniformBallotFindMSB :: Op
pattern OpGroupNonUniformBallotFindMSB = Op 344

pattern OpGroupNonUniformShuffle :: Op
pattern OpGroupNonUniformShuffle = Op 345

pattern OpGroupNonUniformShuffleXor :: Op
pattern OpGroupNonUniformShuffleXor = Op 346

pattern OpGroupNonUniformShuffleUp :: Op
pattern OpGroupNonUniformShuffleUp = Op 347

pattern OpGroupNonUniformShuffleDown :: Op
pattern OpGroupNonUniformShuffleDown = Op 348

pattern OpGroupNonUniformIAdd :: Op
pattern OpGroupNonUniformIAdd = Op 349

pattern OpGroupNonUniformFAdd :: Op
pattern OpGroupNonUniformFAdd = Op 350

pattern OpGroupNonUniformIMul :: Op
pattern OpGroupNonUniformIMul = Op 351

pattern OpGroupNonUniformFMul :: Op
pattern OpGroupNonUniformFMul = Op 352

pattern OpGroupNonUniformSMin :: Op
pattern OpGroupNonUniformSMin = Op 353

pattern OpGroupNonUniformUMin :: Op
pattern OpGroupNonUniformUMin = Op 354

pattern OpGroupNonUniformFMin :: Op
pattern OpGroupNonUniformFMin = Op 355

pattern OpGroupNonUniformSMax :: Op
pattern OpGroupNonUniformSMax = Op 356

pattern OpGroupNonUniformUMax :: Op
pattern OpGroupNonUniformUMax = Op 357

pattern OpGroupNonUniformFMax :: Op
pattern OpGroupNonUniformFMax = Op 358

pattern OpGroupNonUniformBitwiseAnd :: Op
pattern OpGroupNonUniformBitwiseAnd = Op 359

pattern OpGroupNonUniformBitwiseOr :: Op
pattern OpGroupNonUniformBitwiseOr = Op 360

pattern OpGroupNonUniformBitwiseXor :: Op
pattern OpGroupNonUniformBitwiseXor = Op 361

pattern OpGroupNonUniformLogicalAnd :: Op
pattern OpGroupNonUniformLogicalAnd = Op 362

pattern OpGroupNonUniformLogicalOr :: Op
pattern OpGroupNonUniformLogicalOr = Op 363

pattern OpGroupNonUniformLogicalXor :: Op
pattern OpGroupNonUniformLogicalXor = Op 364

pattern OpGroupNonUniformQuadBroadcast :: Op
pattern OpGroupNonUniformQuadBroadcast = Op 365

pattern OpGroupNonUniformQuadSwap :: Op
pattern OpGroupNonUniformQuadSwap = Op 366

pattern OpCopyLogical :: Op
pattern OpCopyLogical = Op 400

pattern OpPtrEqual :: Op
pattern OpPtrEqual = Op 401

pattern OpPtrNotEqual :: Op
pattern OpPtrNotEqual = Op 402

pattern OpPtrDiff :: Op
pattern OpPtrDiff = Op 403

pattern OpColorAttachmentReadEXT :: Op
pattern OpColorAttachmentReadEXT = Op 4160

pattern OpDepthAttachmentReadEXT :: Op
pattern OpDepthAttachmentReadEXT = Op 4161

pattern OpStencilAttachmentReadEXT :: Op
pattern OpStencilAttachmentReadEXT = Op 4162

pattern OpTerminateInvocation :: Op
pattern OpTerminateInvocation = Op 4416

pattern OpSubgroupBallotKHR :: Op
pattern OpSubgroupBallotKHR = Op 4421

pattern OpSubgroupFirstInvocationKHR :: Op
pattern OpSubgroupFirstInvocationKHR = Op 4422

pattern OpSubgroupAllKHR :: Op
pattern OpSubgroupAllKHR = Op 4428

pattern OpSubgroupAnyKHR :: Op
pattern OpSubgroupAnyKHR = Op 4429

pattern OpSubgroupAllEqualKHR :: Op
pattern OpSubgroupAllEqualKHR = Op 4430

pattern OpGroupNonUniformRotateKHR :: Op
pattern OpGroupNonUniformRotateKHR = Op 4431

pattern OpSubgroupReadInvocationKHR :: Op
pattern OpSubgroupReadInvocationKHR = Op 4432

pattern OpExtInstWithForwardRefsKHR :: Op
pattern OpExtInstWithForwardRefsKHR = Op 4433

pattern OpTraceRayKHR :: Op
pattern OpTraceRayKHR = Op 4445

pattern OpExecuteCallableKHR :: Op
pattern OpExecuteCallableKHR = Op 4446

pattern OpConvertUToAccelerationStructureKHR :: Op
pattern OpConvertUToAccelerationStructureKHR = Op 4447

pattern OpIgnoreIntersectionKHR :: Op
pattern OpIgnoreIntersectionKHR = Op 4448

pattern OpTerminateRayKHR :: Op
pattern OpTerminateRayKHR = Op 4449

pattern OpSDot :: Op
pattern OpSDot = Op 4450

pattern OpSDotKHR :: Op
pattern OpSDotKHR = Op 4450

pattern OpUDot :: Op
pattern OpUDot = Op 4451

pattern OpUDotKHR :: Op
pattern OpUDotKHR = Op 4451

pattern OpSUDot :: Op
pattern OpSUDot = Op 4452

pattern OpSUDotKHR :: Op
pattern OpSUDotKHR = Op 4452

pattern OpSDotAccSat :: Op
pattern OpSDotAccSat = Op 4453

pattern OpSDotAccSatKHR :: Op
pattern OpSDotAccSatKHR = Op 4453

pattern OpUDotAccSat :: Op
pattern OpUDotAccSat = Op 4454

pattern OpUDotAccSatKHR :: Op
pattern OpUDotAccSatKHR = Op 4454

pattern OpSUDotAccSat :: Op
pattern OpSUDotAccSat = Op 4455

pattern OpSUDotAccSatKHR :: Op
pattern OpSUDotAccSatKHR = Op 4455

pattern OpTypeCooperativeMatrixKHR :: Op
pattern OpTypeCooperativeMatrixKHR = Op 4456

pattern OpCooperativeMatrixLoadKHR :: Op
pattern OpCooperativeMatrixLoadKHR = Op 4457

pattern OpCooperativeMatrixStoreKHR :: Op
pattern OpCooperativeMatrixStoreKHR = Op 4458

pattern OpCooperativeMatrixMulAddKHR :: Op
pattern OpCooperativeMatrixMulAddKHR = Op 4459

pattern OpCooperativeMatrixLengthKHR :: Op
pattern OpCooperativeMatrixLengthKHR = Op 4460

pattern OpConstantCompositeReplicateEXT :: Op
pattern OpConstantCompositeReplicateEXT = Op 4461

pattern OpSpecConstantCompositeReplicateEXT :: Op
pattern OpSpecConstantCompositeReplicateEXT = Op 4462

pattern OpCompositeConstructReplicateEXT :: Op
pattern OpCompositeConstructReplicateEXT = Op 4463

pattern OpTypeRayQueryKHR :: Op
pattern OpTypeRayQueryKHR = Op 4472

pattern OpRayQueryInitializeKHR :: Op
pattern OpRayQueryInitializeKHR = Op 4473

pattern OpRayQueryTerminateKHR :: Op
pattern OpRayQueryTerminateKHR = Op 4474

pattern OpRayQueryGenerateIntersectionKHR :: Op
pattern OpRayQueryGenerateIntersectionKHR = Op 4475

pattern OpRayQueryConfirmIntersectionKHR :: Op
pattern OpRayQueryConfirmIntersectionKHR = Op 4476

pattern OpRayQueryProceedKHR :: Op
pattern OpRayQueryProceedKHR = Op 4477

pattern OpRayQueryGetIntersectionTypeKHR :: Op
pattern OpRayQueryGetIntersectionTypeKHR = Op 4479

pattern OpImageSampleWeightedQCOM :: Op
pattern OpImageSampleWeightedQCOM = Op 4480

pattern OpImageBoxFilterQCOM :: Op
pattern OpImageBoxFilterQCOM = Op 4481

pattern OpImageBlockMatchSSDQCOM :: Op
pattern OpImageBlockMatchSSDQCOM = Op 4482

pattern OpImageBlockMatchSADQCOM :: Op
pattern OpImageBlockMatchSADQCOM = Op 4483

pattern OpImageBlockMatchWindowSSDQCOM :: Op
pattern OpImageBlockMatchWindowSSDQCOM = Op 4500

pattern OpImageBlockMatchWindowSADQCOM :: Op
pattern OpImageBlockMatchWindowSADQCOM = Op 4501

pattern OpImageBlockMatchGatherSSDQCOM :: Op
pattern OpImageBlockMatchGatherSSDQCOM = Op 4502

pattern OpImageBlockMatchGatherSADQCOM :: Op
pattern OpImageBlockMatchGatherSADQCOM = Op 4503

pattern OpGroupIAddNonUniformAMD :: Op
pattern OpGroupIAddNonUniformAMD = Op 5000

pattern OpGroupFAddNonUniformAMD :: Op
pattern OpGroupFAddNonUniformAMD = Op 5001

pattern OpGroupFMinNonUniformAMD :: Op
pattern OpGroupFMinNonUniformAMD = Op 5002

pattern OpGroupUMinNonUniformAMD :: Op
pattern OpGroupUMinNonUniformAMD = Op 5003

pattern OpGroupSMinNonUniformAMD :: Op
pattern OpGroupSMinNonUniformAMD = Op 5004

pattern OpGroupFMaxNonUniformAMD :: Op
pattern OpGroupFMaxNonUniformAMD = Op 5005

pattern OpGroupUMaxNonUniformAMD :: Op
pattern OpGroupUMaxNonUniformAMD = Op 5006

pattern OpGroupSMaxNonUniformAMD :: Op
pattern OpGroupSMaxNonUniformAMD = Op 5007

pattern OpFragmentMaskFetchAMD :: Op
pattern OpFragmentMaskFetchAMD = Op 5011

pattern OpFragmentFetchAMD :: Op
pattern OpFragmentFetchAMD = Op 5012

pattern OpReadClockKHR :: Op
pattern OpReadClockKHR = Op 5056

pattern OpFinalizeNodePayloadsAMDX :: Op
pattern OpFinalizeNodePayloadsAMDX = Op 5075

pattern OpFinishWritingNodePayloadAMDX :: Op
pattern OpFinishWritingNodePayloadAMDX = Op 5078

pattern OpInitializeNodePayloadsAMDX :: Op
pattern OpInitializeNodePayloadsAMDX = Op 5090

pattern OpGroupNonUniformQuadAllKHR :: Op
pattern OpGroupNonUniformQuadAllKHR = Op 5110

pattern OpGroupNonUniformQuadAnyKHR :: Op
pattern OpGroupNonUniformQuadAnyKHR = Op 5111

pattern OpHitObjectRecordHitMotionNV :: Op
pattern OpHitObjectRecordHitMotionNV = Op 5249

pattern OpHitObjectRecordHitWithIndexMotionNV :: Op
pattern OpHitObjectRecordHitWithIndexMotionNV = Op 5250

pattern OpHitObjectRecordMissMotionNV :: Op
pattern OpHitObjectRecordMissMotionNV = Op 5251

pattern OpHitObjectGetWorldToObjectNV :: Op
pattern OpHitObjectGetWorldToObjectNV = Op 5252

pattern OpHitObjectGetObjectToWorldNV :: Op
pattern OpHitObjectGetObjectToWorldNV = Op 5253

pattern OpHitObjectGetObjectRayDirectionNV :: Op
pattern OpHitObjectGetObjectRayDirectionNV = Op 5254

pattern OpHitObjectGetObjectRayOriginNV :: Op
pattern OpHitObjectGetObjectRayOriginNV = Op 5255

pattern OpHitObjectTraceRayMotionNV :: Op
pattern OpHitObjectTraceRayMotionNV = Op 5256

pattern OpHitObjectGetShaderRecordBufferHandleNV :: Op
pattern OpHitObjectGetShaderRecordBufferHandleNV = Op 5257

pattern OpHitObjectGetShaderBindingTableRecordIndexNV :: Op
pattern OpHitObjectGetShaderBindingTableRecordIndexNV = Op 5258

pattern OpHitObjectRecordEmptyNV :: Op
pattern OpHitObjectRecordEmptyNV = Op 5259

pattern OpHitObjectTraceRayNV :: Op
pattern OpHitObjectTraceRayNV = Op 5260

pattern OpHitObjectRecordHitNV :: Op
pattern OpHitObjectRecordHitNV = Op 5261

pattern OpHitObjectRecordHitWithIndexNV :: Op
pattern OpHitObjectRecordHitWithIndexNV = Op 5262

pattern OpHitObjectRecordMissNV :: Op
pattern OpHitObjectRecordMissNV = Op 5263

pattern OpHitObjectExecuteShaderNV :: Op
pattern OpHitObjectExecuteShaderNV = Op 5264

pattern OpHitObjectGetCurrentTimeNV :: Op
pattern OpHitObjectGetCurrentTimeNV = Op 5265

pattern OpHitObjectGetAttributesNV :: Op
pattern OpHitObjectGetAttributesNV = Op 5266

pattern OpHitObjectGetHitKindNV :: Op
pattern OpHitObjectGetHitKindNV = Op 5267

pattern OpHitObjectGetPrimitiveIndexNV :: Op
pattern OpHitObjectGetPrimitiveIndexNV = Op 5268

pattern OpHitObjectGetGeometryIndexNV :: Op
pattern OpHitObjectGetGeometryIndexNV = Op 5269

pattern OpHitObjectGetInstanceIdNV :: Op
pattern OpHitObjectGetInstanceIdNV = Op 5270

pattern OpHitObjectGetInstanceCustomIndexNV :: Op
pattern OpHitObjectGetInstanceCustomIndexNV = Op 5271

pattern OpHitObjectGetWorldRayDirectionNV :: Op
pattern OpHitObjectGetWorldRayDirectionNV = Op 5272

pattern OpHitObjectGetWorldRayOriginNV :: Op
pattern OpHitObjectGetWorldRayOriginNV = Op 5273

pattern OpHitObjectGetRayTMaxNV :: Op
pattern OpHitObjectGetRayTMaxNV = Op 5274

pattern OpHitObjectGetRayTMinNV :: Op
pattern OpHitObjectGetRayTMinNV = Op 5275

pattern OpHitObjectIsEmptyNV :: Op
pattern OpHitObjectIsEmptyNV = Op 5276

pattern OpHitObjectIsHitNV :: Op
pattern OpHitObjectIsHitNV = Op 5277

pattern OpHitObjectIsMissNV :: Op
pattern OpHitObjectIsMissNV = Op 5278

pattern OpReorderThreadWithHitObjectNV :: Op
pattern OpReorderThreadWithHitObjectNV = Op 5279

pattern OpReorderThreadWithHintNV :: Op
pattern OpReorderThreadWithHintNV = Op 5280

pattern OpTypeHitObjectNV :: Op
pattern OpTypeHitObjectNV = Op 5281

pattern OpImageSampleFootprintNV :: Op
pattern OpImageSampleFootprintNV = Op 5283

pattern OpEmitMeshTasksEXT :: Op
pattern OpEmitMeshTasksEXT = Op 5294

pattern OpSetMeshOutputsEXT :: Op
pattern OpSetMeshOutputsEXT = Op 5295

pattern OpGroupNonUniformPartitionNV :: Op
pattern OpGroupNonUniformPartitionNV = Op 5296

pattern OpWritePackedPrimitiveIndices4x8NV :: Op
pattern OpWritePackedPrimitiveIndices4x8NV = Op 5299

pattern OpFetchMicroTriangleVertexPositionNV :: Op
pattern OpFetchMicroTriangleVertexPositionNV = Op 5300

pattern OpFetchMicroTriangleVertexBarycentricNV :: Op
pattern OpFetchMicroTriangleVertexBarycentricNV = Op 5301

pattern OpReportIntersectionKHR :: Op
pattern OpReportIntersectionKHR = Op 5334

pattern OpReportIntersectionNV :: Op
pattern OpReportIntersectionNV = Op 5334

pattern OpIgnoreIntersectionNV :: Op
pattern OpIgnoreIntersectionNV = Op 5335

pattern OpTerminateRayNV :: Op
pattern OpTerminateRayNV = Op 5336

pattern OpTraceNV :: Op
pattern OpTraceNV = Op 5337

pattern OpTraceMotionNV :: Op
pattern OpTraceMotionNV = Op 5338

pattern OpTraceRayMotionNV :: Op
pattern OpTraceRayMotionNV = Op 5339

pattern OpRayQueryGetIntersectionTriangleVertexPositionsKHR :: Op
pattern OpRayQueryGetIntersectionTriangleVertexPositionsKHR = Op 5340

pattern OpTypeAccelerationStructureKHR :: Op
pattern OpTypeAccelerationStructureKHR = Op 5341

pattern OpTypeAccelerationStructureNV :: Op
pattern OpTypeAccelerationStructureNV = Op 5341

pattern OpExecuteCallableNV :: Op
pattern OpExecuteCallableNV = Op 5344

pattern OpTypeCooperativeMatrixNV :: Op
pattern OpTypeCooperativeMatrixNV = Op 5358

pattern OpCooperativeMatrixLoadNV :: Op
pattern OpCooperativeMatrixLoadNV = Op 5359

pattern OpCooperativeMatrixStoreNV :: Op
pattern OpCooperativeMatrixStoreNV = Op 5360

pattern OpCooperativeMatrixMulAddNV :: Op
pattern OpCooperativeMatrixMulAddNV = Op 5361

pattern OpCooperativeMatrixLengthNV :: Op
pattern OpCooperativeMatrixLengthNV = Op 5362

pattern OpBeginInvocationInterlockEXT :: Op
pattern OpBeginInvocationInterlockEXT = Op 5364

pattern OpEndInvocationInterlockEXT :: Op
pattern OpEndInvocationInterlockEXT = Op 5365

pattern OpDemoteToHelperInvocation :: Op
pattern OpDemoteToHelperInvocation = Op 5380

pattern OpDemoteToHelperInvocationEXT :: Op
pattern OpDemoteToHelperInvocationEXT = Op 5380

pattern OpIsHelperInvocationEXT :: Op
pattern OpIsHelperInvocationEXT = Op 5381

pattern OpConvertUToImageNV :: Op
pattern OpConvertUToImageNV = Op 5391

pattern OpConvertUToSamplerNV :: Op
pattern OpConvertUToSamplerNV = Op 5392

pattern OpConvertImageToUNV :: Op
pattern OpConvertImageToUNV = Op 5393

pattern OpConvertSamplerToUNV :: Op
pattern OpConvertSamplerToUNV = Op 5394

pattern OpConvertUToSampledImageNV :: Op
pattern OpConvertUToSampledImageNV = Op 5395

pattern OpConvertSampledImageToUNV :: Op
pattern OpConvertSampledImageToUNV = Op 5396

pattern OpSamplerImageAddressingModeNV :: Op
pattern OpSamplerImageAddressingModeNV = Op 5397

pattern OpRawAccessChainNV :: Op
pattern OpRawAccessChainNV = Op 5398

pattern OpSubgroupShuffleINTEL :: Op
pattern OpSubgroupShuffleINTEL = Op 5571

pattern OpSubgroupShuffleDownINTEL :: Op
pattern OpSubgroupShuffleDownINTEL = Op 5572

pattern OpSubgroupShuffleUpINTEL :: Op
pattern OpSubgroupShuffleUpINTEL = Op 5573

pattern OpSubgroupShuffleXorINTEL :: Op
pattern OpSubgroupShuffleXorINTEL = Op 5574

pattern OpSubgroupBlockReadINTEL :: Op
pattern OpSubgroupBlockReadINTEL = Op 5575

pattern OpSubgroupBlockWriteINTEL :: Op
pattern OpSubgroupBlockWriteINTEL = Op 5576

pattern OpSubgroupImageBlockReadINTEL :: Op
pattern OpSubgroupImageBlockReadINTEL = Op 5577

pattern OpSubgroupImageBlockWriteINTEL :: Op
pattern OpSubgroupImageBlockWriteINTEL = Op 5578

pattern OpSubgroupImageMediaBlockReadINTEL :: Op
pattern OpSubgroupImageMediaBlockReadINTEL = Op 5580

pattern OpSubgroupImageMediaBlockWriteINTEL :: Op
pattern OpSubgroupImageMediaBlockWriteINTEL = Op 5581

pattern OpUCountLeadingZerosINTEL :: Op
pattern OpUCountLeadingZerosINTEL = Op 5585

pattern OpUCountTrailingZerosINTEL :: Op
pattern OpUCountTrailingZerosINTEL = Op 5586

pattern OpAbsISubINTEL :: Op
pattern OpAbsISubINTEL = Op 5587

pattern OpAbsUSubINTEL :: Op
pattern OpAbsUSubINTEL = Op 5588

pattern OpIAddSatINTEL :: Op
pattern OpIAddSatINTEL = Op 5589

pattern OpUAddSatINTEL :: Op
pattern OpUAddSatINTEL = Op 5590

pattern OpIAverageINTEL :: Op
pattern OpIAverageINTEL = Op 5591

pattern OpUAverageINTEL :: Op
pattern OpUAverageINTEL = Op 5592

pattern OpIAverageRoundedINTEL :: Op
pattern OpIAverageRoundedINTEL = Op 5593

pattern OpUAverageRoundedINTEL :: Op
pattern OpUAverageRoundedINTEL = Op 5594

pattern OpISubSatINTEL :: Op
pattern OpISubSatINTEL = Op 5595

pattern OpUSubSatINTEL :: Op
pattern OpUSubSatINTEL = Op 5596

pattern OpIMul32x16INTEL :: Op
pattern OpIMul32x16INTEL = Op 5597

pattern OpUMul32x16INTEL :: Op
pattern OpUMul32x16INTEL = Op 5598

pattern OpConstantFunctionPointerINTEL :: Op
pattern OpConstantFunctionPointerINTEL = Op 5600

pattern OpFunctionPointerCallINTEL :: Op
pattern OpFunctionPointerCallINTEL = Op 5601

pattern OpAsmTargetINTEL :: Op
pattern OpAsmTargetINTEL = Op 5609

pattern OpAsmINTEL :: Op
pattern OpAsmINTEL = Op 5610

pattern OpAsmCallINTEL :: Op
pattern OpAsmCallINTEL = Op 5611

pattern OpAtomicFMinEXT :: Op
pattern OpAtomicFMinEXT = Op 5614

pattern OpAtomicFMaxEXT :: Op
pattern OpAtomicFMaxEXT = Op 5615

pattern OpAssumeTrueKHR :: Op
pattern OpAssumeTrueKHR = Op 5630

pattern OpExpectKHR :: Op
pattern OpExpectKHR = Op 5631

pattern OpDecorateString :: Op
pattern OpDecorateString = Op 5632

pattern OpDecorateStringGOOGLE :: Op
pattern OpDecorateStringGOOGLE = Op 5632

pattern OpMemberDecorateString :: Op
pattern OpMemberDecorateString = Op 5633

pattern OpMemberDecorateStringGOOGLE :: Op
pattern OpMemberDecorateStringGOOGLE = Op 5633

pattern OpVmeImageINTEL :: Op
pattern OpVmeImageINTEL = Op 5699

pattern OpTypeVmeImageINTEL :: Op
pattern OpTypeVmeImageINTEL = Op 5700

pattern OpTypeAvcImePayloadINTEL :: Op
pattern OpTypeAvcImePayloadINTEL = Op 5701

pattern OpTypeAvcRefPayloadINTEL :: Op
pattern OpTypeAvcRefPayloadINTEL = Op 5702

pattern OpTypeAvcSicPayloadINTEL :: Op
pattern OpTypeAvcSicPayloadINTEL = Op 5703

pattern OpTypeAvcMcePayloadINTEL :: Op
pattern OpTypeAvcMcePayloadINTEL = Op 5704

pattern OpTypeAvcMceResultINTEL :: Op
pattern OpTypeAvcMceResultINTEL = Op 5705

pattern OpTypeAvcImeResultINTEL :: Op
pattern OpTypeAvcImeResultINTEL = Op 5706

pattern OpTypeAvcImeResultSingleReferenceStreamoutINTEL :: Op
pattern OpTypeAvcImeResultSingleReferenceStreamoutINTEL = Op 5707

pattern OpTypeAvcImeResultDualReferenceStreamoutINTEL :: Op
pattern OpTypeAvcImeResultDualReferenceStreamoutINTEL = Op 5708

pattern OpTypeAvcImeSingleReferenceStreaminINTEL :: Op
pattern OpTypeAvcImeSingleReferenceStreaminINTEL = Op 5709

pattern OpTypeAvcImeDualReferenceStreaminINTEL :: Op
pattern OpTypeAvcImeDualReferenceStreaminINTEL = Op 5710

pattern OpTypeAvcRefResultINTEL :: Op
pattern OpTypeAvcRefResultINTEL = Op 5711

pattern OpTypeAvcSicResultINTEL :: Op
pattern OpTypeAvcSicResultINTEL = Op 5712

pattern OpSubgroupAvcMceGetDefaultInterBaseMultiReferencePenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultInterBaseMultiReferencePenaltyINTEL = Op 5713

pattern OpSubgroupAvcMceSetInterBaseMultiReferencePenaltyINTEL :: Op
pattern OpSubgroupAvcMceSetInterBaseMultiReferencePenaltyINTEL = Op 5714

pattern OpSubgroupAvcMceGetDefaultInterShapePenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultInterShapePenaltyINTEL = Op 5715

pattern OpSubgroupAvcMceSetInterShapePenaltyINTEL :: Op
pattern OpSubgroupAvcMceSetInterShapePenaltyINTEL = Op 5716

pattern OpSubgroupAvcMceGetDefaultInterDirectionPenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultInterDirectionPenaltyINTEL = Op 5717

pattern OpSubgroupAvcMceSetInterDirectionPenaltyINTEL :: Op
pattern OpSubgroupAvcMceSetInterDirectionPenaltyINTEL = Op 5718

pattern OpSubgroupAvcMceGetDefaultIntraLumaShapePenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultIntraLumaShapePenaltyINTEL = Op 5719

pattern OpSubgroupAvcMceGetDefaultInterMotionVectorCostTableINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultInterMotionVectorCostTableINTEL = Op 5720

pattern OpSubgroupAvcMceGetDefaultHighPenaltyCostTableINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultHighPenaltyCostTableINTEL = Op 5721

pattern OpSubgroupAvcMceGetDefaultMediumPenaltyCostTableINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultMediumPenaltyCostTableINTEL = Op 5722

pattern OpSubgroupAvcMceGetDefaultLowPenaltyCostTableINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultLowPenaltyCostTableINTEL = Op 5723

pattern OpSubgroupAvcMceSetMotionVectorCostFunctionINTEL :: Op
pattern OpSubgroupAvcMceSetMotionVectorCostFunctionINTEL = Op 5724

pattern OpSubgroupAvcMceGetDefaultIntraLumaModePenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultIntraLumaModePenaltyINTEL = Op 5725

pattern OpSubgroupAvcMceGetDefaultNonDcLumaIntraPenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultNonDcLumaIntraPenaltyINTEL = Op 5726

pattern OpSubgroupAvcMceGetDefaultIntraChromaModeBasePenaltyINTEL :: Op
pattern OpSubgroupAvcMceGetDefaultIntraChromaModeBasePenaltyINTEL = Op 5727

pattern OpSubgroupAvcMceSetAcOnlyHaarINTEL :: Op
pattern OpSubgroupAvcMceSetAcOnlyHaarINTEL = Op 5728

pattern OpSubgroupAvcMceSetSourceInterlacedFieldPolarityINTEL :: Op
pattern OpSubgroupAvcMceSetSourceInterlacedFieldPolarityINTEL = Op 5729

pattern OpSubgroupAvcMceSetSingleReferenceInterlacedFieldPolarityINTEL :: Op
pattern OpSubgroupAvcMceSetSingleReferenceInterlacedFieldPolarityINTEL = Op 5730

pattern OpSubgroupAvcMceSetDualReferenceInterlacedFieldPolaritiesINTEL :: Op
pattern OpSubgroupAvcMceSetDualReferenceInterlacedFieldPolaritiesINTEL = Op 5731

pattern OpSubgroupAvcMceConvertToImePayloadINTEL :: Op
pattern OpSubgroupAvcMceConvertToImePayloadINTEL = Op 5732

pattern OpSubgroupAvcMceConvertToImeResultINTEL :: Op
pattern OpSubgroupAvcMceConvertToImeResultINTEL = Op 5733

pattern OpSubgroupAvcMceConvertToRefPayloadINTEL :: Op
pattern OpSubgroupAvcMceConvertToRefPayloadINTEL = Op 5734

pattern OpSubgroupAvcMceConvertToRefResultINTEL :: Op
pattern OpSubgroupAvcMceConvertToRefResultINTEL = Op 5735

pattern OpSubgroupAvcMceConvertToSicPayloadINTEL :: Op
pattern OpSubgroupAvcMceConvertToSicPayloadINTEL = Op 5736

pattern OpSubgroupAvcMceConvertToSicResultINTEL :: Op
pattern OpSubgroupAvcMceConvertToSicResultINTEL = Op 5737

pattern OpSubgroupAvcMceGetMotionVectorsINTEL :: Op
pattern OpSubgroupAvcMceGetMotionVectorsINTEL = Op 5738

pattern OpSubgroupAvcMceGetInterDistortionsINTEL :: Op
pattern OpSubgroupAvcMceGetInterDistortionsINTEL = Op 5739

pattern OpSubgroupAvcMceGetBestInterDistortionsINTEL :: Op
pattern OpSubgroupAvcMceGetBestInterDistortionsINTEL = Op 5740

pattern OpSubgroupAvcMceGetInterMajorShapeINTEL :: Op
pattern OpSubgroupAvcMceGetInterMajorShapeINTEL = Op 5741

pattern OpSubgroupAvcMceGetInterMinorShapeINTEL :: Op
pattern OpSubgroupAvcMceGetInterMinorShapeINTEL = Op 5742

pattern OpSubgroupAvcMceGetInterDirectionsINTEL :: Op
pattern OpSubgroupAvcMceGetInterDirectionsINTEL = Op 5743

pattern OpSubgroupAvcMceGetInterMotionVectorCountINTEL :: Op
pattern OpSubgroupAvcMceGetInterMotionVectorCountINTEL = Op 5744

pattern OpSubgroupAvcMceGetInterReferenceIdsINTEL :: Op
pattern OpSubgroupAvcMceGetInterReferenceIdsINTEL = Op 5745

pattern OpSubgroupAvcMceGetInterReferenceInterlacedFieldPolaritiesINTEL :: Op
pattern OpSubgroupAvcMceGetInterReferenceInterlacedFieldPolaritiesINTEL = Op 5746

pattern OpSubgroupAvcImeInitializeINTEL :: Op
pattern OpSubgroupAvcImeInitializeINTEL = Op 5747

pattern OpSubgroupAvcImeSetSingleReferenceINTEL :: Op
pattern OpSubgroupAvcImeSetSingleReferenceINTEL = Op 5748

pattern OpSubgroupAvcImeSetDualReferenceINTEL :: Op
pattern OpSubgroupAvcImeSetDualReferenceINTEL = Op 5749

pattern OpSubgroupAvcImeRefWindowSizeINTEL :: Op
pattern OpSubgroupAvcImeRefWindowSizeINTEL = Op 5750

pattern OpSubgroupAvcImeAdjustRefOffsetINTEL :: Op
pattern OpSubgroupAvcImeAdjustRefOffsetINTEL = Op 5751

pattern OpSubgroupAvcImeConvertToMcePayloadINTEL :: Op
pattern OpSubgroupAvcImeConvertToMcePayloadINTEL = Op 5752

pattern OpSubgroupAvcImeSetMaxMotionVectorCountINTEL :: Op
pattern OpSubgroupAvcImeSetMaxMotionVectorCountINTEL = Op 5753

pattern OpSubgroupAvcImeSetUnidirectionalMixDisableINTEL :: Op
pattern OpSubgroupAvcImeSetUnidirectionalMixDisableINTEL = Op 5754

pattern OpSubgroupAvcImeSetEarlySearchTerminationThresholdINTEL :: Op
pattern OpSubgroupAvcImeSetEarlySearchTerminationThresholdINTEL = Op 5755

pattern OpSubgroupAvcImeSetWeightedSadINTEL :: Op
pattern OpSubgroupAvcImeSetWeightedSadINTEL = Op 5756

pattern OpSubgroupAvcImeEvaluateWithSingleReferenceINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithSingleReferenceINTEL = Op 5757

pattern OpSubgroupAvcImeEvaluateWithDualReferenceINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithDualReferenceINTEL = Op 5758

pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminINTEL = Op 5759

pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreaminINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreaminINTEL = Op 5760

pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreamoutINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreamoutINTEL = Op 5761

pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreamoutINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreamoutINTEL = Op 5762

pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminoutINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminoutINTEL = Op 5763

pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreaminoutINTEL :: Op
pattern OpSubgroupAvcImeEvaluateWithDualReferenceStreaminoutINTEL = Op 5764

pattern OpSubgroupAvcImeConvertToMceResultINTEL :: Op
pattern OpSubgroupAvcImeConvertToMceResultINTEL = Op 5765

pattern OpSubgroupAvcImeGetSingleReferenceStreaminINTEL :: Op
pattern OpSubgroupAvcImeGetSingleReferenceStreaminINTEL = Op 5766

pattern OpSubgroupAvcImeGetDualReferenceStreaminINTEL :: Op
pattern OpSubgroupAvcImeGetDualReferenceStreaminINTEL = Op 5767

pattern OpSubgroupAvcImeStripSingleReferenceStreamoutINTEL :: Op
pattern OpSubgroupAvcImeStripSingleReferenceStreamoutINTEL = Op 5768

pattern OpSubgroupAvcImeStripDualReferenceStreamoutINTEL :: Op
pattern OpSubgroupAvcImeStripDualReferenceStreamoutINTEL = Op 5769

pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeMotionVectorsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeMotionVectorsINTEL = Op 5770

pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeDistortionsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeDistortionsINTEL = Op 5771

pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeReferenceIdsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeReferenceIdsINTEL = Op 5772

pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeMotionVectorsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeMotionVectorsINTEL = Op 5773

pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeDistortionsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeDistortionsINTEL = Op 5774

pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeReferenceIdsINTEL :: Op
pattern OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeReferenceIdsINTEL = Op 5775

pattern OpSubgroupAvcImeGetBorderReachedINTEL :: Op
pattern OpSubgroupAvcImeGetBorderReachedINTEL = Op 5776

pattern OpSubgroupAvcImeGetTruncatedSearchIndicationINTEL :: Op
pattern OpSubgroupAvcImeGetTruncatedSearchIndicationINTEL = Op 5777

pattern OpSubgroupAvcImeGetUnidirectionalEarlySearchTerminationINTEL :: Op
pattern OpSubgroupAvcImeGetUnidirectionalEarlySearchTerminationINTEL = Op 5778

pattern OpSubgroupAvcImeGetWeightingPatternMinimumMotionVectorINTEL :: Op
pattern OpSubgroupAvcImeGetWeightingPatternMinimumMotionVectorINTEL = Op 5779

pattern OpSubgroupAvcImeGetWeightingPatternMinimumDistortionINTEL :: Op
pattern OpSubgroupAvcImeGetWeightingPatternMinimumDistortionINTEL = Op 5780

pattern OpSubgroupAvcFmeInitializeINTEL :: Op
pattern OpSubgroupAvcFmeInitializeINTEL = Op 5781

pattern OpSubgroupAvcBmeInitializeINTEL :: Op
pattern OpSubgroupAvcBmeInitializeINTEL = Op 5782

pattern OpSubgroupAvcRefConvertToMcePayloadINTEL :: Op
pattern OpSubgroupAvcRefConvertToMcePayloadINTEL = Op 5783

pattern OpSubgroupAvcRefSetBidirectionalMixDisableINTEL :: Op
pattern OpSubgroupAvcRefSetBidirectionalMixDisableINTEL = Op 5784

pattern OpSubgroupAvcRefSetBilinearFilterEnableINTEL :: Op
pattern OpSubgroupAvcRefSetBilinearFilterEnableINTEL = Op 5785

pattern OpSubgroupAvcRefEvaluateWithSingleReferenceINTEL :: Op
pattern OpSubgroupAvcRefEvaluateWithSingleReferenceINTEL = Op 5786

pattern OpSubgroupAvcRefEvaluateWithDualReferenceINTEL :: Op
pattern OpSubgroupAvcRefEvaluateWithDualReferenceINTEL = Op 5787

pattern OpSubgroupAvcRefEvaluateWithMultiReferenceINTEL :: Op
pattern OpSubgroupAvcRefEvaluateWithMultiReferenceINTEL = Op 5788

pattern OpSubgroupAvcRefEvaluateWithMultiReferenceInterlacedINTEL :: Op
pattern OpSubgroupAvcRefEvaluateWithMultiReferenceInterlacedINTEL = Op 5789

pattern OpSubgroupAvcRefConvertToMceResultINTEL :: Op
pattern OpSubgroupAvcRefConvertToMceResultINTEL = Op 5790

pattern OpSubgroupAvcSicInitializeINTEL :: Op
pattern OpSubgroupAvcSicInitializeINTEL = Op 5791

pattern OpSubgroupAvcSicConfigureSkcINTEL :: Op
pattern OpSubgroupAvcSicConfigureSkcINTEL = Op 5792

pattern OpSubgroupAvcSicConfigureIpeLumaINTEL :: Op
pattern OpSubgroupAvcSicConfigureIpeLumaINTEL = Op 5793

pattern OpSubgroupAvcSicConfigureIpeLumaChromaINTEL :: Op
pattern OpSubgroupAvcSicConfigureIpeLumaChromaINTEL = Op 5794

pattern OpSubgroupAvcSicGetMotionVectorMaskINTEL :: Op
pattern OpSubgroupAvcSicGetMotionVectorMaskINTEL = Op 5795

pattern OpSubgroupAvcSicConvertToMcePayloadINTEL :: Op
pattern OpSubgroupAvcSicConvertToMcePayloadINTEL = Op 5796

pattern OpSubgroupAvcSicSetIntraLumaShapePenaltyINTEL :: Op
pattern OpSubgroupAvcSicSetIntraLumaShapePenaltyINTEL = Op 5797

pattern OpSubgroupAvcSicSetIntraLumaModeCostFunctionINTEL :: Op
pattern OpSubgroupAvcSicSetIntraLumaModeCostFunctionINTEL = Op 5798

pattern OpSubgroupAvcSicSetIntraChromaModeCostFunctionINTEL :: Op
pattern OpSubgroupAvcSicSetIntraChromaModeCostFunctionINTEL = Op 5799

pattern OpSubgroupAvcSicSetBilinearFilterEnableINTEL :: Op
pattern OpSubgroupAvcSicSetBilinearFilterEnableINTEL = Op 5800

pattern OpSubgroupAvcSicSetSkcForwardTransformEnableINTEL :: Op
pattern OpSubgroupAvcSicSetSkcForwardTransformEnableINTEL = Op 5801

pattern OpSubgroupAvcSicSetBlockBasedRawSkipSadINTEL :: Op
pattern OpSubgroupAvcSicSetBlockBasedRawSkipSadINTEL = Op 5802

pattern OpSubgroupAvcSicEvaluateIpeINTEL :: Op
pattern OpSubgroupAvcSicEvaluateIpeINTEL = Op 5803

pattern OpSubgroupAvcSicEvaluateWithSingleReferenceINTEL :: Op
pattern OpSubgroupAvcSicEvaluateWithSingleReferenceINTEL = Op 5804

pattern OpSubgroupAvcSicEvaluateWithDualReferenceINTEL :: Op
pattern OpSubgroupAvcSicEvaluateWithDualReferenceINTEL = Op 5805

pattern OpSubgroupAvcSicEvaluateWithMultiReferenceINTEL :: Op
pattern OpSubgroupAvcSicEvaluateWithMultiReferenceINTEL = Op 5806

pattern OpSubgroupAvcSicEvaluateWithMultiReferenceInterlacedINTEL :: Op
pattern OpSubgroupAvcSicEvaluateWithMultiReferenceInterlacedINTEL = Op 5807

pattern OpSubgroupAvcSicConvertToMceResultINTEL :: Op
pattern OpSubgroupAvcSicConvertToMceResultINTEL = Op 5808

pattern OpSubgroupAvcSicGetIpeLumaShapeINTEL :: Op
pattern OpSubgroupAvcSicGetIpeLumaShapeINTEL = Op 5809

pattern OpSubgroupAvcSicGetBestIpeLumaDistortionINTEL :: Op
pattern OpSubgroupAvcSicGetBestIpeLumaDistortionINTEL = Op 5810

pattern OpSubgroupAvcSicGetBestIpeChromaDistortionINTEL :: Op
pattern OpSubgroupAvcSicGetBestIpeChromaDistortionINTEL = Op 5811

pattern OpSubgroupAvcSicGetPackedIpeLumaModesINTEL :: Op
pattern OpSubgroupAvcSicGetPackedIpeLumaModesINTEL = Op 5812

pattern OpSubgroupAvcSicGetIpeChromaModeINTEL :: Op
pattern OpSubgroupAvcSicGetIpeChromaModeINTEL = Op 5813

pattern OpSubgroupAvcSicGetPackedSkcLumaCountThresholdINTEL :: Op
pattern OpSubgroupAvcSicGetPackedSkcLumaCountThresholdINTEL = Op 5814

pattern OpSubgroupAvcSicGetPackedSkcLumaSumThresholdINTEL :: Op
pattern OpSubgroupAvcSicGetPackedSkcLumaSumThresholdINTEL = Op 5815

pattern OpSubgroupAvcSicGetInterRawSadsINTEL :: Op
pattern OpSubgroupAvcSicGetInterRawSadsINTEL = Op 5816

pattern OpVariableLengthArrayINTEL :: Op
pattern OpVariableLengthArrayINTEL = Op 5818

pattern OpSaveMemoryINTEL :: Op
pattern OpSaveMemoryINTEL = Op 5819

pattern OpRestoreMemoryINTEL :: Op
pattern OpRestoreMemoryINTEL = Op 5820

pattern OpArbitraryFloatSinCosPiINTEL :: Op
pattern OpArbitraryFloatSinCosPiINTEL = Op 5840

pattern OpArbitraryFloatCastINTEL :: Op
pattern OpArbitraryFloatCastINTEL = Op 5841

pattern OpArbitraryFloatCastFromIntINTEL :: Op
pattern OpArbitraryFloatCastFromIntINTEL = Op 5842

pattern OpArbitraryFloatCastToIntINTEL :: Op
pattern OpArbitraryFloatCastToIntINTEL = Op 5843

pattern OpArbitraryFloatAddINTEL :: Op
pattern OpArbitraryFloatAddINTEL = Op 5846

pattern OpArbitraryFloatSubINTEL :: Op
pattern OpArbitraryFloatSubINTEL = Op 5847

pattern OpArbitraryFloatMulINTEL :: Op
pattern OpArbitraryFloatMulINTEL = Op 5848

pattern OpArbitraryFloatDivINTEL :: Op
pattern OpArbitraryFloatDivINTEL = Op 5849

pattern OpArbitraryFloatGTINTEL :: Op
pattern OpArbitraryFloatGTINTEL = Op 5850

pattern OpArbitraryFloatGEINTEL :: Op
pattern OpArbitraryFloatGEINTEL = Op 5851

pattern OpArbitraryFloatLTINTEL :: Op
pattern OpArbitraryFloatLTINTEL = Op 5852

pattern OpArbitraryFloatLEINTEL :: Op
pattern OpArbitraryFloatLEINTEL = Op 5853

pattern OpArbitraryFloatEQINTEL :: Op
pattern OpArbitraryFloatEQINTEL = Op 5854

pattern OpArbitraryFloatRecipINTEL :: Op
pattern OpArbitraryFloatRecipINTEL = Op 5855

pattern OpArbitraryFloatRSqrtINTEL :: Op
pattern OpArbitraryFloatRSqrtINTEL = Op 5856

pattern OpArbitraryFloatCbrtINTEL :: Op
pattern OpArbitraryFloatCbrtINTEL = Op 5857

pattern OpArbitraryFloatHypotINTEL :: Op
pattern OpArbitraryFloatHypotINTEL = Op 5858

pattern OpArbitraryFloatSqrtINTEL :: Op
pattern OpArbitraryFloatSqrtINTEL = Op 5859

pattern OpArbitraryFloatLogINTEL :: Op
pattern OpArbitraryFloatLogINTEL = Op 5860

pattern OpArbitraryFloatLog2INTEL :: Op
pattern OpArbitraryFloatLog2INTEL = Op 5861

pattern OpArbitraryFloatLog10INTEL :: Op
pattern OpArbitraryFloatLog10INTEL = Op 5862

pattern OpArbitraryFloatLog1pINTEL :: Op
pattern OpArbitraryFloatLog1pINTEL = Op 5863

pattern OpArbitraryFloatExpINTEL :: Op
pattern OpArbitraryFloatExpINTEL = Op 5864

pattern OpArbitraryFloatExp2INTEL :: Op
pattern OpArbitraryFloatExp2INTEL = Op 5865

pattern OpArbitraryFloatExp10INTEL :: Op
pattern OpArbitraryFloatExp10INTEL = Op 5866

pattern OpArbitraryFloatExpm1INTEL :: Op
pattern OpArbitraryFloatExpm1INTEL = Op 5867

pattern OpArbitraryFloatSinINTEL :: Op
pattern OpArbitraryFloatSinINTEL = Op 5868

pattern OpArbitraryFloatCosINTEL :: Op
pattern OpArbitraryFloatCosINTEL = Op 5869

pattern OpArbitraryFloatSinCosINTEL :: Op
pattern OpArbitraryFloatSinCosINTEL = Op 5870

pattern OpArbitraryFloatSinPiINTEL :: Op
pattern OpArbitraryFloatSinPiINTEL = Op 5871

pattern OpArbitraryFloatCosPiINTEL :: Op
pattern OpArbitraryFloatCosPiINTEL = Op 5872

pattern OpArbitraryFloatASinINTEL :: Op
pattern OpArbitraryFloatASinINTEL = Op 5873

pattern OpArbitraryFloatASinPiINTEL :: Op
pattern OpArbitraryFloatASinPiINTEL = Op 5874

pattern OpArbitraryFloatACosINTEL :: Op
pattern OpArbitraryFloatACosINTEL = Op 5875

pattern OpArbitraryFloatACosPiINTEL :: Op
pattern OpArbitraryFloatACosPiINTEL = Op 5876

pattern OpArbitraryFloatATanINTEL :: Op
pattern OpArbitraryFloatATanINTEL = Op 5877

pattern OpArbitraryFloatATanPiINTEL :: Op
pattern OpArbitraryFloatATanPiINTEL = Op 5878

pattern OpArbitraryFloatATan2INTEL :: Op
pattern OpArbitraryFloatATan2INTEL = Op 5879

pattern OpArbitraryFloatPowINTEL :: Op
pattern OpArbitraryFloatPowINTEL = Op 5880

pattern OpArbitraryFloatPowRINTEL :: Op
pattern OpArbitraryFloatPowRINTEL = Op 5881

pattern OpArbitraryFloatPowNINTEL :: Op
pattern OpArbitraryFloatPowNINTEL = Op 5882

pattern OpLoopControlINTEL :: Op
pattern OpLoopControlINTEL = Op 5887

pattern OpAliasDomainDeclINTEL :: Op
pattern OpAliasDomainDeclINTEL = Op 5911

pattern OpAliasScopeDeclINTEL :: Op
pattern OpAliasScopeDeclINTEL = Op 5912

pattern OpAliasScopeListDeclINTEL :: Op
pattern OpAliasScopeListDeclINTEL = Op 5913

pattern OpFixedSqrtINTEL :: Op
pattern OpFixedSqrtINTEL = Op 5923

pattern OpFixedRecipINTEL :: Op
pattern OpFixedRecipINTEL = Op 5924

pattern OpFixedRsqrtINTEL :: Op
pattern OpFixedRsqrtINTEL = Op 5925

pattern OpFixedSinINTEL :: Op
pattern OpFixedSinINTEL = Op 5926

pattern OpFixedCosINTEL :: Op
pattern OpFixedCosINTEL = Op 5927

pattern OpFixedSinCosINTEL :: Op
pattern OpFixedSinCosINTEL = Op 5928

pattern OpFixedSinPiINTEL :: Op
pattern OpFixedSinPiINTEL = Op 5929

pattern OpFixedCosPiINTEL :: Op
pattern OpFixedCosPiINTEL = Op 5930

pattern OpFixedSinCosPiINTEL :: Op
pattern OpFixedSinCosPiINTEL = Op 5931

pattern OpFixedLogINTEL :: Op
pattern OpFixedLogINTEL = Op 5932

pattern OpFixedExpINTEL :: Op
pattern OpFixedExpINTEL = Op 5933

pattern OpPtrCastToCrossWorkgroupINTEL :: Op
pattern OpPtrCastToCrossWorkgroupINTEL = Op 5934

pattern OpCrossWorkgroupCastToPtrINTEL :: Op
pattern OpCrossWorkgroupCastToPtrINTEL = Op 5938

pattern OpReadPipeBlockingINTEL :: Op
pattern OpReadPipeBlockingINTEL = Op 5946

pattern OpWritePipeBlockingINTEL :: Op
pattern OpWritePipeBlockingINTEL = Op 5947

pattern OpFPGARegINTEL :: Op
pattern OpFPGARegINTEL = Op 5949

pattern OpRayQueryGetRayTMinKHR :: Op
pattern OpRayQueryGetRayTMinKHR = Op 6016

pattern OpRayQueryGetRayFlagsKHR :: Op
pattern OpRayQueryGetRayFlagsKHR = Op 6017

pattern OpRayQueryGetIntersectionTKHR :: Op
pattern OpRayQueryGetIntersectionTKHR = Op 6018

pattern OpRayQueryGetIntersectionInstanceCustomIndexKHR :: Op
pattern OpRayQueryGetIntersectionInstanceCustomIndexKHR = Op 6019

pattern OpRayQueryGetIntersectionInstanceIdKHR :: Op
pattern OpRayQueryGetIntersectionInstanceIdKHR = Op 6020

pattern OpRayQueryGetIntersectionInstanceShaderBindingTableRecordOffsetKHR :: Op
pattern OpRayQueryGetIntersectionInstanceShaderBindingTableRecordOffsetKHR = Op 6021

pattern OpRayQueryGetIntersectionGeometryIndexKHR :: Op
pattern OpRayQueryGetIntersectionGeometryIndexKHR = Op 6022

pattern OpRayQueryGetIntersectionPrimitiveIndexKHR :: Op
pattern OpRayQueryGetIntersectionPrimitiveIndexKHR = Op 6023

pattern OpRayQueryGetIntersectionBarycentricsKHR :: Op
pattern OpRayQueryGetIntersectionBarycentricsKHR = Op 6024

pattern OpRayQueryGetIntersectionFrontFaceKHR :: Op
pattern OpRayQueryGetIntersectionFrontFaceKHR = Op 6025

pattern OpRayQueryGetIntersectionCandidateAABBOpaqueKHR :: Op
pattern OpRayQueryGetIntersectionCandidateAABBOpaqueKHR = Op 6026

pattern OpRayQueryGetIntersectionObjectRayDirectionKHR :: Op
pattern OpRayQueryGetIntersectionObjectRayDirectionKHR = Op 6027

pattern OpRayQueryGetIntersectionObjectRayOriginKHR :: Op
pattern OpRayQueryGetIntersectionObjectRayOriginKHR = Op 6028

pattern OpRayQueryGetWorldRayDirectionKHR :: Op
pattern OpRayQueryGetWorldRayDirectionKHR = Op 6029

pattern OpRayQueryGetWorldRayOriginKHR :: Op
pattern OpRayQueryGetWorldRayOriginKHR = Op 6030

pattern OpRayQueryGetIntersectionObjectToWorldKHR :: Op
pattern OpRayQueryGetIntersectionObjectToWorldKHR = Op 6031

pattern OpRayQueryGetIntersectionWorldToObjectKHR :: Op
pattern OpRayQueryGetIntersectionWorldToObjectKHR = Op 6032

pattern OpAtomicFAddEXT :: Op
pattern OpAtomicFAddEXT = Op 6035

pattern OpTypeBufferSurfaceINTEL :: Op
pattern OpTypeBufferSurfaceINTEL = Op 6086

pattern OpTypeStructContinuedINTEL :: Op
pattern OpTypeStructContinuedINTEL = Op 6090

pattern OpConstantCompositeContinuedINTEL :: Op
pattern OpConstantCompositeContinuedINTEL = Op 6091

pattern OpSpecConstantCompositeContinuedINTEL :: Op
pattern OpSpecConstantCompositeContinuedINTEL = Op 6092

pattern OpCompositeConstructContinuedINTEL :: Op
pattern OpCompositeConstructContinuedINTEL = Op 6096

pattern OpConvertFToBF16INTEL :: Op
pattern OpConvertFToBF16INTEL = Op 6116

pattern OpConvertBF16ToFINTEL :: Op
pattern OpConvertBF16ToFINTEL = Op 6117

pattern OpControlBarrierArriveINTEL :: Op
pattern OpControlBarrierArriveINTEL = Op 6142

pattern OpControlBarrierWaitINTEL :: Op
pattern OpControlBarrierWaitINTEL = Op 6143

pattern OpGroupIMulKHR :: Op
pattern OpGroupIMulKHR = Op 6401

pattern OpGroupFMulKHR :: Op
pattern OpGroupFMulKHR = Op 6402

pattern OpGroupBitwiseAndKHR :: Op
pattern OpGroupBitwiseAndKHR = Op 6403

pattern OpGroupBitwiseOrKHR :: Op
pattern OpGroupBitwiseOrKHR = Op 6404

pattern OpGroupBitwiseXorKHR :: Op
pattern OpGroupBitwiseXorKHR = Op 6405

pattern OpGroupLogicalAndKHR :: Op
pattern OpGroupLogicalAndKHR = Op 6406

pattern OpGroupLogicalOrKHR :: Op
pattern OpGroupLogicalOrKHR = Op 6407

pattern OpGroupLogicalXorKHR :: Op
pattern OpGroupLogicalXorKHR = Op 6408

pattern OpMaskedGatherINTEL :: Op
pattern OpMaskedGatherINTEL = Op 6428

pattern OpMaskedScatterINTEL :: Op
pattern OpMaskedScatterINTEL = Op 6429