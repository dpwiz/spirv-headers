{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.Scope where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype Scope = Scope Int32
  deriving newtype (Eq, Ord, Storable)

instance Show Scope where
  showsPrec p (Scope v) = case v of
    0 -> showString "CrossDevice"
    1 -> showString "Device"
    2 -> showString "Workgroup"
    3 -> showString "Subgroup"
    4 -> showString "Invocation"
    5 -> showString "QueueFamily"
    6 -> showString "ShaderCallKHR"
    x -> showParen (p > 10) $ showString "Scope " . showsPrec (p + 1) x

pattern CrossDevice :: Scope
pattern CrossDevice = Scope 0

pattern Device :: Scope
pattern Device = Scope 1

pattern Workgroup :: Scope
pattern Workgroup = Scope 2

pattern Subgroup :: Scope
pattern Subgroup = Scope 3

pattern Invocation :: Scope
pattern Invocation = Scope 4

pattern QueueFamily :: Scope
pattern QueueFamily = Scope 5

pattern QueueFamilyKHR :: Scope
pattern QueueFamilyKHR = Scope 5

pattern ShaderCallKHR :: Scope
pattern ShaderCallKHR = Scope 6