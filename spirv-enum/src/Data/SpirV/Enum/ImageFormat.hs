{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ImageFormat where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype ImageFormat = ImageFormat Int32
  deriving newtype (Eq, Ord, Storable)

instance Show ImageFormat where
  showsPrec p (ImageFormat v) = case v of
    0 -> showString "Unknown"
    1 -> showString "Rgba32f"
    2 -> showString "Rgba16f"
    3 -> showString "R32f"
    4 -> showString "Rgba8"
    5 -> showString "Rgba8Snorm"
    6 -> showString "Rg32f"
    7 -> showString "Rg16f"
    8 -> showString "R11fG11fB10f"
    9 -> showString "R16f"
    10 -> showString "Rgba16"
    11 -> showString "Rgb10A2"
    12 -> showString "Rg16"
    13 -> showString "Rg8"
    14 -> showString "R16"
    15 -> showString "R8"
    16 -> showString "Rgba16Snorm"
    17 -> showString "Rg16Snorm"
    18 -> showString "Rg8Snorm"
    19 -> showString "R16Snorm"
    20 -> showString "R8Snorm"
    21 -> showString "Rgba32i"
    22 -> showString "Rgba16i"
    23 -> showString "Rgba8i"
    24 -> showString "R32i"
    25 -> showString "Rg32i"
    26 -> showString "Rg16i"
    27 -> showString "Rg8i"
    28 -> showString "R16i"
    29 -> showString "R8i"
    30 -> showString "Rgba32ui"
    31 -> showString "Rgba16ui"
    32 -> showString "Rgba8ui"
    33 -> showString "R32ui"
    34 -> showString "Rgb10a2ui"
    35 -> showString "Rg32ui"
    36 -> showString "Rg16ui"
    37 -> showString "Rg8ui"
    38 -> showString "R16ui"
    39 -> showString "R8ui"
    40 -> showString "R64ui"
    41 -> showString "R64i"
    x -> showParen (p > 10) $ showString "ImageFormat " . showsPrec (p + 1) x

pattern Unknown :: ImageFormat
pattern Unknown = ImageFormat 0

pattern Rgba32f :: ImageFormat
pattern Rgba32f = ImageFormat 1

pattern Rgba16f :: ImageFormat
pattern Rgba16f = ImageFormat 2

pattern R32f :: ImageFormat
pattern R32f = ImageFormat 3

pattern Rgba8 :: ImageFormat
pattern Rgba8 = ImageFormat 4

pattern Rgba8Snorm :: ImageFormat
pattern Rgba8Snorm = ImageFormat 5

pattern Rg32f :: ImageFormat
pattern Rg32f = ImageFormat 6

pattern Rg16f :: ImageFormat
pattern Rg16f = ImageFormat 7

pattern R11fG11fB10f :: ImageFormat
pattern R11fG11fB10f = ImageFormat 8

pattern R16f :: ImageFormat
pattern R16f = ImageFormat 9

pattern Rgba16 :: ImageFormat
pattern Rgba16 = ImageFormat 10

pattern Rgb10A2 :: ImageFormat
pattern Rgb10A2 = ImageFormat 11

pattern Rg16 :: ImageFormat
pattern Rg16 = ImageFormat 12

pattern Rg8 :: ImageFormat
pattern Rg8 = ImageFormat 13

pattern R16 :: ImageFormat
pattern R16 = ImageFormat 14

pattern R8 :: ImageFormat
pattern R8 = ImageFormat 15

pattern Rgba16Snorm :: ImageFormat
pattern Rgba16Snorm = ImageFormat 16

pattern Rg16Snorm :: ImageFormat
pattern Rg16Snorm = ImageFormat 17

pattern Rg8Snorm :: ImageFormat
pattern Rg8Snorm = ImageFormat 18

pattern R16Snorm :: ImageFormat
pattern R16Snorm = ImageFormat 19

pattern R8Snorm :: ImageFormat
pattern R8Snorm = ImageFormat 20

pattern Rgba32i :: ImageFormat
pattern Rgba32i = ImageFormat 21

pattern Rgba16i :: ImageFormat
pattern Rgba16i = ImageFormat 22

pattern Rgba8i :: ImageFormat
pattern Rgba8i = ImageFormat 23

pattern R32i :: ImageFormat
pattern R32i = ImageFormat 24

pattern Rg32i :: ImageFormat
pattern Rg32i = ImageFormat 25

pattern Rg16i :: ImageFormat
pattern Rg16i = ImageFormat 26

pattern Rg8i :: ImageFormat
pattern Rg8i = ImageFormat 27

pattern R16i :: ImageFormat
pattern R16i = ImageFormat 28

pattern R8i :: ImageFormat
pattern R8i = ImageFormat 29

pattern Rgba32ui :: ImageFormat
pattern Rgba32ui = ImageFormat 30

pattern Rgba16ui :: ImageFormat
pattern Rgba16ui = ImageFormat 31

pattern Rgba8ui :: ImageFormat
pattern Rgba8ui = ImageFormat 32

pattern R32ui :: ImageFormat
pattern R32ui = ImageFormat 33

pattern Rgb10a2ui :: ImageFormat
pattern Rgb10a2ui = ImageFormat 34

pattern Rg32ui :: ImageFormat
pattern Rg32ui = ImageFormat 35

pattern Rg16ui :: ImageFormat
pattern Rg16ui = ImageFormat 36

pattern Rg8ui :: ImageFormat
pattern Rg8ui = ImageFormat 37

pattern R16ui :: ImageFormat
pattern R16ui = ImageFormat 38

pattern R8ui :: ImageFormat
pattern R8ui = ImageFormat 39

pattern R64ui :: ImageFormat
pattern R64ui = ImageFormat 40

pattern R64i :: ImageFormat
pattern R64i = ImageFormat 41