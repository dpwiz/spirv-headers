{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.SourceLanguage where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype SourceLanguage = SourceLanguage Int32
  deriving newtype (Eq, Ord, Storable)

instance Show SourceLanguage where
  showsPrec p (SourceLanguage v) = case v of
    0 -> showString "Unknown"
    1 -> showString "ESSL"
    2 -> showString "GLSL"
    3 -> showString "OpenCL_C"
    4 -> showString "OpenCL_CPP"
    5 -> showString "HLSL"
    6 -> showString "CPP_for_OpenCL"
    7 -> showString "SYCL"
    8 -> showString "HERO_C"
    9 -> showString "NZSL"
    10 -> showString "WGSL"
    11 -> showString "Slang"
    12 -> showString "Zig"
    x -> showParen (p > 10) $ showString "SourceLanguage " . showsPrec (p + 1) x

pattern Unknown :: SourceLanguage
pattern Unknown = SourceLanguage 0

pattern ESSL :: SourceLanguage
pattern ESSL = SourceLanguage 1

pattern GLSL :: SourceLanguage
pattern GLSL = SourceLanguage 2

pattern OpenCL_C :: SourceLanguage
pattern OpenCL_C = SourceLanguage 3

pattern OpenCL_CPP :: SourceLanguage
pattern OpenCL_CPP = SourceLanguage 4

pattern HLSL :: SourceLanguage
pattern HLSL = SourceLanguage 5

pattern CPP_for_OpenCL :: SourceLanguage
pattern CPP_for_OpenCL = SourceLanguage 6

pattern SYCL :: SourceLanguage
pattern SYCL = SourceLanguage 7

pattern HERO_C :: SourceLanguage
pattern HERO_C = SourceLanguage 8

pattern NZSL :: SourceLanguage
pattern NZSL = SourceLanguage 9

pattern WGSL :: SourceLanguage
pattern WGSL = SourceLanguage 10

pattern Slang :: SourceLanguage
pattern Slang = SourceLanguage 11

pattern Zig :: SourceLanguage
pattern Zig = SourceLanguage 12