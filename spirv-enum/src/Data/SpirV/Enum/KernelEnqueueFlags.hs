{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.KernelEnqueueFlags where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype KernelEnqueueFlags = KernelEnqueueFlags Int32
  deriving newtype (Eq, Ord, Storable)

instance Show KernelEnqueueFlags where
  showsPrec p (KernelEnqueueFlags v) = case v of
    0 -> showString "NoWait"
    1 -> showString "WaitKernel"
    2 -> showString "WaitWorkGroup"
    x -> showParen (p > 10) $ showString "KernelEnqueueFlags " . showsPrec (p + 1) x

pattern NoWait :: KernelEnqueueFlags
pattern NoWait = KernelEnqueueFlags 0

pattern WaitKernel :: KernelEnqueueFlags
pattern WaitKernel = KernelEnqueueFlags 1

pattern WaitWorkGroup :: KernelEnqueueFlags
pattern WaitWorkGroup = KernelEnqueueFlags 2