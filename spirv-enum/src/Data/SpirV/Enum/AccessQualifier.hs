{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.AccessQualifier where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype AccessQualifier = AccessQualifier Int32
  deriving newtype (Eq, Ord, Storable)

instance Show AccessQualifier where
  showsPrec p (AccessQualifier v) = case v of
    0 -> showString "ReadOnly"
    1 -> showString "WriteOnly"
    2 -> showString "ReadWrite"
    x -> showParen (p > 10) $ showString "AccessQualifier " . showsPrec (p + 1) x

pattern ReadOnly :: AccessQualifier
pattern ReadOnly = AccessQualifier 0

pattern WriteOnly :: AccessQualifier
pattern WriteOnly = AccessQualifier 1

pattern ReadWrite :: AccessQualifier
pattern ReadWrite = AccessQualifier 2