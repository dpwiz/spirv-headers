{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ImageOperands where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type ImageOperands = ImageOperandsBits

newtype ImageOperandsBits = ImageOperandsBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup ImageOperands where
  (ImageOperandsBits a) <> (ImageOperandsBits b) = ImageOperandsBits (a .|. b)

instance Monoid ImageOperands where
  mempty = ImageOperandsBits 0

pattern Bias :: ImageOperandsBits
pattern Bias = ImageOperandsBits 0x00000001

pattern Lod :: ImageOperandsBits
pattern Lod = ImageOperandsBits 0x00000002

pattern Grad :: ImageOperandsBits
pattern Grad = ImageOperandsBits 0x00000004

pattern ConstOffset :: ImageOperandsBits
pattern ConstOffset = ImageOperandsBits 0x00000008

pattern Offset :: ImageOperandsBits
pattern Offset = ImageOperandsBits 0x00000010

pattern ConstOffsets :: ImageOperandsBits
pattern ConstOffsets = ImageOperandsBits 0x00000020

pattern Sample :: ImageOperandsBits
pattern Sample = ImageOperandsBits 0x00000040

pattern MinLod :: ImageOperandsBits
pattern MinLod = ImageOperandsBits 0x00000080

pattern MakeTexelAvailable :: ImageOperandsBits
pattern MakeTexelAvailable = ImageOperandsBits 0x00000100

pattern MakeTexelAvailableKHR :: ImageOperandsBits
pattern MakeTexelAvailableKHR = ImageOperandsBits 0x00000100

pattern MakeTexelVisible :: ImageOperandsBits
pattern MakeTexelVisible = ImageOperandsBits 0x00000200

pattern MakeTexelVisibleKHR :: ImageOperandsBits
pattern MakeTexelVisibleKHR = ImageOperandsBits 0x00000200

pattern NonPrivateTexel :: ImageOperandsBits
pattern NonPrivateTexel = ImageOperandsBits 0x00000400

pattern NonPrivateTexelKHR :: ImageOperandsBits
pattern NonPrivateTexelKHR = ImageOperandsBits 0x00000400

pattern VolatileTexel :: ImageOperandsBits
pattern VolatileTexel = ImageOperandsBits 0x00000800

pattern VolatileTexelKHR :: ImageOperandsBits
pattern VolatileTexelKHR = ImageOperandsBits 0x00000800

pattern SignExtend :: ImageOperandsBits
pattern SignExtend = ImageOperandsBits 0x00001000

pattern ZeroExtend :: ImageOperandsBits
pattern ZeroExtend = ImageOperandsBits 0x00002000

pattern Nontemporal :: ImageOperandsBits
pattern Nontemporal = ImageOperandsBits 0x00004000

pattern Offsets :: ImageOperandsBits
pattern Offsets = ImageOperandsBits 0x00010000