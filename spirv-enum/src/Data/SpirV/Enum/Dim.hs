{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.Dim where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype Dim = Dim Int32
  deriving newtype (Eq, Ord, Storable)

instance Show Dim where
  showsPrec p (Dim v) = case v of
    0 -> showString "Dim1D"
    1 -> showString "Dim2D"
    2 -> showString "Dim3D"
    3 -> showString "Cube"
    4 -> showString "Rect"
    5 -> showString "Buffer"
    6 -> showString "SubpassData"
    4173 -> showString "TileImageDataEXT"
    x -> showParen (p > 10) $ showString "Dim " . showsPrec (p + 1) x

pattern Dim1D :: Dim
pattern Dim1D = Dim 0

pattern Dim2D :: Dim
pattern Dim2D = Dim 1

pattern Dim3D :: Dim
pattern Dim3D = Dim 2

pattern Cube :: Dim
pattern Cube = Dim 3

pattern Rect :: Dim
pattern Rect = Dim 4

pattern Buffer :: Dim
pattern Buffer = Dim 5

pattern SubpassData :: Dim
pattern SubpassData = Dim 6

pattern TileImageDataEXT :: Dim
pattern TileImageDataEXT = Dim 4173