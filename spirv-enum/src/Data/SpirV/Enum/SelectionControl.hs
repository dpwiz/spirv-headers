{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.SelectionControl where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type SelectionControl = SelectionControlBits

newtype SelectionControlBits = SelectionControlBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup SelectionControl where
  (SelectionControlBits a) <> (SelectionControlBits b) = SelectionControlBits (a .|. b)

instance Monoid SelectionControl where
  mempty = SelectionControlBits 0

pattern Flatten :: SelectionControlBits
pattern Flatten = SelectionControlBits 0x00000001

pattern DontFlatten :: SelectionControlBits
pattern DontFlatten = SelectionControlBits 0x00000002