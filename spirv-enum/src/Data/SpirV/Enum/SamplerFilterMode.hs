{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.SamplerFilterMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype SamplerFilterMode = SamplerFilterMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show SamplerFilterMode where
  showsPrec p (SamplerFilterMode v) = case v of
    0 -> showString "Nearest"
    1 -> showString "Linear"
    x -> showParen (p > 10) $ showString "SamplerFilterMode " . showsPrec (p + 1) x

pattern Nearest :: SamplerFilterMode
pattern Nearest = SamplerFilterMode 0

pattern Linear :: SamplerFilterMode
pattern Linear = SamplerFilterMode 1