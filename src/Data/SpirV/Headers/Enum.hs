{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StrictData #-}

module Data.SpirV.Headers.Enum
  ( decode
  , SpirvJson(..)
  , Spv(..)
  , Meta(..)
  , Enum(..)
  , Type(..)
  ) where

import Prelude hiding (Enum)

import Data.Aeson (eitherDecodeFileStrict)
import Data.Aeson.Types (FromJSON(..), Options(..), defaultOptions, genericParseJSON)
import Data.Char (toUpper)
import Data.Map.Strict (Map)
import Data.Text (Text)
import Data.Word (Word32)
import GHC.Generics (Generic)

decode :: FilePath -> IO (Either String Spv)
decode fp = fmap spv <$> eitherDecodeFileStrict fp

newtype SpirvJson = SpirvJson
  { spv :: Spv
  }
  deriving (Eq, Show, Generic)

instance FromJSON SpirvJson

data Spv = Spv
  { meta :: Meta
  , enum :: [Enum]
  }
  deriving (Eq, Show, Generic)

instance FromJSON Spv

data Meta = Meta
  { comment :: [[Text]]
  , magicNumber :: Int
  , version :: Int
  , revision :: Int
  , opCodeMask :: Word32
  , wordCountShift :: Int
  }
  deriving (Eq, Show, Generic)

instance FromJSON Meta where
  parseJSON = genericParseJSON pascalCase

data Enum = Enum
  { name :: Text
  , type_ :: Type
  , values :: Map Text Integer
  }
  deriving (Eq, Show, Generic)

instance FromJSON Enum where
  parseJSON = genericParseJSON pascalCase

data Type
  = Value
  | Bit
  deriving (Eq, Show, Generic)

instance FromJSON Type

pascalCase :: Options
pascalCase = defaultOptions
  { fieldLabelModifier = \case
      [] -> []
      "type_" -> "Type"
      x : xs -> toUpper x : xs
  }
