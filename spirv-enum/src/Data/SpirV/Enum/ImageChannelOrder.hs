{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ImageChannelOrder where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype ImageChannelOrder = ImageChannelOrder Int32
  deriving newtype (Eq, Ord, Storable)

instance Show ImageChannelOrder where
  showsPrec p (ImageChannelOrder v) = case v of
    0 -> showString "R"
    1 -> showString "A"
    2 -> showString "RG"
    3 -> showString "RA"
    4 -> showString "RGB"
    5 -> showString "RGBA"
    6 -> showString "BGRA"
    7 -> showString "ARGB"
    8 -> showString "Intensity"
    9 -> showString "Luminance"
    10 -> showString "Rx"
    11 -> showString "RGx"
    12 -> showString "RGBx"
    13 -> showString "Depth"
    14 -> showString "DepthStencil"
    15 -> showString "SRGB"
    16 -> showString "SRGBx"
    17 -> showString "SRGBA"
    18 -> showString "SBGRA"
    19 -> showString "ABGR"
    x -> showParen (p > 10) $ showString "ImageChannelOrder " . showsPrec (p + 1) x

pattern R :: ImageChannelOrder
pattern R = ImageChannelOrder 0

pattern A :: ImageChannelOrder
pattern A = ImageChannelOrder 1

pattern RG :: ImageChannelOrder
pattern RG = ImageChannelOrder 2

pattern RA :: ImageChannelOrder
pattern RA = ImageChannelOrder 3

pattern RGB :: ImageChannelOrder
pattern RGB = ImageChannelOrder 4

pattern RGBA :: ImageChannelOrder
pattern RGBA = ImageChannelOrder 5

pattern BGRA :: ImageChannelOrder
pattern BGRA = ImageChannelOrder 6

pattern ARGB :: ImageChannelOrder
pattern ARGB = ImageChannelOrder 7

pattern Intensity :: ImageChannelOrder
pattern Intensity = ImageChannelOrder 8

pattern Luminance :: ImageChannelOrder
pattern Luminance = ImageChannelOrder 9

pattern Rx :: ImageChannelOrder
pattern Rx = ImageChannelOrder 10

pattern RGx :: ImageChannelOrder
pattern RGx = ImageChannelOrder 11

pattern RGBx :: ImageChannelOrder
pattern RGBx = ImageChannelOrder 12

pattern Depth :: ImageChannelOrder
pattern Depth = ImageChannelOrder 13

pattern DepthStencil :: ImageChannelOrder
pattern DepthStencil = ImageChannelOrder 14

pattern SRGB :: ImageChannelOrder
pattern SRGB = ImageChannelOrder 15

pattern SRGBx :: ImageChannelOrder
pattern SRGBx = ImageChannelOrder 16

pattern SRGBA :: ImageChannelOrder
pattern SRGBA = ImageChannelOrder 17

pattern SBGRA :: ImageChannelOrder
pattern SBGRA = ImageChannelOrder 18

pattern ABGR :: ImageChannelOrder
pattern ABGR = ImageChannelOrder 19