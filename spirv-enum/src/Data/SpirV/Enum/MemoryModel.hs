{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.MemoryModel where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype MemoryModel = MemoryModel Int32
  deriving newtype (Eq, Ord, Storable)

instance Show MemoryModel where
  showsPrec p (MemoryModel v) = case v of
    0 -> showString "Simple"
    1 -> showString "GLSL450"
    2 -> showString "OpenCL"
    3 -> showString "Vulkan"
    x -> showParen (p > 10) $ showString "MemoryModel " . showsPrec (p + 1) x

pattern Simple :: MemoryModel
pattern Simple = MemoryModel 0

pattern GLSL450 :: MemoryModel
pattern GLSL450 = MemoryModel 1

pattern OpenCL :: MemoryModel
pattern OpenCL = MemoryModel 2

pattern Vulkan :: MemoryModel
pattern Vulkan = MemoryModel 3

pattern VulkanKHR :: MemoryModel
pattern VulkanKHR = MemoryModel 3