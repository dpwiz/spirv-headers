{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.RayQueryIntersection where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype RayQueryIntersection = RayQueryIntersection Int32
  deriving newtype (Eq, Ord, Storable)

instance Show RayQueryIntersection where
  showsPrec p (RayQueryIntersection v) = case v of
    0 -> showString "RayQueryCandidateIntersectionKHR"
    1 -> showString "RayQueryCommittedIntersectionKHR"
    x -> showParen (p > 10) $ showString "RayQueryIntersection " . showsPrec (p + 1) x

pattern RayQueryCandidateIntersectionKHR :: RayQueryIntersection
pattern RayQueryCandidateIntersectionKHR = RayQueryIntersection 0

pattern RayQueryCommittedIntersectionKHR :: RayQueryIntersection
pattern RayQueryCommittedIntersectionKHR = RayQueryIntersection 1