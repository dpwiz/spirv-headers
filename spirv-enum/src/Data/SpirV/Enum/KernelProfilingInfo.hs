{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.KernelProfilingInfo where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type KernelProfilingInfo = KernelProfilingInfoBits

newtype KernelProfilingInfoBits = KernelProfilingInfoBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup KernelProfilingInfo where
  (KernelProfilingInfoBits a) <> (KernelProfilingInfoBits b) = KernelProfilingInfoBits (a .|. b)

instance Monoid KernelProfilingInfo where
  mempty = KernelProfilingInfoBits 0

pattern CmdExecTime :: KernelProfilingInfoBits
pattern CmdExecTime = KernelProfilingInfoBits 0x00000001