{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.RayQueryCommittedIntersectionType where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype RayQueryCommittedIntersectionType = RayQueryCommittedIntersectionType Int32
  deriving newtype (Eq, Ord, Storable)

instance Show RayQueryCommittedIntersectionType where
  showsPrec p (RayQueryCommittedIntersectionType v) = case v of
    0 -> showString "RayQueryCommittedIntersectionNoneKHR"
    1 -> showString "RayQueryCommittedIntersectionTriangleKHR"
    2 -> showString "RayQueryCommittedIntersectionGeneratedKHR"
    x -> showParen (p > 10) $ showString "RayQueryCommittedIntersectionType " . showsPrec (p + 1) x

pattern RayQueryCommittedIntersectionNoneKHR :: RayQueryCommittedIntersectionType
pattern RayQueryCommittedIntersectionNoneKHR = RayQueryCommittedIntersectionType 0

pattern RayQueryCommittedIntersectionTriangleKHR :: RayQueryCommittedIntersectionType
pattern RayQueryCommittedIntersectionTriangleKHR = RayQueryCommittedIntersectionType 1

pattern RayQueryCommittedIntersectionGeneratedKHR :: RayQueryCommittedIntersectionType
pattern RayQueryCommittedIntersectionGeneratedKHR = RayQueryCommittedIntersectionType 2