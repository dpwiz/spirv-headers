{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FPRoundingMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype FPRoundingMode = FPRoundingMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show FPRoundingMode where
  showsPrec p (FPRoundingMode v) = case v of
    0 -> showString "RTE"
    1 -> showString "RTZ"
    2 -> showString "RTP"
    3 -> showString "RTN"
    x -> showParen (p > 10) $ showString "FPRoundingMode " . showsPrec (p + 1) x

pattern RTE :: FPRoundingMode
pattern RTE = FPRoundingMode 0

pattern RTZ :: FPRoundingMode
pattern RTZ = FPRoundingMode 1

pattern RTP :: FPRoundingMode
pattern RTP = FPRoundingMode 2

pattern RTN :: FPRoundingMode
pattern RTN = FPRoundingMode 3