{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.Capability where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype Capability = Capability Int32
  deriving newtype (Eq, Ord, Storable)

instance Show Capability where
  showsPrec p (Capability v) = case v of
    0 -> showString "Matrix"
    1 -> showString "Shader"
    2 -> showString "Geometry"
    3 -> showString "Tessellation"
    4 -> showString "Addresses"
    5 -> showString "Linkage"
    6 -> showString "Kernel"
    7 -> showString "Vector16"
    8 -> showString "Float16Buffer"
    9 -> showString "Float16"
    10 -> showString "Float64"
    11 -> showString "Int64"
    12 -> showString "Int64Atomics"
    13 -> showString "ImageBasic"
    14 -> showString "ImageReadWrite"
    15 -> showString "ImageMipmap"
    17 -> showString "Pipes"
    18 -> showString "Groups"
    19 -> showString "DeviceEnqueue"
    20 -> showString "LiteralSampler"
    21 -> showString "AtomicStorage"
    22 -> showString "Int16"
    23 -> showString "TessellationPointSize"
    24 -> showString "GeometryPointSize"
    25 -> showString "ImageGatherExtended"
    27 -> showString "StorageImageMultisample"
    28 -> showString "UniformBufferArrayDynamicIndexing"
    29 -> showString "SampledImageArrayDynamicIndexing"
    30 -> showString "StorageBufferArrayDynamicIndexing"
    31 -> showString "StorageImageArrayDynamicIndexing"
    32 -> showString "ClipDistance"
    33 -> showString "CullDistance"
    34 -> showString "ImageCubeArray"
    35 -> showString "SampleRateShading"
    36 -> showString "ImageRect"
    37 -> showString "SampledRect"
    38 -> showString "GenericPointer"
    39 -> showString "Int8"
    40 -> showString "InputAttachment"
    41 -> showString "SparseResidency"
    42 -> showString "MinLod"
    43 -> showString "Sampled1D"
    44 -> showString "Image1D"
    45 -> showString "SampledCubeArray"
    46 -> showString "SampledBuffer"
    47 -> showString "ImageBuffer"
    48 -> showString "ImageMSArray"
    49 -> showString "StorageImageExtendedFormats"
    50 -> showString "ImageQuery"
    51 -> showString "DerivativeControl"
    52 -> showString "InterpolationFunction"
    53 -> showString "TransformFeedback"
    54 -> showString "GeometryStreams"
    55 -> showString "StorageImageReadWithoutFormat"
    56 -> showString "StorageImageWriteWithoutFormat"
    57 -> showString "MultiViewport"
    58 -> showString "SubgroupDispatch"
    59 -> showString "NamedBarrier"
    60 -> showString "PipeStorage"
    61 -> showString "GroupNonUniform"
    62 -> showString "GroupNonUniformVote"
    63 -> showString "GroupNonUniformArithmetic"
    64 -> showString "GroupNonUniformBallot"
    65 -> showString "GroupNonUniformShuffle"
    66 -> showString "GroupNonUniformShuffleRelative"
    67 -> showString "GroupNonUniformClustered"
    68 -> showString "GroupNonUniformQuad"
    69 -> showString "ShaderLayer"
    70 -> showString "ShaderViewportIndex"
    71 -> showString "UniformDecoration"
    4165 -> showString "CoreBuiltinsARM"
    4166 -> showString "TileImageColorReadAccessEXT"
    4167 -> showString "TileImageDepthReadAccessEXT"
    4168 -> showString "TileImageStencilReadAccessEXT"
    4201 -> showString "CooperativeMatrixLayoutsARM"
    4422 -> showString "FragmentShadingRateKHR"
    4423 -> showString "SubgroupBallotKHR"
    4427 -> showString "DrawParameters"
    4428 -> showString "WorkgroupMemoryExplicitLayoutKHR"
    4429 -> showString "WorkgroupMemoryExplicitLayout8BitAccessKHR"
    4430 -> showString "WorkgroupMemoryExplicitLayout16BitAccessKHR"
    4431 -> showString "SubgroupVoteKHR"
    4433 -> showString "StorageBuffer16BitAccess"
    4434 -> showString "StorageUniform16"
    4435 -> showString "StoragePushConstant16"
    4436 -> showString "StorageInputOutput16"
    4437 -> showString "DeviceGroup"
    4439 -> showString "MultiView"
    4441 -> showString "VariablePointersStorageBuffer"
    4442 -> showString "VariablePointers"
    4445 -> showString "AtomicStorageOps"
    4447 -> showString "SampleMaskPostDepthCoverage"
    4448 -> showString "StorageBuffer8BitAccess"
    4449 -> showString "UniformAndStorageBuffer8BitAccess"
    4450 -> showString "StoragePushConstant8"
    4464 -> showString "DenormPreserve"
    4465 -> showString "DenormFlushToZero"
    4466 -> showString "SignedZeroInfNanPreserve"
    4467 -> showString "RoundingModeRTE"
    4468 -> showString "RoundingModeRTZ"
    4471 -> showString "RayQueryProvisionalKHR"
    4472 -> showString "RayQueryKHR"
    4478 -> showString "RayTraversalPrimitiveCullingKHR"
    4479 -> showString "RayTracingKHR"
    4484 -> showString "TextureSampleWeightedQCOM"
    4485 -> showString "TextureBoxFilterQCOM"
    4486 -> showString "TextureBlockMatchQCOM"
    4498 -> showString "TextureBlockMatch2QCOM"
    5008 -> showString "Float16ImageAMD"
    5009 -> showString "ImageGatherBiasLodAMD"
    5010 -> showString "FragmentMaskAMD"
    5013 -> showString "StencilExportEXT"
    5015 -> showString "ImageReadWriteLodAMD"
    5016 -> showString "Int64ImageEXT"
    5055 -> showString "ShaderClockKHR"
    5067 -> showString "ShaderEnqueueAMDX"
    5087 -> showString "QuadControlKHR"
    5249 -> showString "SampleMaskOverrideCoverageNV"
    5251 -> showString "GeometryShaderPassthroughNV"
    5254 -> showString "ShaderViewportIndexLayerEXT"
    5255 -> showString "ShaderViewportMaskNV"
    5259 -> showString "ShaderStereoViewNV"
    5260 -> showString "PerViewAttributesNV"
    5265 -> showString "FragmentFullyCoveredEXT"
    5266 -> showString "MeshShadingNV"
    5282 -> showString "ImageFootprintNV"
    5283 -> showString "MeshShadingEXT"
    5284 -> showString "FragmentBarycentricKHR"
    5288 -> showString "ComputeDerivativeGroupQuadsNV"
    5291 -> showString "FragmentDensityEXT"
    5297 -> showString "GroupNonUniformPartitionedNV"
    5301 -> showString "ShaderNonUniform"
    5302 -> showString "RuntimeDescriptorArray"
    5303 -> showString "InputAttachmentArrayDynamicIndexing"
    5304 -> showString "UniformTexelBufferArrayDynamicIndexing"
    5305 -> showString "StorageTexelBufferArrayDynamicIndexing"
    5306 -> showString "UniformBufferArrayNonUniformIndexing"
    5307 -> showString "SampledImageArrayNonUniformIndexing"
    5308 -> showString "StorageBufferArrayNonUniformIndexing"
    5309 -> showString "StorageImageArrayNonUniformIndexing"
    5310 -> showString "InputAttachmentArrayNonUniformIndexing"
    5311 -> showString "UniformTexelBufferArrayNonUniformIndexing"
    5312 -> showString "StorageTexelBufferArrayNonUniformIndexing"
    5336 -> showString "RayTracingPositionFetchKHR"
    5340 -> showString "RayTracingNV"
    5341 -> showString "RayTracingMotionBlurNV"
    5345 -> showString "VulkanMemoryModel"
    5346 -> showString "VulkanMemoryModelDeviceScope"
    5347 -> showString "PhysicalStorageBufferAddresses"
    5350 -> showString "ComputeDerivativeGroupLinearNV"
    5353 -> showString "RayTracingProvisionalKHR"
    5357 -> showString "CooperativeMatrixNV"
    5363 -> showString "FragmentShaderSampleInterlockEXT"
    5372 -> showString "FragmentShaderShadingRateInterlockEXT"
    5373 -> showString "ShaderSMBuiltinsNV"
    5378 -> showString "FragmentShaderPixelInterlockEXT"
    5379 -> showString "DemoteToHelperInvocation"
    5380 -> showString "DisplacementMicromapNV"
    5381 -> showString "RayTracingOpacityMicromapEXT"
    5383 -> showString "ShaderInvocationReorderNV"
    5390 -> showString "BindlessTextureNV"
    5391 -> showString "RayQueryPositionFetchKHR"
    5404 -> showString "AtomicFloat16VectorNV"
    5409 -> showString "RayTracingDisplacementMicromapNV"
    5414 -> showString "RawAccessChainsNV"
    5568 -> showString "SubgroupShuffleINTEL"
    5569 -> showString "SubgroupBufferBlockIOINTEL"
    5570 -> showString "SubgroupImageBlockIOINTEL"
    5579 -> showString "SubgroupImageMediaBlockIOINTEL"
    5582 -> showString "RoundToInfinityINTEL"
    5583 -> showString "FloatingPointModeINTEL"
    5584 -> showString "IntegerFunctions2INTEL"
    5603 -> showString "FunctionPointersINTEL"
    5604 -> showString "IndirectReferencesINTEL"
    5606 -> showString "AsmINTEL"
    5612 -> showString "AtomicFloat32MinMaxEXT"
    5613 -> showString "AtomicFloat64MinMaxEXT"
    5616 -> showString "AtomicFloat16MinMaxEXT"
    5617 -> showString "VectorComputeINTEL"
    5619 -> showString "VectorAnyINTEL"
    5629 -> showString "ExpectAssumeKHR"
    5696 -> showString "SubgroupAvcMotionEstimationINTEL"
    5697 -> showString "SubgroupAvcMotionEstimationIntraINTEL"
    5698 -> showString "SubgroupAvcMotionEstimationChromaINTEL"
    5817 -> showString "VariableLengthArrayINTEL"
    5821 -> showString "FunctionFloatControlINTEL"
    5824 -> showString "FPGAMemoryAttributesINTEL"
    5837 -> showString "FPFastMathModeINTEL"
    5844 -> showString "ArbitraryPrecisionIntegersINTEL"
    5845 -> showString "ArbitraryPrecisionFloatingPointINTEL"
    5886 -> showString "UnstructuredLoopControlsINTEL"
    5888 -> showString "FPGALoopControlsINTEL"
    5892 -> showString "KernelAttributesINTEL"
    5897 -> showString "FPGAKernelAttributesINTEL"
    5898 -> showString "FPGAMemoryAccessesINTEL"
    5904 -> showString "FPGAClusterAttributesINTEL"
    5906 -> showString "LoopFuseINTEL"
    5908 -> showString "FPGADSPControlINTEL"
    5910 -> showString "MemoryAccessAliasingINTEL"
    5916 -> showString "FPGAInvocationPipeliningAttributesINTEL"
    5920 -> showString "FPGABufferLocationINTEL"
    5922 -> showString "ArbitraryPrecisionFixedPointINTEL"
    5935 -> showString "USMStorageClassesINTEL"
    5939 -> showString "RuntimeAlignedAttributeINTEL"
    5943 -> showString "IOPipesINTEL"
    5945 -> showString "BlockingPipesINTEL"
    5948 -> showString "FPGARegINTEL"
    6016 -> showString "DotProductInputAll"
    6017 -> showString "DotProductInput4x8Bit"
    6018 -> showString "DotProductInput4x8BitPacked"
    6019 -> showString "DotProduct"
    6020 -> showString "RayCullMaskKHR"
    6022 -> showString "CooperativeMatrixKHR"
    6024 -> showString "ReplicatedCompositesEXT"
    6025 -> showString "BitInstructions"
    6026 -> showString "GroupNonUniformRotateKHR"
    6029 -> showString "FloatControls2"
    6033 -> showString "AtomicFloat32AddEXT"
    6034 -> showString "AtomicFloat64AddEXT"
    6089 -> showString "LongCompositesINTEL"
    6094 -> showString "OptNoneINTEL"
    6095 -> showString "AtomicFloat16AddEXT"
    6114 -> showString "DebugInfoModuleINTEL"
    6115 -> showString "BFloat16ConversionINTEL"
    6141 -> showString "SplitBarrierINTEL"
    6150 -> showString "FPGAClusterAttributesV2INTEL"
    6161 -> showString "FPGAKernelAttributesv2INTEL"
    6169 -> showString "FPMaxErrorINTEL"
    6171 -> showString "FPGALatencyControlINTEL"
    6174 -> showString "FPGAArgumentInterfacesINTEL"
    6187 -> showString "GlobalVariableHostAccessINTEL"
    6189 -> showString "GlobalVariableFPGADecorationsINTEL"
    6400 -> showString "GroupUniformArithmeticKHR"
    6427 -> showString "MaskedGatherScatterINTEL"
    6441 -> showString "CacheControlsINTEL"
    6460 -> showString "RegisterLimitsINTEL"
    x -> showParen (p > 10) $ showString "Capability " . showsPrec (p + 1) x

pattern Matrix :: Capability
pattern Matrix = Capability 0

pattern Shader :: Capability
pattern Shader = Capability 1

pattern Geometry :: Capability
pattern Geometry = Capability 2

pattern Tessellation :: Capability
pattern Tessellation = Capability 3

pattern Addresses :: Capability
pattern Addresses = Capability 4

pattern Linkage :: Capability
pattern Linkage = Capability 5

pattern Kernel :: Capability
pattern Kernel = Capability 6

pattern Vector16 :: Capability
pattern Vector16 = Capability 7

pattern Float16Buffer :: Capability
pattern Float16Buffer = Capability 8

pattern Float16 :: Capability
pattern Float16 = Capability 9

pattern Float64 :: Capability
pattern Float64 = Capability 10

pattern Int64 :: Capability
pattern Int64 = Capability 11

pattern Int64Atomics :: Capability
pattern Int64Atomics = Capability 12

pattern ImageBasic :: Capability
pattern ImageBasic = Capability 13

pattern ImageReadWrite :: Capability
pattern ImageReadWrite = Capability 14

pattern ImageMipmap :: Capability
pattern ImageMipmap = Capability 15

pattern Pipes :: Capability
pattern Pipes = Capability 17

pattern Groups :: Capability
pattern Groups = Capability 18

pattern DeviceEnqueue :: Capability
pattern DeviceEnqueue = Capability 19

pattern LiteralSampler :: Capability
pattern LiteralSampler = Capability 20

pattern AtomicStorage :: Capability
pattern AtomicStorage = Capability 21

pattern Int16 :: Capability
pattern Int16 = Capability 22

pattern TessellationPointSize :: Capability
pattern TessellationPointSize = Capability 23

pattern GeometryPointSize :: Capability
pattern GeometryPointSize = Capability 24

pattern ImageGatherExtended :: Capability
pattern ImageGatherExtended = Capability 25

pattern StorageImageMultisample :: Capability
pattern StorageImageMultisample = Capability 27

pattern UniformBufferArrayDynamicIndexing :: Capability
pattern UniformBufferArrayDynamicIndexing = Capability 28

pattern SampledImageArrayDynamicIndexing :: Capability
pattern SampledImageArrayDynamicIndexing = Capability 29

pattern StorageBufferArrayDynamicIndexing :: Capability
pattern StorageBufferArrayDynamicIndexing = Capability 30

pattern StorageImageArrayDynamicIndexing :: Capability
pattern StorageImageArrayDynamicIndexing = Capability 31

pattern ClipDistance :: Capability
pattern ClipDistance = Capability 32

pattern CullDistance :: Capability
pattern CullDistance = Capability 33

pattern ImageCubeArray :: Capability
pattern ImageCubeArray = Capability 34

pattern SampleRateShading :: Capability
pattern SampleRateShading = Capability 35

pattern ImageRect :: Capability
pattern ImageRect = Capability 36

pattern SampledRect :: Capability
pattern SampledRect = Capability 37

pattern GenericPointer :: Capability
pattern GenericPointer = Capability 38

pattern Int8 :: Capability
pattern Int8 = Capability 39

pattern InputAttachment :: Capability
pattern InputAttachment = Capability 40

pattern SparseResidency :: Capability
pattern SparseResidency = Capability 41

pattern MinLod :: Capability
pattern MinLod = Capability 42

pattern Sampled1D :: Capability
pattern Sampled1D = Capability 43

pattern Image1D :: Capability
pattern Image1D = Capability 44

pattern SampledCubeArray :: Capability
pattern SampledCubeArray = Capability 45

pattern SampledBuffer :: Capability
pattern SampledBuffer = Capability 46

pattern ImageBuffer :: Capability
pattern ImageBuffer = Capability 47

pattern ImageMSArray :: Capability
pattern ImageMSArray = Capability 48

pattern StorageImageExtendedFormats :: Capability
pattern StorageImageExtendedFormats = Capability 49

pattern ImageQuery :: Capability
pattern ImageQuery = Capability 50

pattern DerivativeControl :: Capability
pattern DerivativeControl = Capability 51

pattern InterpolationFunction :: Capability
pattern InterpolationFunction = Capability 52

pattern TransformFeedback :: Capability
pattern TransformFeedback = Capability 53

pattern GeometryStreams :: Capability
pattern GeometryStreams = Capability 54

pattern StorageImageReadWithoutFormat :: Capability
pattern StorageImageReadWithoutFormat = Capability 55

pattern StorageImageWriteWithoutFormat :: Capability
pattern StorageImageWriteWithoutFormat = Capability 56

pattern MultiViewport :: Capability
pattern MultiViewport = Capability 57

pattern SubgroupDispatch :: Capability
pattern SubgroupDispatch = Capability 58

pattern NamedBarrier :: Capability
pattern NamedBarrier = Capability 59

pattern PipeStorage :: Capability
pattern PipeStorage = Capability 60

pattern GroupNonUniform :: Capability
pattern GroupNonUniform = Capability 61

pattern GroupNonUniformVote :: Capability
pattern GroupNonUniformVote = Capability 62

pattern GroupNonUniformArithmetic :: Capability
pattern GroupNonUniformArithmetic = Capability 63

pattern GroupNonUniformBallot :: Capability
pattern GroupNonUniformBallot = Capability 64

pattern GroupNonUniformShuffle :: Capability
pattern GroupNonUniformShuffle = Capability 65

pattern GroupNonUniformShuffleRelative :: Capability
pattern GroupNonUniformShuffleRelative = Capability 66

pattern GroupNonUniformClustered :: Capability
pattern GroupNonUniformClustered = Capability 67

pattern GroupNonUniformQuad :: Capability
pattern GroupNonUniformQuad = Capability 68

pattern ShaderLayer :: Capability
pattern ShaderLayer = Capability 69

pattern ShaderViewportIndex :: Capability
pattern ShaderViewportIndex = Capability 70

pattern UniformDecoration :: Capability
pattern UniformDecoration = Capability 71

pattern CoreBuiltinsARM :: Capability
pattern CoreBuiltinsARM = Capability 4165

pattern TileImageColorReadAccessEXT :: Capability
pattern TileImageColorReadAccessEXT = Capability 4166

pattern TileImageDepthReadAccessEXT :: Capability
pattern TileImageDepthReadAccessEXT = Capability 4167

pattern TileImageStencilReadAccessEXT :: Capability
pattern TileImageStencilReadAccessEXT = Capability 4168

pattern CooperativeMatrixLayoutsARM :: Capability
pattern CooperativeMatrixLayoutsARM = Capability 4201

pattern FragmentShadingRateKHR :: Capability
pattern FragmentShadingRateKHR = Capability 4422

pattern SubgroupBallotKHR :: Capability
pattern SubgroupBallotKHR = Capability 4423

pattern DrawParameters :: Capability
pattern DrawParameters = Capability 4427

pattern WorkgroupMemoryExplicitLayoutKHR :: Capability
pattern WorkgroupMemoryExplicitLayoutKHR = Capability 4428

pattern WorkgroupMemoryExplicitLayout8BitAccessKHR :: Capability
pattern WorkgroupMemoryExplicitLayout8BitAccessKHR = Capability 4429

pattern WorkgroupMemoryExplicitLayout16BitAccessKHR :: Capability
pattern WorkgroupMemoryExplicitLayout16BitAccessKHR = Capability 4430

pattern SubgroupVoteKHR :: Capability
pattern SubgroupVoteKHR = Capability 4431

pattern StorageBuffer16BitAccess :: Capability
pattern StorageBuffer16BitAccess = Capability 4433

pattern StorageUniformBufferBlock16 :: Capability
pattern StorageUniformBufferBlock16 = Capability 4433

pattern StorageUniform16 :: Capability
pattern StorageUniform16 = Capability 4434

pattern UniformAndStorageBuffer16BitAccess :: Capability
pattern UniformAndStorageBuffer16BitAccess = Capability 4434

pattern StoragePushConstant16 :: Capability
pattern StoragePushConstant16 = Capability 4435

pattern StorageInputOutput16 :: Capability
pattern StorageInputOutput16 = Capability 4436

pattern DeviceGroup :: Capability
pattern DeviceGroup = Capability 4437

pattern MultiView :: Capability
pattern MultiView = Capability 4439

pattern VariablePointersStorageBuffer :: Capability
pattern VariablePointersStorageBuffer = Capability 4441

pattern VariablePointers :: Capability
pattern VariablePointers = Capability 4442

pattern AtomicStorageOps :: Capability
pattern AtomicStorageOps = Capability 4445

pattern SampleMaskPostDepthCoverage :: Capability
pattern SampleMaskPostDepthCoverage = Capability 4447

pattern StorageBuffer8BitAccess :: Capability
pattern StorageBuffer8BitAccess = Capability 4448

pattern UniformAndStorageBuffer8BitAccess :: Capability
pattern UniformAndStorageBuffer8BitAccess = Capability 4449

pattern StoragePushConstant8 :: Capability
pattern StoragePushConstant8 = Capability 4450

pattern DenormPreserve :: Capability
pattern DenormPreserve = Capability 4464

pattern DenormFlushToZero :: Capability
pattern DenormFlushToZero = Capability 4465

pattern SignedZeroInfNanPreserve :: Capability
pattern SignedZeroInfNanPreserve = Capability 4466

pattern RoundingModeRTE :: Capability
pattern RoundingModeRTE = Capability 4467

pattern RoundingModeRTZ :: Capability
pattern RoundingModeRTZ = Capability 4468

pattern RayQueryProvisionalKHR :: Capability
pattern RayQueryProvisionalKHR = Capability 4471

pattern RayQueryKHR :: Capability
pattern RayQueryKHR = Capability 4472

pattern RayTraversalPrimitiveCullingKHR :: Capability
pattern RayTraversalPrimitiveCullingKHR = Capability 4478

pattern RayTracingKHR :: Capability
pattern RayTracingKHR = Capability 4479

pattern TextureSampleWeightedQCOM :: Capability
pattern TextureSampleWeightedQCOM = Capability 4484

pattern TextureBoxFilterQCOM :: Capability
pattern TextureBoxFilterQCOM = Capability 4485

pattern TextureBlockMatchQCOM :: Capability
pattern TextureBlockMatchQCOM = Capability 4486

pattern TextureBlockMatch2QCOM :: Capability
pattern TextureBlockMatch2QCOM = Capability 4498

pattern Float16ImageAMD :: Capability
pattern Float16ImageAMD = Capability 5008

pattern ImageGatherBiasLodAMD :: Capability
pattern ImageGatherBiasLodAMD = Capability 5009

pattern FragmentMaskAMD :: Capability
pattern FragmentMaskAMD = Capability 5010

pattern StencilExportEXT :: Capability
pattern StencilExportEXT = Capability 5013

pattern ImageReadWriteLodAMD :: Capability
pattern ImageReadWriteLodAMD = Capability 5015

pattern Int64ImageEXT :: Capability
pattern Int64ImageEXT = Capability 5016

pattern ShaderClockKHR :: Capability
pattern ShaderClockKHR = Capability 5055

pattern ShaderEnqueueAMDX :: Capability
pattern ShaderEnqueueAMDX = Capability 5067

pattern QuadControlKHR :: Capability
pattern QuadControlKHR = Capability 5087

pattern SampleMaskOverrideCoverageNV :: Capability
pattern SampleMaskOverrideCoverageNV = Capability 5249

pattern GeometryShaderPassthroughNV :: Capability
pattern GeometryShaderPassthroughNV = Capability 5251

pattern ShaderViewportIndexLayerEXT :: Capability
pattern ShaderViewportIndexLayerEXT = Capability 5254

pattern ShaderViewportIndexLayerNV :: Capability
pattern ShaderViewportIndexLayerNV = Capability 5254

pattern ShaderViewportMaskNV :: Capability
pattern ShaderViewportMaskNV = Capability 5255

pattern ShaderStereoViewNV :: Capability
pattern ShaderStereoViewNV = Capability 5259

pattern PerViewAttributesNV :: Capability
pattern PerViewAttributesNV = Capability 5260

pattern FragmentFullyCoveredEXT :: Capability
pattern FragmentFullyCoveredEXT = Capability 5265

pattern MeshShadingNV :: Capability
pattern MeshShadingNV = Capability 5266

pattern ImageFootprintNV :: Capability
pattern ImageFootprintNV = Capability 5282

pattern MeshShadingEXT :: Capability
pattern MeshShadingEXT = Capability 5283

pattern FragmentBarycentricKHR :: Capability
pattern FragmentBarycentricKHR = Capability 5284

pattern FragmentBarycentricNV :: Capability
pattern FragmentBarycentricNV = Capability 5284

pattern ComputeDerivativeGroupQuadsNV :: Capability
pattern ComputeDerivativeGroupQuadsNV = Capability 5288

pattern FragmentDensityEXT :: Capability
pattern FragmentDensityEXT = Capability 5291

pattern ShadingRateNV :: Capability
pattern ShadingRateNV = Capability 5291

pattern GroupNonUniformPartitionedNV :: Capability
pattern GroupNonUniformPartitionedNV = Capability 5297

pattern ShaderNonUniform :: Capability
pattern ShaderNonUniform = Capability 5301

pattern ShaderNonUniformEXT :: Capability
pattern ShaderNonUniformEXT = Capability 5301

pattern RuntimeDescriptorArray :: Capability
pattern RuntimeDescriptorArray = Capability 5302

pattern RuntimeDescriptorArrayEXT :: Capability
pattern RuntimeDescriptorArrayEXT = Capability 5302

pattern InputAttachmentArrayDynamicIndexing :: Capability
pattern InputAttachmentArrayDynamicIndexing = Capability 5303

pattern InputAttachmentArrayDynamicIndexingEXT :: Capability
pattern InputAttachmentArrayDynamicIndexingEXT = Capability 5303

pattern UniformTexelBufferArrayDynamicIndexing :: Capability
pattern UniformTexelBufferArrayDynamicIndexing = Capability 5304

pattern UniformTexelBufferArrayDynamicIndexingEXT :: Capability
pattern UniformTexelBufferArrayDynamicIndexingEXT = Capability 5304

pattern StorageTexelBufferArrayDynamicIndexing :: Capability
pattern StorageTexelBufferArrayDynamicIndexing = Capability 5305

pattern StorageTexelBufferArrayDynamicIndexingEXT :: Capability
pattern StorageTexelBufferArrayDynamicIndexingEXT = Capability 5305

pattern UniformBufferArrayNonUniformIndexing :: Capability
pattern UniformBufferArrayNonUniformIndexing = Capability 5306

pattern UniformBufferArrayNonUniformIndexingEXT :: Capability
pattern UniformBufferArrayNonUniformIndexingEXT = Capability 5306

pattern SampledImageArrayNonUniformIndexing :: Capability
pattern SampledImageArrayNonUniformIndexing = Capability 5307

pattern SampledImageArrayNonUniformIndexingEXT :: Capability
pattern SampledImageArrayNonUniformIndexingEXT = Capability 5307

pattern StorageBufferArrayNonUniformIndexing :: Capability
pattern StorageBufferArrayNonUniformIndexing = Capability 5308

pattern StorageBufferArrayNonUniformIndexingEXT :: Capability
pattern StorageBufferArrayNonUniformIndexingEXT = Capability 5308

pattern StorageImageArrayNonUniformIndexing :: Capability
pattern StorageImageArrayNonUniformIndexing = Capability 5309

pattern StorageImageArrayNonUniformIndexingEXT :: Capability
pattern StorageImageArrayNonUniformIndexingEXT = Capability 5309

pattern InputAttachmentArrayNonUniformIndexing :: Capability
pattern InputAttachmentArrayNonUniformIndexing = Capability 5310

pattern InputAttachmentArrayNonUniformIndexingEXT :: Capability
pattern InputAttachmentArrayNonUniformIndexingEXT = Capability 5310

pattern UniformTexelBufferArrayNonUniformIndexing :: Capability
pattern UniformTexelBufferArrayNonUniformIndexing = Capability 5311

pattern UniformTexelBufferArrayNonUniformIndexingEXT :: Capability
pattern UniformTexelBufferArrayNonUniformIndexingEXT = Capability 5311

pattern StorageTexelBufferArrayNonUniformIndexing :: Capability
pattern StorageTexelBufferArrayNonUniformIndexing = Capability 5312

pattern StorageTexelBufferArrayNonUniformIndexingEXT :: Capability
pattern StorageTexelBufferArrayNonUniformIndexingEXT = Capability 5312

pattern RayTracingPositionFetchKHR :: Capability
pattern RayTracingPositionFetchKHR = Capability 5336

pattern RayTracingNV :: Capability
pattern RayTracingNV = Capability 5340

pattern RayTracingMotionBlurNV :: Capability
pattern RayTracingMotionBlurNV = Capability 5341

pattern VulkanMemoryModel :: Capability
pattern VulkanMemoryModel = Capability 5345

pattern VulkanMemoryModelKHR :: Capability
pattern VulkanMemoryModelKHR = Capability 5345

pattern VulkanMemoryModelDeviceScope :: Capability
pattern VulkanMemoryModelDeviceScope = Capability 5346

pattern VulkanMemoryModelDeviceScopeKHR :: Capability
pattern VulkanMemoryModelDeviceScopeKHR = Capability 5346

pattern PhysicalStorageBufferAddresses :: Capability
pattern PhysicalStorageBufferAddresses = Capability 5347

pattern PhysicalStorageBufferAddressesEXT :: Capability
pattern PhysicalStorageBufferAddressesEXT = Capability 5347

pattern ComputeDerivativeGroupLinearNV :: Capability
pattern ComputeDerivativeGroupLinearNV = Capability 5350

pattern RayTracingProvisionalKHR :: Capability
pattern RayTracingProvisionalKHR = Capability 5353

pattern CooperativeMatrixNV :: Capability
pattern CooperativeMatrixNV = Capability 5357

pattern FragmentShaderSampleInterlockEXT :: Capability
pattern FragmentShaderSampleInterlockEXT = Capability 5363

pattern FragmentShaderShadingRateInterlockEXT :: Capability
pattern FragmentShaderShadingRateInterlockEXT = Capability 5372

pattern ShaderSMBuiltinsNV :: Capability
pattern ShaderSMBuiltinsNV = Capability 5373

pattern FragmentShaderPixelInterlockEXT :: Capability
pattern FragmentShaderPixelInterlockEXT = Capability 5378

pattern DemoteToHelperInvocation :: Capability
pattern DemoteToHelperInvocation = Capability 5379

pattern DemoteToHelperInvocationEXT :: Capability
pattern DemoteToHelperInvocationEXT = Capability 5379

pattern DisplacementMicromapNV :: Capability
pattern DisplacementMicromapNV = Capability 5380

pattern RayTracingOpacityMicromapEXT :: Capability
pattern RayTracingOpacityMicromapEXT = Capability 5381

pattern ShaderInvocationReorderNV :: Capability
pattern ShaderInvocationReorderNV = Capability 5383

pattern BindlessTextureNV :: Capability
pattern BindlessTextureNV = Capability 5390

pattern RayQueryPositionFetchKHR :: Capability
pattern RayQueryPositionFetchKHR = Capability 5391

pattern AtomicFloat16VectorNV :: Capability
pattern AtomicFloat16VectorNV = Capability 5404

pattern RayTracingDisplacementMicromapNV :: Capability
pattern RayTracingDisplacementMicromapNV = Capability 5409

pattern RawAccessChainsNV :: Capability
pattern RawAccessChainsNV = Capability 5414

pattern SubgroupShuffleINTEL :: Capability
pattern SubgroupShuffleINTEL = Capability 5568

pattern SubgroupBufferBlockIOINTEL :: Capability
pattern SubgroupBufferBlockIOINTEL = Capability 5569

pattern SubgroupImageBlockIOINTEL :: Capability
pattern SubgroupImageBlockIOINTEL = Capability 5570

pattern SubgroupImageMediaBlockIOINTEL :: Capability
pattern SubgroupImageMediaBlockIOINTEL = Capability 5579

pattern RoundToInfinityINTEL :: Capability
pattern RoundToInfinityINTEL = Capability 5582

pattern FloatingPointModeINTEL :: Capability
pattern FloatingPointModeINTEL = Capability 5583

pattern IntegerFunctions2INTEL :: Capability
pattern IntegerFunctions2INTEL = Capability 5584

pattern FunctionPointersINTEL :: Capability
pattern FunctionPointersINTEL = Capability 5603

pattern IndirectReferencesINTEL :: Capability
pattern IndirectReferencesINTEL = Capability 5604

pattern AsmINTEL :: Capability
pattern AsmINTEL = Capability 5606

pattern AtomicFloat32MinMaxEXT :: Capability
pattern AtomicFloat32MinMaxEXT = Capability 5612

pattern AtomicFloat64MinMaxEXT :: Capability
pattern AtomicFloat64MinMaxEXT = Capability 5613

pattern AtomicFloat16MinMaxEXT :: Capability
pattern AtomicFloat16MinMaxEXT = Capability 5616

pattern VectorComputeINTEL :: Capability
pattern VectorComputeINTEL = Capability 5617

pattern VectorAnyINTEL :: Capability
pattern VectorAnyINTEL = Capability 5619

pattern ExpectAssumeKHR :: Capability
pattern ExpectAssumeKHR = Capability 5629

pattern SubgroupAvcMotionEstimationINTEL :: Capability
pattern SubgroupAvcMotionEstimationINTEL = Capability 5696

pattern SubgroupAvcMotionEstimationIntraINTEL :: Capability
pattern SubgroupAvcMotionEstimationIntraINTEL = Capability 5697

pattern SubgroupAvcMotionEstimationChromaINTEL :: Capability
pattern SubgroupAvcMotionEstimationChromaINTEL = Capability 5698

pattern VariableLengthArrayINTEL :: Capability
pattern VariableLengthArrayINTEL = Capability 5817

pattern FunctionFloatControlINTEL :: Capability
pattern FunctionFloatControlINTEL = Capability 5821

pattern FPGAMemoryAttributesINTEL :: Capability
pattern FPGAMemoryAttributesINTEL = Capability 5824

pattern FPFastMathModeINTEL :: Capability
pattern FPFastMathModeINTEL = Capability 5837

pattern ArbitraryPrecisionIntegersINTEL :: Capability
pattern ArbitraryPrecisionIntegersINTEL = Capability 5844

pattern ArbitraryPrecisionFloatingPointINTEL :: Capability
pattern ArbitraryPrecisionFloatingPointINTEL = Capability 5845

pattern UnstructuredLoopControlsINTEL :: Capability
pattern UnstructuredLoopControlsINTEL = Capability 5886

pattern FPGALoopControlsINTEL :: Capability
pattern FPGALoopControlsINTEL = Capability 5888

pattern KernelAttributesINTEL :: Capability
pattern KernelAttributesINTEL = Capability 5892

pattern FPGAKernelAttributesINTEL :: Capability
pattern FPGAKernelAttributesINTEL = Capability 5897

pattern FPGAMemoryAccessesINTEL :: Capability
pattern FPGAMemoryAccessesINTEL = Capability 5898

pattern FPGAClusterAttributesINTEL :: Capability
pattern FPGAClusterAttributesINTEL = Capability 5904

pattern LoopFuseINTEL :: Capability
pattern LoopFuseINTEL = Capability 5906

pattern FPGADSPControlINTEL :: Capability
pattern FPGADSPControlINTEL = Capability 5908

pattern MemoryAccessAliasingINTEL :: Capability
pattern MemoryAccessAliasingINTEL = Capability 5910

pattern FPGAInvocationPipeliningAttributesINTEL :: Capability
pattern FPGAInvocationPipeliningAttributesINTEL = Capability 5916

pattern FPGABufferLocationINTEL :: Capability
pattern FPGABufferLocationINTEL = Capability 5920

pattern ArbitraryPrecisionFixedPointINTEL :: Capability
pattern ArbitraryPrecisionFixedPointINTEL = Capability 5922

pattern USMStorageClassesINTEL :: Capability
pattern USMStorageClassesINTEL = Capability 5935

pattern RuntimeAlignedAttributeINTEL :: Capability
pattern RuntimeAlignedAttributeINTEL = Capability 5939

pattern IOPipesINTEL :: Capability
pattern IOPipesINTEL = Capability 5943

pattern BlockingPipesINTEL :: Capability
pattern BlockingPipesINTEL = Capability 5945

pattern FPGARegINTEL :: Capability
pattern FPGARegINTEL = Capability 5948

pattern DotProductInputAll :: Capability
pattern DotProductInputAll = Capability 6016

pattern DotProductInputAllKHR :: Capability
pattern DotProductInputAllKHR = Capability 6016

pattern DotProductInput4x8Bit :: Capability
pattern DotProductInput4x8Bit = Capability 6017

pattern DotProductInput4x8BitKHR :: Capability
pattern DotProductInput4x8BitKHR = Capability 6017

pattern DotProductInput4x8BitPacked :: Capability
pattern DotProductInput4x8BitPacked = Capability 6018

pattern DotProductInput4x8BitPackedKHR :: Capability
pattern DotProductInput4x8BitPackedKHR = Capability 6018

pattern DotProduct :: Capability
pattern DotProduct = Capability 6019

pattern DotProductKHR :: Capability
pattern DotProductKHR = Capability 6019

pattern RayCullMaskKHR :: Capability
pattern RayCullMaskKHR = Capability 6020

pattern CooperativeMatrixKHR :: Capability
pattern CooperativeMatrixKHR = Capability 6022

pattern ReplicatedCompositesEXT :: Capability
pattern ReplicatedCompositesEXT = Capability 6024

pattern BitInstructions :: Capability
pattern BitInstructions = Capability 6025

pattern GroupNonUniformRotateKHR :: Capability
pattern GroupNonUniformRotateKHR = Capability 6026

pattern FloatControls2 :: Capability
pattern FloatControls2 = Capability 6029

pattern AtomicFloat32AddEXT :: Capability
pattern AtomicFloat32AddEXT = Capability 6033

pattern AtomicFloat64AddEXT :: Capability
pattern AtomicFloat64AddEXT = Capability 6034

pattern LongCompositesINTEL :: Capability
pattern LongCompositesINTEL = Capability 6089

pattern OptNoneINTEL :: Capability
pattern OptNoneINTEL = Capability 6094

pattern AtomicFloat16AddEXT :: Capability
pattern AtomicFloat16AddEXT = Capability 6095

pattern DebugInfoModuleINTEL :: Capability
pattern DebugInfoModuleINTEL = Capability 6114

pattern BFloat16ConversionINTEL :: Capability
pattern BFloat16ConversionINTEL = Capability 6115

pattern SplitBarrierINTEL :: Capability
pattern SplitBarrierINTEL = Capability 6141

pattern FPGAClusterAttributesV2INTEL :: Capability
pattern FPGAClusterAttributesV2INTEL = Capability 6150

pattern FPGAKernelAttributesv2INTEL :: Capability
pattern FPGAKernelAttributesv2INTEL = Capability 6161

pattern FPMaxErrorINTEL :: Capability
pattern FPMaxErrorINTEL = Capability 6169

pattern FPGALatencyControlINTEL :: Capability
pattern FPGALatencyControlINTEL = Capability 6171

pattern FPGAArgumentInterfacesINTEL :: Capability
pattern FPGAArgumentInterfacesINTEL = Capability 6174

pattern GlobalVariableHostAccessINTEL :: Capability
pattern GlobalVariableHostAccessINTEL = Capability 6187

pattern GlobalVariableFPGADecorationsINTEL :: Capability
pattern GlobalVariableFPGADecorationsINTEL = Capability 6189

pattern GroupUniformArithmeticKHR :: Capability
pattern GroupUniformArithmeticKHR = Capability 6400

pattern MaskedGatherScatterINTEL :: Capability
pattern MaskedGatherScatterINTEL = Capability 6427

pattern CacheControlsINTEL :: Capability
pattern CacheControlsINTEL = Capability 6441

pattern RegisterLimitsINTEL :: Capability
pattern RegisterLimitsINTEL = Capability 6460