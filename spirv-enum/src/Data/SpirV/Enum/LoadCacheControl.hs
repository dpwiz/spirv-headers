{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.LoadCacheControl where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype LoadCacheControl = LoadCacheControl Int32
  deriving newtype (Eq, Ord, Storable)

instance Show LoadCacheControl where
  showsPrec p (LoadCacheControl v) = case v of
    0 -> showString "UncachedINTEL"
    1 -> showString "CachedINTEL"
    2 -> showString "StreamingINTEL"
    3 -> showString "InvalidateAfterReadINTEL"
    4 -> showString "ConstCachedINTEL"
    x -> showParen (p > 10) $ showString "LoadCacheControl " . showsPrec (p + 1) x

pattern UncachedINTEL :: LoadCacheControl
pattern UncachedINTEL = LoadCacheControl 0

pattern CachedINTEL :: LoadCacheControl
pattern CachedINTEL = LoadCacheControl 1

pattern StreamingINTEL :: LoadCacheControl
pattern StreamingINTEL = LoadCacheControl 2

pattern InvalidateAfterReadINTEL :: LoadCacheControl
pattern InvalidateAfterReadINTEL = LoadCacheControl 3

pattern ConstCachedINTEL :: LoadCacheControl
pattern ConstCachedINTEL = LoadCacheControl 4