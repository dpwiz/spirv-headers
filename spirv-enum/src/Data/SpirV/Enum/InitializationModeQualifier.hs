{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.InitializationModeQualifier where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype InitializationModeQualifier = InitializationModeQualifier Int32
  deriving newtype (Eq, Ord, Storable)

instance Show InitializationModeQualifier where
  showsPrec p (InitializationModeQualifier v) = case v of
    0 -> showString "InitOnDeviceReprogramINTEL"
    1 -> showString "InitOnDeviceResetINTEL"
    x -> showParen (p > 10) $ showString "InitializationModeQualifier " . showsPrec (p + 1) x

pattern InitOnDeviceReprogramINTEL :: InitializationModeQualifier
pattern InitOnDeviceReprogramINTEL = InitializationModeQualifier 0

pattern InitOnDeviceResetINTEL :: InitializationModeQualifier
pattern InitOnDeviceResetINTEL = InitializationModeQualifier 1