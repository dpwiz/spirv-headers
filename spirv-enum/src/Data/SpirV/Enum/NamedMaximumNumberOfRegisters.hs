{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.NamedMaximumNumberOfRegisters where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype NamedMaximumNumberOfRegisters = NamedMaximumNumberOfRegisters Int32
  deriving newtype (Eq, Ord, Storable)

instance Show NamedMaximumNumberOfRegisters where
  showsPrec p (NamedMaximumNumberOfRegisters v) = case v of
    0 -> showString "AutoINTEL"
    x -> showParen (p > 10) $ showString "NamedMaximumNumberOfRegisters " . showsPrec (p + 1) x

pattern AutoINTEL :: NamedMaximumNumberOfRegisters
pattern AutoINTEL = NamedMaximumNumberOfRegisters 0