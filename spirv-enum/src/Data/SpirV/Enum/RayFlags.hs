{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.RayFlags where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type RayFlags = RayFlagsBits

newtype RayFlagsBits = RayFlagsBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup RayFlags where
  (RayFlagsBits a) <> (RayFlagsBits b) = RayFlagsBits (a .|. b)

instance Monoid RayFlags where
  mempty = RayFlagsBits 0

pattern OpaqueKHR :: RayFlagsBits
pattern OpaqueKHR = RayFlagsBits 0x00000001

pattern NoOpaqueKHR :: RayFlagsBits
pattern NoOpaqueKHR = RayFlagsBits 0x00000002

pattern TerminateOnFirstHitKHR :: RayFlagsBits
pattern TerminateOnFirstHitKHR = RayFlagsBits 0x00000004

pattern SkipClosestHitShaderKHR :: RayFlagsBits
pattern SkipClosestHitShaderKHR = RayFlagsBits 0x00000008

pattern CullBackFacingTrianglesKHR :: RayFlagsBits
pattern CullBackFacingTrianglesKHR = RayFlagsBits 0x00000010

pattern CullFrontFacingTrianglesKHR :: RayFlagsBits
pattern CullFrontFacingTrianglesKHR = RayFlagsBits 0x00000020

pattern CullOpaqueKHR :: RayFlagsBits
pattern CullOpaqueKHR = RayFlagsBits 0x00000040

pattern CullNoOpaqueKHR :: RayFlagsBits
pattern CullNoOpaqueKHR = RayFlagsBits 0x00000080

pattern SkipTrianglesKHR :: RayFlagsBits
pattern SkipTrianglesKHR = RayFlagsBits 0x00000100

pattern SkipAABBsKHR :: RayFlagsBits
pattern SkipAABBsKHR = RayFlagsBits 0x00000200

pattern ForceOpacityMicromap2StateEXT :: RayFlagsBits
pattern ForceOpacityMicromap2StateEXT = RayFlagsBits 0x00000400