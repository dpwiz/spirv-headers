{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.HostAccessQualifier where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype HostAccessQualifier = HostAccessQualifier Int32
  deriving newtype (Eq, Ord, Storable)

instance Show HostAccessQualifier where
  showsPrec p (HostAccessQualifier v) = case v of
    0 -> showString "NoneINTEL"
    1 -> showString "ReadINTEL"
    2 -> showString "WriteINTEL"
    3 -> showString "ReadWriteINTEL"
    x -> showParen (p > 10) $ showString "HostAccessQualifier " . showsPrec (p + 1) x

pattern NoneINTEL :: HostAccessQualifier
pattern NoneINTEL = HostAccessQualifier 0

pattern ReadINTEL :: HostAccessQualifier
pattern ReadINTEL = HostAccessQualifier 1

pattern WriteINTEL :: HostAccessQualifier
pattern WriteINTEL = HostAccessQualifier 2

pattern ReadWriteINTEL :: HostAccessQualifier
pattern ReadWriteINTEL = HostAccessQualifier 3