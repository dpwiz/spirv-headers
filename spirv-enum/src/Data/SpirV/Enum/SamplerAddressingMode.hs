{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.SamplerAddressingMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype SamplerAddressingMode = SamplerAddressingMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show SamplerAddressingMode where
  showsPrec p (SamplerAddressingMode v) = case v of
    0 -> showString "None"
    1 -> showString "ClampToEdge"
    2 -> showString "Clamp"
    3 -> showString "Repeat"
    4 -> showString "RepeatMirrored"
    x -> showParen (p > 10) $ showString "SamplerAddressingMode " . showsPrec (p + 1) x

pattern None :: SamplerAddressingMode
pattern None = SamplerAddressingMode 0

pattern ClampToEdge :: SamplerAddressingMode
pattern ClampToEdge = SamplerAddressingMode 1

pattern Clamp :: SamplerAddressingMode
pattern Clamp = SamplerAddressingMode 2

pattern Repeat :: SamplerAddressingMode
pattern Repeat = SamplerAddressingMode 3

pattern RepeatMirrored :: SamplerAddressingMode
pattern RepeatMirrored = SamplerAddressingMode 4