{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.CooperativeMatrixOperands where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type CooperativeMatrixOperands = CooperativeMatrixOperandsBits

newtype CooperativeMatrixOperandsBits = CooperativeMatrixOperandsBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup CooperativeMatrixOperands where
  (CooperativeMatrixOperandsBits a) <> (CooperativeMatrixOperandsBits b) = CooperativeMatrixOperandsBits (a .|. b)

instance Monoid CooperativeMatrixOperands where
  mempty = CooperativeMatrixOperandsBits 0

pattern MatrixASignedComponentsKHR :: CooperativeMatrixOperandsBits
pattern MatrixASignedComponentsKHR = CooperativeMatrixOperandsBits 0x00000001

pattern MatrixBSignedComponentsKHR :: CooperativeMatrixOperandsBits
pattern MatrixBSignedComponentsKHR = CooperativeMatrixOperandsBits 0x00000002

pattern MatrixCSignedComponentsKHR :: CooperativeMatrixOperandsBits
pattern MatrixCSignedComponentsKHR = CooperativeMatrixOperandsBits 0x00000004

pattern MatrixResultSignedComponentsKHR :: CooperativeMatrixOperandsBits
pattern MatrixResultSignedComponentsKHR = CooperativeMatrixOperandsBits 0x00000008

pattern SaturatingAccumulationKHR :: CooperativeMatrixOperandsBits
pattern SaturatingAccumulationKHR = CooperativeMatrixOperandsBits 0x00000010