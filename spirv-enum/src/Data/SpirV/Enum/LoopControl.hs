{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.LoopControl where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type LoopControl = LoopControlBits

newtype LoopControlBits = LoopControlBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup LoopControl where
  (LoopControlBits a) <> (LoopControlBits b) = LoopControlBits (a .|. b)

instance Monoid LoopControl where
  mempty = LoopControlBits 0

pattern Unroll :: LoopControlBits
pattern Unroll = LoopControlBits 0x00000001

pattern DontUnroll :: LoopControlBits
pattern DontUnroll = LoopControlBits 0x00000002

pattern DependencyInfinite :: LoopControlBits
pattern DependencyInfinite = LoopControlBits 0x00000004

pattern DependencyLength :: LoopControlBits
pattern DependencyLength = LoopControlBits 0x00000008

pattern MinIterations :: LoopControlBits
pattern MinIterations = LoopControlBits 0x00000010

pattern MaxIterations :: LoopControlBits
pattern MaxIterations = LoopControlBits 0x00000020

pattern IterationMultiple :: LoopControlBits
pattern IterationMultiple = LoopControlBits 0x00000040

pattern PeelCount :: LoopControlBits
pattern PeelCount = LoopControlBits 0x00000080

pattern PartialCount :: LoopControlBits
pattern PartialCount = LoopControlBits 0x00000100

pattern InitiationIntervalINTEL :: LoopControlBits
pattern InitiationIntervalINTEL = LoopControlBits 0x00010000

pattern MaxConcurrencyINTEL :: LoopControlBits
pattern MaxConcurrencyINTEL = LoopControlBits 0x00020000

pattern DependencyArrayINTEL :: LoopControlBits
pattern DependencyArrayINTEL = LoopControlBits 0x00040000

pattern PipelineEnableINTEL :: LoopControlBits
pattern PipelineEnableINTEL = LoopControlBits 0x00080000

pattern LoopCoalesceINTEL :: LoopControlBits
pattern LoopCoalesceINTEL = LoopControlBits 0x00100000

pattern MaxInterleavingINTEL :: LoopControlBits
pattern MaxInterleavingINTEL = LoopControlBits 0x00200000

pattern SpeculatedIterationsINTEL :: LoopControlBits
pattern SpeculatedIterationsINTEL = LoopControlBits 0x00400000

pattern NoFusionINTEL :: LoopControlBits
pattern NoFusionINTEL = LoopControlBits 0x00800000

pattern LoopCountINTEL :: LoopControlBits
pattern LoopCountINTEL = LoopControlBits 0x01000000

pattern MaxReinvocationDelayINTEL :: LoopControlBits
pattern MaxReinvocationDelayINTEL = LoopControlBits 0x02000000