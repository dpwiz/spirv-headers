{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.MemorySemantics where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type MemorySemantics = MemorySemanticsBits

newtype MemorySemanticsBits = MemorySemanticsBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup MemorySemantics where
  (MemorySemanticsBits a) <> (MemorySemanticsBits b) = MemorySemanticsBits (a .|. b)

instance Monoid MemorySemantics where
  mempty = MemorySemanticsBits 0

pattern Acquire :: MemorySemanticsBits
pattern Acquire = MemorySemanticsBits 0x00000002

pattern Release :: MemorySemanticsBits
pattern Release = MemorySemanticsBits 0x00000004

pattern AcquireRelease :: MemorySemanticsBits
pattern AcquireRelease = MemorySemanticsBits 0x00000008

pattern SequentiallyConsistent :: MemorySemanticsBits
pattern SequentiallyConsistent = MemorySemanticsBits 0x00000010

pattern UniformMemory :: MemorySemanticsBits
pattern UniformMemory = MemorySemanticsBits 0x00000040

pattern SubgroupMemory :: MemorySemanticsBits
pattern SubgroupMemory = MemorySemanticsBits 0x00000080

pattern WorkgroupMemory :: MemorySemanticsBits
pattern WorkgroupMemory = MemorySemanticsBits 0x00000100

pattern CrossWorkgroupMemory :: MemorySemanticsBits
pattern CrossWorkgroupMemory = MemorySemanticsBits 0x00000200

pattern AtomicCounterMemory :: MemorySemanticsBits
pattern AtomicCounterMemory = MemorySemanticsBits 0x00000400

pattern ImageMemory :: MemorySemanticsBits
pattern ImageMemory = MemorySemanticsBits 0x00000800

pattern OutputMemory :: MemorySemanticsBits
pattern OutputMemory = MemorySemanticsBits 0x00001000

pattern OutputMemoryKHR :: MemorySemanticsBits
pattern OutputMemoryKHR = MemorySemanticsBits 0x00001000

pattern MakeAvailable :: MemorySemanticsBits
pattern MakeAvailable = MemorySemanticsBits 0x00002000

pattern MakeAvailableKHR :: MemorySemanticsBits
pattern MakeAvailableKHR = MemorySemanticsBits 0x00002000

pattern MakeVisible :: MemorySemanticsBits
pattern MakeVisible = MemorySemanticsBits 0x00004000

pattern MakeVisibleKHR :: MemorySemanticsBits
pattern MakeVisibleKHR = MemorySemanticsBits 0x00004000

pattern Volatile :: MemorySemanticsBits
pattern Volatile = MemorySemanticsBits 0x00008000