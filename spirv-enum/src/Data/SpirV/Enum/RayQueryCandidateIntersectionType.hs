{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.RayQueryCandidateIntersectionType where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype RayQueryCandidateIntersectionType = RayQueryCandidateIntersectionType Int32
  deriving newtype (Eq, Ord, Storable)

instance Show RayQueryCandidateIntersectionType where
  showsPrec p (RayQueryCandidateIntersectionType v) = case v of
    0 -> showString "RayQueryCandidateIntersectionTriangleKHR"
    1 -> showString "RayQueryCandidateIntersectionAABBKHR"
    x -> showParen (p > 10) $ showString "RayQueryCandidateIntersectionType " . showsPrec (p + 1) x

pattern RayQueryCandidateIntersectionTriangleKHR :: RayQueryCandidateIntersectionType
pattern RayQueryCandidateIntersectionTriangleKHR = RayQueryCandidateIntersectionType 0

pattern RayQueryCandidateIntersectionAABBKHR :: RayQueryCandidateIntersectionType
pattern RayQueryCandidateIntersectionAABBKHR = RayQueryCandidateIntersectionType 1