{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.StorageClass where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype StorageClass = StorageClass Int32
  deriving newtype (Eq, Ord, Storable)

instance Show StorageClass where
  showsPrec p (StorageClass v) = case v of
    0 -> showString "UniformConstant"
    1 -> showString "Input"
    2 -> showString "Uniform"
    3 -> showString "Output"
    4 -> showString "Workgroup"
    5 -> showString "CrossWorkgroup"
    6 -> showString "Private"
    7 -> showString "Function"
    8 -> showString "Generic"
    9 -> showString "PushConstant"
    10 -> showString "AtomicCounter"
    11 -> showString "Image"
    12 -> showString "StorageBuffer"
    4172 -> showString "TileImageEXT"
    5068 -> showString "NodePayloadAMDX"
    5076 -> showString "NodeOutputPayloadAMDX"
    5328 -> showString "CallableDataKHR"
    5329 -> showString "IncomingCallableDataKHR"
    5338 -> showString "RayPayloadKHR"
    5339 -> showString "HitAttributeKHR"
    5342 -> showString "IncomingRayPayloadKHR"
    5343 -> showString "ShaderRecordBufferKHR"
    5349 -> showString "PhysicalStorageBuffer"
    5385 -> showString "HitObjectAttributeNV"
    5402 -> showString "TaskPayloadWorkgroupEXT"
    5605 -> showString "CodeSectionINTEL"
    5936 -> showString "DeviceOnlyINTEL"
    5937 -> showString "HostOnlyINTEL"
    x -> showParen (p > 10) $ showString "StorageClass " . showsPrec (p + 1) x

pattern UniformConstant :: StorageClass
pattern UniformConstant = StorageClass 0

pattern Input :: StorageClass
pattern Input = StorageClass 1

pattern Uniform :: StorageClass
pattern Uniform = StorageClass 2

pattern Output :: StorageClass
pattern Output = StorageClass 3

pattern Workgroup :: StorageClass
pattern Workgroup = StorageClass 4

pattern CrossWorkgroup :: StorageClass
pattern CrossWorkgroup = StorageClass 5

pattern Private :: StorageClass
pattern Private = StorageClass 6

pattern Function :: StorageClass
pattern Function = StorageClass 7

pattern Generic :: StorageClass
pattern Generic = StorageClass 8

pattern PushConstant :: StorageClass
pattern PushConstant = StorageClass 9

pattern AtomicCounter :: StorageClass
pattern AtomicCounter = StorageClass 10

pattern Image :: StorageClass
pattern Image = StorageClass 11

pattern StorageBuffer :: StorageClass
pattern StorageBuffer = StorageClass 12

pattern TileImageEXT :: StorageClass
pattern TileImageEXT = StorageClass 4172

pattern NodePayloadAMDX :: StorageClass
pattern NodePayloadAMDX = StorageClass 5068

pattern NodeOutputPayloadAMDX :: StorageClass
pattern NodeOutputPayloadAMDX = StorageClass 5076

pattern CallableDataKHR :: StorageClass
pattern CallableDataKHR = StorageClass 5328

pattern CallableDataNV :: StorageClass
pattern CallableDataNV = StorageClass 5328

pattern IncomingCallableDataKHR :: StorageClass
pattern IncomingCallableDataKHR = StorageClass 5329

pattern IncomingCallableDataNV :: StorageClass
pattern IncomingCallableDataNV = StorageClass 5329

pattern RayPayloadKHR :: StorageClass
pattern RayPayloadKHR = StorageClass 5338

pattern RayPayloadNV :: StorageClass
pattern RayPayloadNV = StorageClass 5338

pattern HitAttributeKHR :: StorageClass
pattern HitAttributeKHR = StorageClass 5339

pattern HitAttributeNV :: StorageClass
pattern HitAttributeNV = StorageClass 5339

pattern IncomingRayPayloadKHR :: StorageClass
pattern IncomingRayPayloadKHR = StorageClass 5342

pattern IncomingRayPayloadNV :: StorageClass
pattern IncomingRayPayloadNV = StorageClass 5342

pattern ShaderRecordBufferKHR :: StorageClass
pattern ShaderRecordBufferKHR = StorageClass 5343

pattern ShaderRecordBufferNV :: StorageClass
pattern ShaderRecordBufferNV = StorageClass 5343

pattern PhysicalStorageBuffer :: StorageClass
pattern PhysicalStorageBuffer = StorageClass 5349

pattern PhysicalStorageBufferEXT :: StorageClass
pattern PhysicalStorageBufferEXT = StorageClass 5349

pattern HitObjectAttributeNV :: StorageClass
pattern HitObjectAttributeNV = StorageClass 5385

pattern TaskPayloadWorkgroupEXT :: StorageClass
pattern TaskPayloadWorkgroupEXT = StorageClass 5402

pattern CodeSectionINTEL :: StorageClass
pattern CodeSectionINTEL = StorageClass 5605

pattern DeviceOnlyINTEL :: StorageClass
pattern DeviceOnlyINTEL = StorageClass 5936

pattern HostOnlyINTEL :: StorageClass
pattern HostOnlyINTEL = StorageClass 5937