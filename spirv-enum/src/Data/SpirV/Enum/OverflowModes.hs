{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.OverflowModes where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype OverflowModes = OverflowModes Int32
  deriving newtype (Eq, Ord, Storable)

instance Show OverflowModes where
  showsPrec p (OverflowModes v) = case v of
    0 -> showString "WRAP"
    1 -> showString "SAT"
    2 -> showString "SAT_ZERO"
    3 -> showString "SAT_SYM"
    x -> showParen (p > 10) $ showString "OverflowModes " . showsPrec (p + 1) x

pattern WRAP :: OverflowModes
pattern WRAP = OverflowModes 0

pattern SAT :: OverflowModes
pattern SAT = OverflowModes 1

pattern SAT_ZERO :: OverflowModes
pattern SAT_ZERO = OverflowModes 2

pattern SAT_SYM :: OverflowModes
pattern SAT_SYM = OverflowModes 3