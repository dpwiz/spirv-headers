{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FPOperationMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype FPOperationMode = FPOperationMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show FPOperationMode where
  showsPrec p (FPOperationMode v) = case v of
    0 -> showString "IEEE"
    1 -> showString "ALT"
    x -> showParen (p > 10) $ showString "FPOperationMode " . showsPrec (p + 1) x

pattern IEEE :: FPOperationMode
pattern IEEE = FPOperationMode 0

pattern ALT :: FPOperationMode
pattern ALT = FPOperationMode 1