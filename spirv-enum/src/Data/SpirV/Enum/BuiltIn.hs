{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.BuiltIn where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype BuiltIn = BuiltIn Int32
  deriving newtype (Eq, Ord, Storable)

instance Show BuiltIn where
  showsPrec p (BuiltIn v) = case v of
    0 -> showString "Position"
    1 -> showString "PointSize"
    3 -> showString "ClipDistance"
    4 -> showString "CullDistance"
    5 -> showString "VertexId"
    6 -> showString "InstanceId"
    7 -> showString "PrimitiveId"
    8 -> showString "InvocationId"
    9 -> showString "Layer"
    10 -> showString "ViewportIndex"
    11 -> showString "TessLevelOuter"
    12 -> showString "TessLevelInner"
    13 -> showString "TessCoord"
    14 -> showString "PatchVertices"
    15 -> showString "FragCoord"
    16 -> showString "PointCoord"
    17 -> showString "FrontFacing"
    18 -> showString "SampleId"
    19 -> showString "SamplePosition"
    20 -> showString "SampleMask"
    22 -> showString "FragDepth"
    23 -> showString "HelperInvocation"
    24 -> showString "NumWorkgroups"
    25 -> showString "WorkgroupSize"
    26 -> showString "WorkgroupId"
    27 -> showString "LocalInvocationId"
    28 -> showString "GlobalInvocationId"
    29 -> showString "LocalInvocationIndex"
    30 -> showString "WorkDim"
    31 -> showString "GlobalSize"
    32 -> showString "EnqueuedWorkgroupSize"
    33 -> showString "GlobalOffset"
    34 -> showString "GlobalLinearId"
    36 -> showString "SubgroupSize"
    37 -> showString "SubgroupMaxSize"
    38 -> showString "NumSubgroups"
    39 -> showString "NumEnqueuedSubgroups"
    40 -> showString "SubgroupId"
    41 -> showString "SubgroupLocalInvocationId"
    42 -> showString "VertexIndex"
    43 -> showString "InstanceIndex"
    4160 -> showString "CoreIDARM"
    4161 -> showString "CoreCountARM"
    4162 -> showString "CoreMaxIDARM"
    4163 -> showString "WarpIDARM"
    4164 -> showString "WarpMaxIDARM"
    4416 -> showString "SubgroupEqMask"
    4417 -> showString "SubgroupGeMask"
    4418 -> showString "SubgroupGtMask"
    4419 -> showString "SubgroupLeMask"
    4420 -> showString "SubgroupLtMask"
    4424 -> showString "BaseVertex"
    4425 -> showString "BaseInstance"
    4426 -> showString "DrawIndex"
    4432 -> showString "PrimitiveShadingRateKHR"
    4438 -> showString "DeviceIndex"
    4440 -> showString "ViewIndex"
    4444 -> showString "ShadingRateKHR"
    4992 -> showString "BaryCoordNoPerspAMD"
    4993 -> showString "BaryCoordNoPerspCentroidAMD"
    4994 -> showString "BaryCoordNoPerspSampleAMD"
    4995 -> showString "BaryCoordSmoothAMD"
    4996 -> showString "BaryCoordSmoothCentroidAMD"
    4997 -> showString "BaryCoordSmoothSampleAMD"
    4998 -> showString "BaryCoordPullModelAMD"
    5014 -> showString "FragStencilRefEXT"
    5021 -> showString "CoalescedInputCountAMDX"
    5073 -> showString "ShaderIndexAMDX"
    5253 -> showString "ViewportMaskNV"
    5257 -> showString "SecondaryPositionNV"
    5258 -> showString "SecondaryViewportMaskNV"
    5261 -> showString "PositionPerViewNV"
    5262 -> showString "ViewportMaskPerViewNV"
    5264 -> showString "FullyCoveredEXT"
    5274 -> showString "TaskCountNV"
    5275 -> showString "PrimitiveCountNV"
    5276 -> showString "PrimitiveIndicesNV"
    5277 -> showString "ClipDistancePerViewNV"
    5278 -> showString "CullDistancePerViewNV"
    5279 -> showString "LayerPerViewNV"
    5280 -> showString "MeshViewCountNV"
    5281 -> showString "MeshViewIndicesNV"
    5286 -> showString "BaryCoordKHR"
    5287 -> showString "BaryCoordNoPerspKHR"
    5292 -> showString "FragSizeEXT"
    5293 -> showString "FragInvocationCountEXT"
    5294 -> showString "PrimitivePointIndicesEXT"
    5295 -> showString "PrimitiveLineIndicesEXT"
    5296 -> showString "PrimitiveTriangleIndicesEXT"
    5299 -> showString "CullPrimitiveEXT"
    5319 -> showString "LaunchIdKHR"
    5320 -> showString "LaunchSizeKHR"
    5321 -> showString "WorldRayOriginKHR"
    5322 -> showString "WorldRayDirectionKHR"
    5323 -> showString "ObjectRayOriginKHR"
    5324 -> showString "ObjectRayDirectionKHR"
    5325 -> showString "RayTminKHR"
    5326 -> showString "RayTmaxKHR"
    5327 -> showString "InstanceCustomIndexKHR"
    5330 -> showString "ObjectToWorldKHR"
    5331 -> showString "WorldToObjectKHR"
    5332 -> showString "HitTNV"
    5333 -> showString "HitKindKHR"
    5334 -> showString "CurrentRayTimeNV"
    5335 -> showString "HitTriangleVertexPositionsKHR"
    5337 -> showString "HitMicroTriangleVertexPositionsNV"
    5344 -> showString "HitMicroTriangleVertexBarycentricsNV"
    5351 -> showString "IncomingRayFlagsKHR"
    5352 -> showString "RayGeometryIndexKHR"
    5374 -> showString "WarpsPerSMNV"
    5375 -> showString "SMCountNV"
    5376 -> showString "WarpIDNV"
    5377 -> showString "SMIDNV"
    5405 -> showString "HitKindFrontFacingMicroTriangleNV"
    5406 -> showString "HitKindBackFacingMicroTriangleNV"
    6021 -> showString "CullMaskKHR"
    x -> showParen (p > 10) $ showString "BuiltIn " . showsPrec (p + 1) x

pattern Position :: BuiltIn
pattern Position = BuiltIn 0

pattern PointSize :: BuiltIn
pattern PointSize = BuiltIn 1

pattern ClipDistance :: BuiltIn
pattern ClipDistance = BuiltIn 3

pattern CullDistance :: BuiltIn
pattern CullDistance = BuiltIn 4

pattern VertexId :: BuiltIn
pattern VertexId = BuiltIn 5

pattern InstanceId :: BuiltIn
pattern InstanceId = BuiltIn 6

pattern PrimitiveId :: BuiltIn
pattern PrimitiveId = BuiltIn 7

pattern InvocationId :: BuiltIn
pattern InvocationId = BuiltIn 8

pattern Layer :: BuiltIn
pattern Layer = BuiltIn 9

pattern ViewportIndex :: BuiltIn
pattern ViewportIndex = BuiltIn 10

pattern TessLevelOuter :: BuiltIn
pattern TessLevelOuter = BuiltIn 11

pattern TessLevelInner :: BuiltIn
pattern TessLevelInner = BuiltIn 12

pattern TessCoord :: BuiltIn
pattern TessCoord = BuiltIn 13

pattern PatchVertices :: BuiltIn
pattern PatchVertices = BuiltIn 14

pattern FragCoord :: BuiltIn
pattern FragCoord = BuiltIn 15

pattern PointCoord :: BuiltIn
pattern PointCoord = BuiltIn 16

pattern FrontFacing :: BuiltIn
pattern FrontFacing = BuiltIn 17

pattern SampleId :: BuiltIn
pattern SampleId = BuiltIn 18

pattern SamplePosition :: BuiltIn
pattern SamplePosition = BuiltIn 19

pattern SampleMask :: BuiltIn
pattern SampleMask = BuiltIn 20

pattern FragDepth :: BuiltIn
pattern FragDepth = BuiltIn 22

pattern HelperInvocation :: BuiltIn
pattern HelperInvocation = BuiltIn 23

pattern NumWorkgroups :: BuiltIn
pattern NumWorkgroups = BuiltIn 24

pattern WorkgroupSize :: BuiltIn
pattern WorkgroupSize = BuiltIn 25

pattern WorkgroupId :: BuiltIn
pattern WorkgroupId = BuiltIn 26

pattern LocalInvocationId :: BuiltIn
pattern LocalInvocationId = BuiltIn 27

pattern GlobalInvocationId :: BuiltIn
pattern GlobalInvocationId = BuiltIn 28

pattern LocalInvocationIndex :: BuiltIn
pattern LocalInvocationIndex = BuiltIn 29

pattern WorkDim :: BuiltIn
pattern WorkDim = BuiltIn 30

pattern GlobalSize :: BuiltIn
pattern GlobalSize = BuiltIn 31

pattern EnqueuedWorkgroupSize :: BuiltIn
pattern EnqueuedWorkgroupSize = BuiltIn 32

pattern GlobalOffset :: BuiltIn
pattern GlobalOffset = BuiltIn 33

pattern GlobalLinearId :: BuiltIn
pattern GlobalLinearId = BuiltIn 34

pattern SubgroupSize :: BuiltIn
pattern SubgroupSize = BuiltIn 36

pattern SubgroupMaxSize :: BuiltIn
pattern SubgroupMaxSize = BuiltIn 37

pattern NumSubgroups :: BuiltIn
pattern NumSubgroups = BuiltIn 38

pattern NumEnqueuedSubgroups :: BuiltIn
pattern NumEnqueuedSubgroups = BuiltIn 39

pattern SubgroupId :: BuiltIn
pattern SubgroupId = BuiltIn 40

pattern SubgroupLocalInvocationId :: BuiltIn
pattern SubgroupLocalInvocationId = BuiltIn 41

pattern VertexIndex :: BuiltIn
pattern VertexIndex = BuiltIn 42

pattern InstanceIndex :: BuiltIn
pattern InstanceIndex = BuiltIn 43

pattern CoreIDARM :: BuiltIn
pattern CoreIDARM = BuiltIn 4160

pattern CoreCountARM :: BuiltIn
pattern CoreCountARM = BuiltIn 4161

pattern CoreMaxIDARM :: BuiltIn
pattern CoreMaxIDARM = BuiltIn 4162

pattern WarpIDARM :: BuiltIn
pattern WarpIDARM = BuiltIn 4163

pattern WarpMaxIDARM :: BuiltIn
pattern WarpMaxIDARM = BuiltIn 4164

pattern SubgroupEqMask :: BuiltIn
pattern SubgroupEqMask = BuiltIn 4416

pattern SubgroupEqMaskKHR :: BuiltIn
pattern SubgroupEqMaskKHR = BuiltIn 4416

pattern SubgroupGeMask :: BuiltIn
pattern SubgroupGeMask = BuiltIn 4417

pattern SubgroupGeMaskKHR :: BuiltIn
pattern SubgroupGeMaskKHR = BuiltIn 4417

pattern SubgroupGtMask :: BuiltIn
pattern SubgroupGtMask = BuiltIn 4418

pattern SubgroupGtMaskKHR :: BuiltIn
pattern SubgroupGtMaskKHR = BuiltIn 4418

pattern SubgroupLeMask :: BuiltIn
pattern SubgroupLeMask = BuiltIn 4419

pattern SubgroupLeMaskKHR :: BuiltIn
pattern SubgroupLeMaskKHR = BuiltIn 4419

pattern SubgroupLtMask :: BuiltIn
pattern SubgroupLtMask = BuiltIn 4420

pattern SubgroupLtMaskKHR :: BuiltIn
pattern SubgroupLtMaskKHR = BuiltIn 4420

pattern BaseVertex :: BuiltIn
pattern BaseVertex = BuiltIn 4424

pattern BaseInstance :: BuiltIn
pattern BaseInstance = BuiltIn 4425

pattern DrawIndex :: BuiltIn
pattern DrawIndex = BuiltIn 4426

pattern PrimitiveShadingRateKHR :: BuiltIn
pattern PrimitiveShadingRateKHR = BuiltIn 4432

pattern DeviceIndex :: BuiltIn
pattern DeviceIndex = BuiltIn 4438

pattern ViewIndex :: BuiltIn
pattern ViewIndex = BuiltIn 4440

pattern ShadingRateKHR :: BuiltIn
pattern ShadingRateKHR = BuiltIn 4444

pattern BaryCoordNoPerspAMD :: BuiltIn
pattern BaryCoordNoPerspAMD = BuiltIn 4992

pattern BaryCoordNoPerspCentroidAMD :: BuiltIn
pattern BaryCoordNoPerspCentroidAMD = BuiltIn 4993

pattern BaryCoordNoPerspSampleAMD :: BuiltIn
pattern BaryCoordNoPerspSampleAMD = BuiltIn 4994

pattern BaryCoordSmoothAMD :: BuiltIn
pattern BaryCoordSmoothAMD = BuiltIn 4995

pattern BaryCoordSmoothCentroidAMD :: BuiltIn
pattern BaryCoordSmoothCentroidAMD = BuiltIn 4996

pattern BaryCoordSmoothSampleAMD :: BuiltIn
pattern BaryCoordSmoothSampleAMD = BuiltIn 4997

pattern BaryCoordPullModelAMD :: BuiltIn
pattern BaryCoordPullModelAMD = BuiltIn 4998

pattern FragStencilRefEXT :: BuiltIn
pattern FragStencilRefEXT = BuiltIn 5014

pattern CoalescedInputCountAMDX :: BuiltIn
pattern CoalescedInputCountAMDX = BuiltIn 5021

pattern ShaderIndexAMDX :: BuiltIn
pattern ShaderIndexAMDX = BuiltIn 5073

pattern ViewportMaskNV :: BuiltIn
pattern ViewportMaskNV = BuiltIn 5253

pattern SecondaryPositionNV :: BuiltIn
pattern SecondaryPositionNV = BuiltIn 5257

pattern SecondaryViewportMaskNV :: BuiltIn
pattern SecondaryViewportMaskNV = BuiltIn 5258

pattern PositionPerViewNV :: BuiltIn
pattern PositionPerViewNV = BuiltIn 5261

pattern ViewportMaskPerViewNV :: BuiltIn
pattern ViewportMaskPerViewNV = BuiltIn 5262

pattern FullyCoveredEXT :: BuiltIn
pattern FullyCoveredEXT = BuiltIn 5264

pattern TaskCountNV :: BuiltIn
pattern TaskCountNV = BuiltIn 5274

pattern PrimitiveCountNV :: BuiltIn
pattern PrimitiveCountNV = BuiltIn 5275

pattern PrimitiveIndicesNV :: BuiltIn
pattern PrimitiveIndicesNV = BuiltIn 5276

pattern ClipDistancePerViewNV :: BuiltIn
pattern ClipDistancePerViewNV = BuiltIn 5277

pattern CullDistancePerViewNV :: BuiltIn
pattern CullDistancePerViewNV = BuiltIn 5278

pattern LayerPerViewNV :: BuiltIn
pattern LayerPerViewNV = BuiltIn 5279

pattern MeshViewCountNV :: BuiltIn
pattern MeshViewCountNV = BuiltIn 5280

pattern MeshViewIndicesNV :: BuiltIn
pattern MeshViewIndicesNV = BuiltIn 5281

pattern BaryCoordKHR :: BuiltIn
pattern BaryCoordKHR = BuiltIn 5286

pattern BaryCoordNV :: BuiltIn
pattern BaryCoordNV = BuiltIn 5286

pattern BaryCoordNoPerspKHR :: BuiltIn
pattern BaryCoordNoPerspKHR = BuiltIn 5287

pattern BaryCoordNoPerspNV :: BuiltIn
pattern BaryCoordNoPerspNV = BuiltIn 5287

pattern FragSizeEXT :: BuiltIn
pattern FragSizeEXT = BuiltIn 5292

pattern FragmentSizeNV :: BuiltIn
pattern FragmentSizeNV = BuiltIn 5292

pattern FragInvocationCountEXT :: BuiltIn
pattern FragInvocationCountEXT = BuiltIn 5293

pattern InvocationsPerPixelNV :: BuiltIn
pattern InvocationsPerPixelNV = BuiltIn 5293

pattern PrimitivePointIndicesEXT :: BuiltIn
pattern PrimitivePointIndicesEXT = BuiltIn 5294

pattern PrimitiveLineIndicesEXT :: BuiltIn
pattern PrimitiveLineIndicesEXT = BuiltIn 5295

pattern PrimitiveTriangleIndicesEXT :: BuiltIn
pattern PrimitiveTriangleIndicesEXT = BuiltIn 5296

pattern CullPrimitiveEXT :: BuiltIn
pattern CullPrimitiveEXT = BuiltIn 5299

pattern LaunchIdKHR :: BuiltIn
pattern LaunchIdKHR = BuiltIn 5319

pattern LaunchIdNV :: BuiltIn
pattern LaunchIdNV = BuiltIn 5319

pattern LaunchSizeKHR :: BuiltIn
pattern LaunchSizeKHR = BuiltIn 5320

pattern LaunchSizeNV :: BuiltIn
pattern LaunchSizeNV = BuiltIn 5320

pattern WorldRayOriginKHR :: BuiltIn
pattern WorldRayOriginKHR = BuiltIn 5321

pattern WorldRayOriginNV :: BuiltIn
pattern WorldRayOriginNV = BuiltIn 5321

pattern WorldRayDirectionKHR :: BuiltIn
pattern WorldRayDirectionKHR = BuiltIn 5322

pattern WorldRayDirectionNV :: BuiltIn
pattern WorldRayDirectionNV = BuiltIn 5322

pattern ObjectRayOriginKHR :: BuiltIn
pattern ObjectRayOriginKHR = BuiltIn 5323

pattern ObjectRayOriginNV :: BuiltIn
pattern ObjectRayOriginNV = BuiltIn 5323

pattern ObjectRayDirectionKHR :: BuiltIn
pattern ObjectRayDirectionKHR = BuiltIn 5324

pattern ObjectRayDirectionNV :: BuiltIn
pattern ObjectRayDirectionNV = BuiltIn 5324

pattern RayTminKHR :: BuiltIn
pattern RayTminKHR = BuiltIn 5325

pattern RayTminNV :: BuiltIn
pattern RayTminNV = BuiltIn 5325

pattern RayTmaxKHR :: BuiltIn
pattern RayTmaxKHR = BuiltIn 5326

pattern RayTmaxNV :: BuiltIn
pattern RayTmaxNV = BuiltIn 5326

pattern InstanceCustomIndexKHR :: BuiltIn
pattern InstanceCustomIndexKHR = BuiltIn 5327

pattern InstanceCustomIndexNV :: BuiltIn
pattern InstanceCustomIndexNV = BuiltIn 5327

pattern ObjectToWorldKHR :: BuiltIn
pattern ObjectToWorldKHR = BuiltIn 5330

pattern ObjectToWorldNV :: BuiltIn
pattern ObjectToWorldNV = BuiltIn 5330

pattern WorldToObjectKHR :: BuiltIn
pattern WorldToObjectKHR = BuiltIn 5331

pattern WorldToObjectNV :: BuiltIn
pattern WorldToObjectNV = BuiltIn 5331

pattern HitTNV :: BuiltIn
pattern HitTNV = BuiltIn 5332

pattern HitKindKHR :: BuiltIn
pattern HitKindKHR = BuiltIn 5333

pattern HitKindNV :: BuiltIn
pattern HitKindNV = BuiltIn 5333

pattern CurrentRayTimeNV :: BuiltIn
pattern CurrentRayTimeNV = BuiltIn 5334

pattern HitTriangleVertexPositionsKHR :: BuiltIn
pattern HitTriangleVertexPositionsKHR = BuiltIn 5335

pattern HitMicroTriangleVertexPositionsNV :: BuiltIn
pattern HitMicroTriangleVertexPositionsNV = BuiltIn 5337

pattern HitMicroTriangleVertexBarycentricsNV :: BuiltIn
pattern HitMicroTriangleVertexBarycentricsNV = BuiltIn 5344

pattern IncomingRayFlagsKHR :: BuiltIn
pattern IncomingRayFlagsKHR = BuiltIn 5351

pattern IncomingRayFlagsNV :: BuiltIn
pattern IncomingRayFlagsNV = BuiltIn 5351

pattern RayGeometryIndexKHR :: BuiltIn
pattern RayGeometryIndexKHR = BuiltIn 5352

pattern WarpsPerSMNV :: BuiltIn
pattern WarpsPerSMNV = BuiltIn 5374

pattern SMCountNV :: BuiltIn
pattern SMCountNV = BuiltIn 5375

pattern WarpIDNV :: BuiltIn
pattern WarpIDNV = BuiltIn 5376

pattern SMIDNV :: BuiltIn
pattern SMIDNV = BuiltIn 5377

pattern HitKindFrontFacingMicroTriangleNV :: BuiltIn
pattern HitKindFrontFacingMicroTriangleNV = BuiltIn 5405

pattern HitKindBackFacingMicroTriangleNV :: BuiltIn
pattern HitKindBackFacingMicroTriangleNV = BuiltIn 5406

pattern CullMaskKHR :: BuiltIn
pattern CullMaskKHR = BuiltIn 6021