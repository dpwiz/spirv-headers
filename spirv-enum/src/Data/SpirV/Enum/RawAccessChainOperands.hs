{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.RawAccessChainOperands where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type RawAccessChainOperands = RawAccessChainOperandsBits

newtype RawAccessChainOperandsBits = RawAccessChainOperandsBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup RawAccessChainOperands where
  (RawAccessChainOperandsBits a) <> (RawAccessChainOperandsBits b) = RawAccessChainOperandsBits (a .|. b)

instance Monoid RawAccessChainOperands where
  mempty = RawAccessChainOperandsBits 0

pattern RobustnessPerComponentNV :: RawAccessChainOperandsBits
pattern RobustnessPerComponentNV = RawAccessChainOperandsBits 0x00000001

pattern RobustnessPerElementNV :: RawAccessChainOperandsBits
pattern RobustnessPerElementNV = RawAccessChainOperandsBits 0x00000002