{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.PackedVectorFormat where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype PackedVectorFormat = PackedVectorFormat Int32
  deriving newtype (Eq, Ord, Storable)

instance Show PackedVectorFormat where
  showsPrec p (PackedVectorFormat v) = case v of
    0 -> showString "PackedVectorFormat4x8Bit"
    x -> showParen (p > 10) $ showString "PackedVectorFormat " . showsPrec (p + 1) x

pattern PackedVectorFormat4x8Bit :: PackedVectorFormat
pattern PackedVectorFormat4x8Bit = PackedVectorFormat 0

pattern PackedVectorFormat4x8BitKHR :: PackedVectorFormat
pattern PackedVectorFormat4x8BitKHR = PackedVectorFormat 0