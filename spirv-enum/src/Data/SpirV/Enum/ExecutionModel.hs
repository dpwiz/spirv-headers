{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ExecutionModel where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype ExecutionModel = ExecutionModel Int32
  deriving newtype (Eq, Ord, Storable)

instance Show ExecutionModel where
  showsPrec p (ExecutionModel v) = case v of
    0 -> showString "Vertex"
    1 -> showString "TessellationControl"
    2 -> showString "TessellationEvaluation"
    3 -> showString "Geometry"
    4 -> showString "Fragment"
    5 -> showString "GLCompute"
    6 -> showString "Kernel"
    5267 -> showString "TaskNV"
    5268 -> showString "MeshNV"
    5313 -> showString "RayGenerationKHR"
    5314 -> showString "IntersectionKHR"
    5315 -> showString "AnyHitKHR"
    5316 -> showString "ClosestHitKHR"
    5317 -> showString "MissKHR"
    5318 -> showString "CallableKHR"
    5364 -> showString "TaskEXT"
    5365 -> showString "MeshEXT"
    x -> showParen (p > 10) $ showString "ExecutionModel " . showsPrec (p + 1) x

pattern Vertex :: ExecutionModel
pattern Vertex = ExecutionModel 0

pattern TessellationControl :: ExecutionModel
pattern TessellationControl = ExecutionModel 1

pattern TessellationEvaluation :: ExecutionModel
pattern TessellationEvaluation = ExecutionModel 2

pattern Geometry :: ExecutionModel
pattern Geometry = ExecutionModel 3

pattern Fragment :: ExecutionModel
pattern Fragment = ExecutionModel 4

pattern GLCompute :: ExecutionModel
pattern GLCompute = ExecutionModel 5

pattern Kernel :: ExecutionModel
pattern Kernel = ExecutionModel 6

pattern TaskNV :: ExecutionModel
pattern TaskNV = ExecutionModel 5267

pattern MeshNV :: ExecutionModel
pattern MeshNV = ExecutionModel 5268

pattern RayGenerationKHR :: ExecutionModel
pattern RayGenerationKHR = ExecutionModel 5313

pattern RayGenerationNV :: ExecutionModel
pattern RayGenerationNV = ExecutionModel 5313

pattern IntersectionKHR :: ExecutionModel
pattern IntersectionKHR = ExecutionModel 5314

pattern IntersectionNV :: ExecutionModel
pattern IntersectionNV = ExecutionModel 5314

pattern AnyHitKHR :: ExecutionModel
pattern AnyHitKHR = ExecutionModel 5315

pattern AnyHitNV :: ExecutionModel
pattern AnyHitNV = ExecutionModel 5315

pattern ClosestHitKHR :: ExecutionModel
pattern ClosestHitKHR = ExecutionModel 5316

pattern ClosestHitNV :: ExecutionModel
pattern ClosestHitNV = ExecutionModel 5316

pattern MissKHR :: ExecutionModel
pattern MissKHR = ExecutionModel 5317

pattern MissNV :: ExecutionModel
pattern MissNV = ExecutionModel 5317

pattern CallableKHR :: ExecutionModel
pattern CallableKHR = ExecutionModel 5318

pattern CallableNV :: ExecutionModel
pattern CallableNV = ExecutionModel 5318

pattern TaskEXT :: ExecutionModel
pattern TaskEXT = ExecutionModel 5364

pattern MeshEXT :: ExecutionModel
pattern MeshEXT = ExecutionModel 5365