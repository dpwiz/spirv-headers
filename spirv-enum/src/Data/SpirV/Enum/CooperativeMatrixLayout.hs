{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.CooperativeMatrixLayout where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype CooperativeMatrixLayout = CooperativeMatrixLayout Int32
  deriving newtype (Eq, Ord, Storable)

instance Show CooperativeMatrixLayout where
  showsPrec p (CooperativeMatrixLayout v) = case v of
    0 -> showString "RowMajorKHR"
    1 -> showString "ColumnMajorKHR"
    4202 -> showString "RowBlockedInterleavedARM"
    4203 -> showString "ColumnBlockedInterleavedARM"
    x -> showParen (p > 10) $ showString "CooperativeMatrixLayout " . showsPrec (p + 1) x

pattern RowMajorKHR :: CooperativeMatrixLayout
pattern RowMajorKHR = CooperativeMatrixLayout 0

pattern ColumnMajorKHR :: CooperativeMatrixLayout
pattern ColumnMajorKHR = CooperativeMatrixLayout 1

pattern RowBlockedInterleavedARM :: CooperativeMatrixLayout
pattern RowBlockedInterleavedARM = CooperativeMatrixLayout 4202

pattern ColumnBlockedInterleavedARM :: CooperativeMatrixLayout
pattern ColumnBlockedInterleavedARM = CooperativeMatrixLayout 4203