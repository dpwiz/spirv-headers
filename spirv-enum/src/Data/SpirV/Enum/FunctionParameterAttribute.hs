{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FunctionParameterAttribute where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype FunctionParameterAttribute = FunctionParameterAttribute Int32
  deriving newtype (Eq, Ord, Storable)

instance Show FunctionParameterAttribute where
  showsPrec p (FunctionParameterAttribute v) = case v of
    0 -> showString "Zext"
    1 -> showString "Sext"
    2 -> showString "ByVal"
    3 -> showString "Sret"
    4 -> showString "NoAlias"
    5 -> showString "NoCapture"
    6 -> showString "NoWrite"
    7 -> showString "NoReadWrite"
    5940 -> showString "RuntimeAlignedINTEL"
    x -> showParen (p > 10) $ showString "FunctionParameterAttribute " . showsPrec (p + 1) x

pattern Zext :: FunctionParameterAttribute
pattern Zext = FunctionParameterAttribute 0

pattern Sext :: FunctionParameterAttribute
pattern Sext = FunctionParameterAttribute 1

pattern ByVal :: FunctionParameterAttribute
pattern ByVal = FunctionParameterAttribute 2

pattern Sret :: FunctionParameterAttribute
pattern Sret = FunctionParameterAttribute 3

pattern NoAlias :: FunctionParameterAttribute
pattern NoAlias = FunctionParameterAttribute 4

pattern NoCapture :: FunctionParameterAttribute
pattern NoCapture = FunctionParameterAttribute 5

pattern NoWrite :: FunctionParameterAttribute
pattern NoWrite = FunctionParameterAttribute 6

pattern NoReadWrite :: FunctionParameterAttribute
pattern NoReadWrite = FunctionParameterAttribute 7

pattern RuntimeAlignedINTEL :: FunctionParameterAttribute
pattern RuntimeAlignedINTEL = FunctionParameterAttribute 5940