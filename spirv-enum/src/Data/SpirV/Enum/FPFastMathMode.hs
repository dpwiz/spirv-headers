{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FPFastMathMode where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type FPFastMathMode = FPFastMathModeBits

newtype FPFastMathModeBits = FPFastMathModeBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup FPFastMathMode where
  (FPFastMathModeBits a) <> (FPFastMathModeBits b) = FPFastMathModeBits (a .|. b)

instance Monoid FPFastMathMode where
  mempty = FPFastMathModeBits 0

pattern NotNaN :: FPFastMathModeBits
pattern NotNaN = FPFastMathModeBits 0x00000001

pattern NotInf :: FPFastMathModeBits
pattern NotInf = FPFastMathModeBits 0x00000002

pattern NSZ :: FPFastMathModeBits
pattern NSZ = FPFastMathModeBits 0x00000004

pattern AllowRecip :: FPFastMathModeBits
pattern AllowRecip = FPFastMathModeBits 0x00000008

pattern Fast :: FPFastMathModeBits
pattern Fast = FPFastMathModeBits 0x00000010

pattern AllowContract :: FPFastMathModeBits
pattern AllowContract = FPFastMathModeBits 0x00010000

pattern AllowContractFastINTEL :: FPFastMathModeBits
pattern AllowContractFastINTEL = FPFastMathModeBits 0x00010000

pattern AllowReassoc :: FPFastMathModeBits
pattern AllowReassoc = FPFastMathModeBits 0x00020000

pattern AllowReassocINTEL :: FPFastMathModeBits
pattern AllowReassocINTEL = FPFastMathModeBits 0x00020000

pattern AllowTransform :: FPFastMathModeBits
pattern AllowTransform = FPFastMathModeBits 0x00040000