{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.CooperativeMatrixUse where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype CooperativeMatrixUse = CooperativeMatrixUse Int32
  deriving newtype (Eq, Ord, Storable)

instance Show CooperativeMatrixUse where
  showsPrec p (CooperativeMatrixUse v) = case v of
    0 -> showString "MatrixAKHR"
    1 -> showString "MatrixBKHR"
    2 -> showString "MatrixAccumulatorKHR"
    x -> showParen (p > 10) $ showString "CooperativeMatrixUse " . showsPrec (p + 1) x

pattern MatrixAKHR :: CooperativeMatrixUse
pattern MatrixAKHR = CooperativeMatrixUse 0

pattern MatrixBKHR :: CooperativeMatrixUse
pattern MatrixBKHR = CooperativeMatrixUse 1

pattern MatrixAccumulatorKHR :: CooperativeMatrixUse
pattern MatrixAccumulatorKHR = CooperativeMatrixUse 2