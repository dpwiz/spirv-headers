{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.Decoration where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype Decoration = Decoration Int32
  deriving newtype (Eq, Ord, Storable)

instance Show Decoration where
  showsPrec p (Decoration v) = case v of
    0 -> showString "RelaxedPrecision"
    1 -> showString "SpecId"
    2 -> showString "Block"
    3 -> showString "BufferBlock"
    4 -> showString "RowMajor"
    5 -> showString "ColMajor"
    6 -> showString "ArrayStride"
    7 -> showString "MatrixStride"
    8 -> showString "GLSLShared"
    9 -> showString "GLSLPacked"
    10 -> showString "CPacked"
    11 -> showString "BuiltIn"
    13 -> showString "NoPerspective"
    14 -> showString "Flat"
    15 -> showString "Patch"
    16 -> showString "Centroid"
    17 -> showString "Sample"
    18 -> showString "Invariant"
    19 -> showString "Restrict"
    20 -> showString "Aliased"
    21 -> showString "Volatile"
    22 -> showString "Constant"
    23 -> showString "Coherent"
    24 -> showString "NonWritable"
    25 -> showString "NonReadable"
    26 -> showString "Uniform"
    27 -> showString "UniformId"
    28 -> showString "SaturatedConversion"
    29 -> showString "Stream"
    30 -> showString "Location"
    31 -> showString "Component"
    32 -> showString "Index"
    33 -> showString "Binding"
    34 -> showString "DescriptorSet"
    35 -> showString "Offset"
    36 -> showString "XfbBuffer"
    37 -> showString "XfbStride"
    38 -> showString "FuncParamAttr"
    39 -> showString "FPRoundingMode"
    40 -> showString "FPFastMathMode"
    41 -> showString "LinkageAttributes"
    42 -> showString "NoContraction"
    43 -> showString "InputAttachmentIndex"
    44 -> showString "Alignment"
    45 -> showString "MaxByteOffset"
    46 -> showString "AlignmentId"
    47 -> showString "MaxByteOffsetId"
    4469 -> showString "NoSignedWrap"
    4470 -> showString "NoUnsignedWrap"
    4487 -> showString "WeightTextureQCOM"
    4488 -> showString "BlockMatchTextureQCOM"
    4499 -> showString "BlockMatchSamplerQCOM"
    4999 -> showString "ExplicitInterpAMD"
    5019 -> showString "NodeSharesPayloadLimitsWithAMDX"
    5020 -> showString "NodeMaxPayloadsAMDX"
    5078 -> showString "TrackFinishWritingAMDX"
    5091 -> showString "PayloadNodeNameAMDX"
    5248 -> showString "OverrideCoverageNV"
    5250 -> showString "PassthroughNV"
    5252 -> showString "ViewportRelativeNV"
    5256 -> showString "SecondaryViewportRelativeNV"
    5271 -> showString "PerPrimitiveEXT"
    5272 -> showString "PerViewNV"
    5273 -> showString "PerTaskNV"
    5285 -> showString "PerVertexKHR"
    5300 -> showString "NonUniform"
    5355 -> showString "RestrictPointer"
    5356 -> showString "AliasedPointer"
    5386 -> showString "HitObjectShaderRecordBufferNV"
    5398 -> showString "BindlessSamplerNV"
    5399 -> showString "BindlessImageNV"
    5400 -> showString "BoundSamplerNV"
    5401 -> showString "BoundImageNV"
    5599 -> showString "SIMTCallINTEL"
    5602 -> showString "ReferencedIndirectlyINTEL"
    5607 -> showString "ClobberINTEL"
    5608 -> showString "SideEffectsINTEL"
    5624 -> showString "VectorComputeVariableINTEL"
    5625 -> showString "FuncParamIOKindINTEL"
    5626 -> showString "VectorComputeFunctionINTEL"
    5627 -> showString "StackCallINTEL"
    5628 -> showString "GlobalVariableOffsetINTEL"
    5634 -> showString "CounterBuffer"
    5635 -> showString "HlslSemanticGOOGLE"
    5636 -> showString "UserTypeGOOGLE"
    5822 -> showString "FunctionRoundingModeINTEL"
    5823 -> showString "FunctionDenormModeINTEL"
    5825 -> showString "RegisterINTEL"
    5826 -> showString "MemoryINTEL"
    5827 -> showString "NumbanksINTEL"
    5828 -> showString "BankwidthINTEL"
    5829 -> showString "MaxPrivateCopiesINTEL"
    5830 -> showString "SinglepumpINTEL"
    5831 -> showString "DoublepumpINTEL"
    5832 -> showString "MaxReplicatesINTEL"
    5833 -> showString "SimpleDualPortINTEL"
    5834 -> showString "MergeINTEL"
    5835 -> showString "BankBitsINTEL"
    5836 -> showString "ForcePow2DepthINTEL"
    5883 -> showString "StridesizeINTEL"
    5884 -> showString "WordsizeINTEL"
    5885 -> showString "TrueDualPortINTEL"
    5899 -> showString "BurstCoalesceINTEL"
    5900 -> showString "CacheSizeINTEL"
    5901 -> showString "DontStaticallyCoalesceINTEL"
    5902 -> showString "PrefetchINTEL"
    5905 -> showString "StallEnableINTEL"
    5907 -> showString "FuseLoopsInFunctionINTEL"
    5909 -> showString "MathOpDSPModeINTEL"
    5914 -> showString "AliasScopeINTEL"
    5915 -> showString "NoAliasINTEL"
    5917 -> showString "InitiationIntervalINTEL"
    5918 -> showString "MaxConcurrencyINTEL"
    5919 -> showString "PipelineEnableINTEL"
    5921 -> showString "BufferLocationINTEL"
    5944 -> showString "IOPipeStorageINTEL"
    6080 -> showString "FunctionFloatingPointModeINTEL"
    6085 -> showString "SingleElementVectorINTEL"
    6087 -> showString "VectorComputeCallableFunctionINTEL"
    6140 -> showString "MediaBlockIOINTEL"
    6151 -> showString "StallFreeINTEL"
    6170 -> showString "FPMaxErrorDecorationINTEL"
    6172 -> showString "LatencyControlLabelINTEL"
    6173 -> showString "LatencyControlConstraintINTEL"
    6175 -> showString "ConduitKernelArgumentINTEL"
    6176 -> showString "RegisterMapKernelArgumentINTEL"
    6177 -> showString "MMHostInterfaceAddressWidthINTEL"
    6178 -> showString "MMHostInterfaceDataWidthINTEL"
    6179 -> showString "MMHostInterfaceLatencyINTEL"
    6180 -> showString "MMHostInterfaceReadWriteModeINTEL"
    6181 -> showString "MMHostInterfaceMaxBurstINTEL"
    6182 -> showString "MMHostInterfaceWaitRequestINTEL"
    6183 -> showString "StableKernelArgumentINTEL"
    6188 -> showString "HostAccessINTEL"
    6190 -> showString "InitModeINTEL"
    6191 -> showString "ImplementInRegisterMapINTEL"
    6442 -> showString "CacheControlLoadINTEL"
    6443 -> showString "CacheControlStoreINTEL"
    x -> showParen (p > 10) $ showString "Decoration " . showsPrec (p + 1) x

pattern RelaxedPrecision :: Decoration
pattern RelaxedPrecision = Decoration 0

pattern SpecId :: Decoration
pattern SpecId = Decoration 1

pattern Block :: Decoration
pattern Block = Decoration 2

pattern BufferBlock :: Decoration
pattern BufferBlock = Decoration 3

pattern RowMajor :: Decoration
pattern RowMajor = Decoration 4

pattern ColMajor :: Decoration
pattern ColMajor = Decoration 5

pattern ArrayStride :: Decoration
pattern ArrayStride = Decoration 6

pattern MatrixStride :: Decoration
pattern MatrixStride = Decoration 7

pattern GLSLShared :: Decoration
pattern GLSLShared = Decoration 8

pattern GLSLPacked :: Decoration
pattern GLSLPacked = Decoration 9

pattern CPacked :: Decoration
pattern CPacked = Decoration 10

pattern BuiltIn :: Decoration
pattern BuiltIn = Decoration 11

pattern NoPerspective :: Decoration
pattern NoPerspective = Decoration 13

pattern Flat :: Decoration
pattern Flat = Decoration 14

pattern Patch :: Decoration
pattern Patch = Decoration 15

pattern Centroid :: Decoration
pattern Centroid = Decoration 16

pattern Sample :: Decoration
pattern Sample = Decoration 17

pattern Invariant :: Decoration
pattern Invariant = Decoration 18

pattern Restrict :: Decoration
pattern Restrict = Decoration 19

pattern Aliased :: Decoration
pattern Aliased = Decoration 20

pattern Volatile :: Decoration
pattern Volatile = Decoration 21

pattern Constant :: Decoration
pattern Constant = Decoration 22

pattern Coherent :: Decoration
pattern Coherent = Decoration 23

pattern NonWritable :: Decoration
pattern NonWritable = Decoration 24

pattern NonReadable :: Decoration
pattern NonReadable = Decoration 25

pattern Uniform :: Decoration
pattern Uniform = Decoration 26

pattern UniformId :: Decoration
pattern UniformId = Decoration 27

pattern SaturatedConversion :: Decoration
pattern SaturatedConversion = Decoration 28

pattern Stream :: Decoration
pattern Stream = Decoration 29

pattern Location :: Decoration
pattern Location = Decoration 30

pattern Component :: Decoration
pattern Component = Decoration 31

pattern Index :: Decoration
pattern Index = Decoration 32

pattern Binding :: Decoration
pattern Binding = Decoration 33

pattern DescriptorSet :: Decoration
pattern DescriptorSet = Decoration 34

pattern Offset :: Decoration
pattern Offset = Decoration 35

pattern XfbBuffer :: Decoration
pattern XfbBuffer = Decoration 36

pattern XfbStride :: Decoration
pattern XfbStride = Decoration 37

pattern FuncParamAttr :: Decoration
pattern FuncParamAttr = Decoration 38

pattern FPRoundingMode :: Decoration
pattern FPRoundingMode = Decoration 39

pattern FPFastMathMode :: Decoration
pattern FPFastMathMode = Decoration 40

pattern LinkageAttributes :: Decoration
pattern LinkageAttributes = Decoration 41

pattern NoContraction :: Decoration
pattern NoContraction = Decoration 42

pattern InputAttachmentIndex :: Decoration
pattern InputAttachmentIndex = Decoration 43

pattern Alignment :: Decoration
pattern Alignment = Decoration 44

pattern MaxByteOffset :: Decoration
pattern MaxByteOffset = Decoration 45

pattern AlignmentId :: Decoration
pattern AlignmentId = Decoration 46

pattern MaxByteOffsetId :: Decoration
pattern MaxByteOffsetId = Decoration 47

pattern NoSignedWrap :: Decoration
pattern NoSignedWrap = Decoration 4469

pattern NoUnsignedWrap :: Decoration
pattern NoUnsignedWrap = Decoration 4470

pattern WeightTextureQCOM :: Decoration
pattern WeightTextureQCOM = Decoration 4487

pattern BlockMatchTextureQCOM :: Decoration
pattern BlockMatchTextureQCOM = Decoration 4488

pattern BlockMatchSamplerQCOM :: Decoration
pattern BlockMatchSamplerQCOM = Decoration 4499

pattern ExplicitInterpAMD :: Decoration
pattern ExplicitInterpAMD = Decoration 4999

pattern NodeSharesPayloadLimitsWithAMDX :: Decoration
pattern NodeSharesPayloadLimitsWithAMDX = Decoration 5019

pattern NodeMaxPayloadsAMDX :: Decoration
pattern NodeMaxPayloadsAMDX = Decoration 5020

pattern TrackFinishWritingAMDX :: Decoration
pattern TrackFinishWritingAMDX = Decoration 5078

pattern PayloadNodeNameAMDX :: Decoration
pattern PayloadNodeNameAMDX = Decoration 5091

pattern OverrideCoverageNV :: Decoration
pattern OverrideCoverageNV = Decoration 5248

pattern PassthroughNV :: Decoration
pattern PassthroughNV = Decoration 5250

pattern ViewportRelativeNV :: Decoration
pattern ViewportRelativeNV = Decoration 5252

pattern SecondaryViewportRelativeNV :: Decoration
pattern SecondaryViewportRelativeNV = Decoration 5256

pattern PerPrimitiveEXT :: Decoration
pattern PerPrimitiveEXT = Decoration 5271

pattern PerPrimitiveNV :: Decoration
pattern PerPrimitiveNV = Decoration 5271

pattern PerViewNV :: Decoration
pattern PerViewNV = Decoration 5272

pattern PerTaskNV :: Decoration
pattern PerTaskNV = Decoration 5273

pattern PerVertexKHR :: Decoration
pattern PerVertexKHR = Decoration 5285

pattern PerVertexNV :: Decoration
pattern PerVertexNV = Decoration 5285

pattern NonUniform :: Decoration
pattern NonUniform = Decoration 5300

pattern NonUniformEXT :: Decoration
pattern NonUniformEXT = Decoration 5300

pattern RestrictPointer :: Decoration
pattern RestrictPointer = Decoration 5355

pattern RestrictPointerEXT :: Decoration
pattern RestrictPointerEXT = Decoration 5355

pattern AliasedPointer :: Decoration
pattern AliasedPointer = Decoration 5356

pattern AliasedPointerEXT :: Decoration
pattern AliasedPointerEXT = Decoration 5356

pattern HitObjectShaderRecordBufferNV :: Decoration
pattern HitObjectShaderRecordBufferNV = Decoration 5386

pattern BindlessSamplerNV :: Decoration
pattern BindlessSamplerNV = Decoration 5398

pattern BindlessImageNV :: Decoration
pattern BindlessImageNV = Decoration 5399

pattern BoundSamplerNV :: Decoration
pattern BoundSamplerNV = Decoration 5400

pattern BoundImageNV :: Decoration
pattern BoundImageNV = Decoration 5401

pattern SIMTCallINTEL :: Decoration
pattern SIMTCallINTEL = Decoration 5599

pattern ReferencedIndirectlyINTEL :: Decoration
pattern ReferencedIndirectlyINTEL = Decoration 5602

pattern ClobberINTEL :: Decoration
pattern ClobberINTEL = Decoration 5607

pattern SideEffectsINTEL :: Decoration
pattern SideEffectsINTEL = Decoration 5608

pattern VectorComputeVariableINTEL :: Decoration
pattern VectorComputeVariableINTEL = Decoration 5624

pattern FuncParamIOKindINTEL :: Decoration
pattern FuncParamIOKindINTEL = Decoration 5625

pattern VectorComputeFunctionINTEL :: Decoration
pattern VectorComputeFunctionINTEL = Decoration 5626

pattern StackCallINTEL :: Decoration
pattern StackCallINTEL = Decoration 5627

pattern GlobalVariableOffsetINTEL :: Decoration
pattern GlobalVariableOffsetINTEL = Decoration 5628

pattern CounterBuffer :: Decoration
pattern CounterBuffer = Decoration 5634

pattern HlslCounterBufferGOOGLE :: Decoration
pattern HlslCounterBufferGOOGLE = Decoration 5634

pattern HlslSemanticGOOGLE :: Decoration
pattern HlslSemanticGOOGLE = Decoration 5635

pattern UserSemantic :: Decoration
pattern UserSemantic = Decoration 5635

pattern UserTypeGOOGLE :: Decoration
pattern UserTypeGOOGLE = Decoration 5636

pattern FunctionRoundingModeINTEL :: Decoration
pattern FunctionRoundingModeINTEL = Decoration 5822

pattern FunctionDenormModeINTEL :: Decoration
pattern FunctionDenormModeINTEL = Decoration 5823

pattern RegisterINTEL :: Decoration
pattern RegisterINTEL = Decoration 5825

pattern MemoryINTEL :: Decoration
pattern MemoryINTEL = Decoration 5826

pattern NumbanksINTEL :: Decoration
pattern NumbanksINTEL = Decoration 5827

pattern BankwidthINTEL :: Decoration
pattern BankwidthINTEL = Decoration 5828

pattern MaxPrivateCopiesINTEL :: Decoration
pattern MaxPrivateCopiesINTEL = Decoration 5829

pattern SinglepumpINTEL :: Decoration
pattern SinglepumpINTEL = Decoration 5830

pattern DoublepumpINTEL :: Decoration
pattern DoublepumpINTEL = Decoration 5831

pattern MaxReplicatesINTEL :: Decoration
pattern MaxReplicatesINTEL = Decoration 5832

pattern SimpleDualPortINTEL :: Decoration
pattern SimpleDualPortINTEL = Decoration 5833

pattern MergeINTEL :: Decoration
pattern MergeINTEL = Decoration 5834

pattern BankBitsINTEL :: Decoration
pattern BankBitsINTEL = Decoration 5835

pattern ForcePow2DepthINTEL :: Decoration
pattern ForcePow2DepthINTEL = Decoration 5836

pattern StridesizeINTEL :: Decoration
pattern StridesizeINTEL = Decoration 5883

pattern WordsizeINTEL :: Decoration
pattern WordsizeINTEL = Decoration 5884

pattern TrueDualPortINTEL :: Decoration
pattern TrueDualPortINTEL = Decoration 5885

pattern BurstCoalesceINTEL :: Decoration
pattern BurstCoalesceINTEL = Decoration 5899

pattern CacheSizeINTEL :: Decoration
pattern CacheSizeINTEL = Decoration 5900

pattern DontStaticallyCoalesceINTEL :: Decoration
pattern DontStaticallyCoalesceINTEL = Decoration 5901

pattern PrefetchINTEL :: Decoration
pattern PrefetchINTEL = Decoration 5902

pattern StallEnableINTEL :: Decoration
pattern StallEnableINTEL = Decoration 5905

pattern FuseLoopsInFunctionINTEL :: Decoration
pattern FuseLoopsInFunctionINTEL = Decoration 5907

pattern MathOpDSPModeINTEL :: Decoration
pattern MathOpDSPModeINTEL = Decoration 5909

pattern AliasScopeINTEL :: Decoration
pattern AliasScopeINTEL = Decoration 5914

pattern NoAliasINTEL :: Decoration
pattern NoAliasINTEL = Decoration 5915

pattern InitiationIntervalINTEL :: Decoration
pattern InitiationIntervalINTEL = Decoration 5917

pattern MaxConcurrencyINTEL :: Decoration
pattern MaxConcurrencyINTEL = Decoration 5918

pattern PipelineEnableINTEL :: Decoration
pattern PipelineEnableINTEL = Decoration 5919

pattern BufferLocationINTEL :: Decoration
pattern BufferLocationINTEL = Decoration 5921

pattern IOPipeStorageINTEL :: Decoration
pattern IOPipeStorageINTEL = Decoration 5944

pattern FunctionFloatingPointModeINTEL :: Decoration
pattern FunctionFloatingPointModeINTEL = Decoration 6080

pattern SingleElementVectorINTEL :: Decoration
pattern SingleElementVectorINTEL = Decoration 6085

pattern VectorComputeCallableFunctionINTEL :: Decoration
pattern VectorComputeCallableFunctionINTEL = Decoration 6087

pattern MediaBlockIOINTEL :: Decoration
pattern MediaBlockIOINTEL = Decoration 6140

pattern StallFreeINTEL :: Decoration
pattern StallFreeINTEL = Decoration 6151

pattern FPMaxErrorDecorationINTEL :: Decoration
pattern FPMaxErrorDecorationINTEL = Decoration 6170

pattern LatencyControlLabelINTEL :: Decoration
pattern LatencyControlLabelINTEL = Decoration 6172

pattern LatencyControlConstraintINTEL :: Decoration
pattern LatencyControlConstraintINTEL = Decoration 6173

pattern ConduitKernelArgumentINTEL :: Decoration
pattern ConduitKernelArgumentINTEL = Decoration 6175

pattern RegisterMapKernelArgumentINTEL :: Decoration
pattern RegisterMapKernelArgumentINTEL = Decoration 6176

pattern MMHostInterfaceAddressWidthINTEL :: Decoration
pattern MMHostInterfaceAddressWidthINTEL = Decoration 6177

pattern MMHostInterfaceDataWidthINTEL :: Decoration
pattern MMHostInterfaceDataWidthINTEL = Decoration 6178

pattern MMHostInterfaceLatencyINTEL :: Decoration
pattern MMHostInterfaceLatencyINTEL = Decoration 6179

pattern MMHostInterfaceReadWriteModeINTEL :: Decoration
pattern MMHostInterfaceReadWriteModeINTEL = Decoration 6180

pattern MMHostInterfaceMaxBurstINTEL :: Decoration
pattern MMHostInterfaceMaxBurstINTEL = Decoration 6181

pattern MMHostInterfaceWaitRequestINTEL :: Decoration
pattern MMHostInterfaceWaitRequestINTEL = Decoration 6182

pattern StableKernelArgumentINTEL :: Decoration
pattern StableKernelArgumentINTEL = Decoration 6183

pattern HostAccessINTEL :: Decoration
pattern HostAccessINTEL = Decoration 6188

pattern InitModeINTEL :: Decoration
pattern InitModeINTEL = Decoration 6190

pattern ImplementInRegisterMapINTEL :: Decoration
pattern ImplementInRegisterMapINTEL = Decoration 6191

pattern CacheControlLoadINTEL :: Decoration
pattern CacheControlLoadINTEL = Decoration 6442

pattern CacheControlStoreINTEL :: Decoration
pattern CacheControlStoreINTEL = Decoration 6443