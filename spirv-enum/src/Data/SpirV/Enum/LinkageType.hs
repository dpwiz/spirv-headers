{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.LinkageType where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype LinkageType = LinkageType Int32
  deriving newtype (Eq, Ord, Storable)

instance Show LinkageType where
  showsPrec p (LinkageType v) = case v of
    0 -> showString "Export"
    1 -> showString "Import"
    2 -> showString "LinkOnceODR"
    x -> showParen (p > 10) $ showString "LinkageType " . showsPrec (p + 1) x

pattern Export :: LinkageType
pattern Export = LinkageType 0

pattern Import :: LinkageType
pattern Import = LinkageType 1

pattern LinkOnceODR :: LinkageType
pattern LinkOnceODR = LinkageType 2