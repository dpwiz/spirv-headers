{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FPDenormMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype FPDenormMode = FPDenormMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show FPDenormMode where
  showsPrec p (FPDenormMode v) = case v of
    0 -> showString "Preserve"
    1 -> showString "FlushToZero"
    x -> showParen (p > 10) $ showString "FPDenormMode " . showsPrec (p + 1) x

pattern Preserve :: FPDenormMode
pattern Preserve = FPDenormMode 0

pattern FlushToZero :: FPDenormMode
pattern FlushToZero = FPDenormMode 1