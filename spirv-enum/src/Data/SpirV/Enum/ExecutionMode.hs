{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.ExecutionMode where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype ExecutionMode = ExecutionMode Int32
  deriving newtype (Eq, Ord, Storable)

instance Show ExecutionMode where
  showsPrec p (ExecutionMode v) = case v of
    0 -> showString "Invocations"
    1 -> showString "SpacingEqual"
    2 -> showString "SpacingFractionalEven"
    3 -> showString "SpacingFractionalOdd"
    4 -> showString "VertexOrderCw"
    5 -> showString "VertexOrderCcw"
    6 -> showString "PixelCenterInteger"
    7 -> showString "OriginUpperLeft"
    8 -> showString "OriginLowerLeft"
    9 -> showString "EarlyFragmentTests"
    10 -> showString "PointMode"
    11 -> showString "Xfb"
    12 -> showString "DepthReplacing"
    14 -> showString "DepthGreater"
    15 -> showString "DepthLess"
    16 -> showString "DepthUnchanged"
    17 -> showString "LocalSize"
    18 -> showString "LocalSizeHint"
    19 -> showString "InputPoints"
    20 -> showString "InputLines"
    21 -> showString "InputLinesAdjacency"
    22 -> showString "Triangles"
    23 -> showString "InputTrianglesAdjacency"
    24 -> showString "Quads"
    25 -> showString "Isolines"
    26 -> showString "OutputVertices"
    27 -> showString "OutputPoints"
    28 -> showString "OutputLineStrip"
    29 -> showString "OutputTriangleStrip"
    30 -> showString "VecTypeHint"
    31 -> showString "ContractionOff"
    33 -> showString "Initializer"
    34 -> showString "Finalizer"
    35 -> showString "SubgroupSize"
    36 -> showString "SubgroupsPerWorkgroup"
    37 -> showString "SubgroupsPerWorkgroupId"
    38 -> showString "LocalSizeId"
    39 -> showString "LocalSizeHintId"
    4169 -> showString "NonCoherentColorAttachmentReadEXT"
    4170 -> showString "NonCoherentDepthAttachmentReadEXT"
    4171 -> showString "NonCoherentStencilAttachmentReadEXT"
    4421 -> showString "SubgroupUniformControlFlowKHR"
    4446 -> showString "PostDepthCoverage"
    4459 -> showString "DenormPreserve"
    4460 -> showString "DenormFlushToZero"
    4461 -> showString "SignedZeroInfNanPreserve"
    4462 -> showString "RoundingModeRTE"
    4463 -> showString "RoundingModeRTZ"
    5017 -> showString "EarlyAndLateFragmentTestsAMD"
    5027 -> showString "StencilRefReplacingEXT"
    5069 -> showString "CoalescingAMDX"
    5071 -> showString "MaxNodeRecursionAMDX"
    5072 -> showString "StaticNumWorkgroupsAMDX"
    5073 -> showString "ShaderIndexAMDX"
    5077 -> showString "MaxNumWorkgroupsAMDX"
    5079 -> showString "StencilRefUnchangedFrontAMD"
    5080 -> showString "StencilRefGreaterFrontAMD"
    5081 -> showString "StencilRefLessFrontAMD"
    5082 -> showString "StencilRefUnchangedBackAMD"
    5083 -> showString "StencilRefGreaterBackAMD"
    5084 -> showString "StencilRefLessBackAMD"
    5088 -> showString "QuadDerivativesKHR"
    5089 -> showString "RequireFullQuadsKHR"
    5269 -> showString "OutputLinesEXT"
    5270 -> showString "OutputPrimitivesEXT"
    5289 -> showString "DerivativeGroupQuadsNV"
    5290 -> showString "DerivativeGroupLinearNV"
    5298 -> showString "OutputTrianglesEXT"
    5366 -> showString "PixelInterlockOrderedEXT"
    5367 -> showString "PixelInterlockUnorderedEXT"
    5368 -> showString "SampleInterlockOrderedEXT"
    5369 -> showString "SampleInterlockUnorderedEXT"
    5370 -> showString "ShadingRateInterlockOrderedEXT"
    5371 -> showString "ShadingRateInterlockUnorderedEXT"
    5618 -> showString "SharedLocalMemorySizeINTEL"
    5620 -> showString "RoundingModeRTPINTEL"
    5621 -> showString "RoundingModeRTNINTEL"
    5622 -> showString "FloatingPointModeALTINTEL"
    5623 -> showString "FloatingPointModeIEEEINTEL"
    5893 -> showString "MaxWorkgroupSizeINTEL"
    5894 -> showString "MaxWorkDimINTEL"
    5895 -> showString "NoGlobalOffsetINTEL"
    5896 -> showString "NumSIMDWorkitemsINTEL"
    5903 -> showString "SchedulerTargetFmaxMhzINTEL"
    6023 -> showString "MaximallyReconvergesKHR"
    6028 -> showString "FPFastMathDefault"
    6154 -> showString "StreamingInterfaceINTEL"
    6160 -> showString "RegisterMapInterfaceINTEL"
    6417 -> showString "NamedBarrierCountINTEL"
    6461 -> showString "MaximumRegistersINTEL"
    6462 -> showString "MaximumRegistersIdINTEL"
    6463 -> showString "NamedMaximumRegistersINTEL"
    x -> showParen (p > 10) $ showString "ExecutionMode " . showsPrec (p + 1) x

pattern Invocations :: ExecutionMode
pattern Invocations = ExecutionMode 0

pattern SpacingEqual :: ExecutionMode
pattern SpacingEqual = ExecutionMode 1

pattern SpacingFractionalEven :: ExecutionMode
pattern SpacingFractionalEven = ExecutionMode 2

pattern SpacingFractionalOdd :: ExecutionMode
pattern SpacingFractionalOdd = ExecutionMode 3

pattern VertexOrderCw :: ExecutionMode
pattern VertexOrderCw = ExecutionMode 4

pattern VertexOrderCcw :: ExecutionMode
pattern VertexOrderCcw = ExecutionMode 5

pattern PixelCenterInteger :: ExecutionMode
pattern PixelCenterInteger = ExecutionMode 6

pattern OriginUpperLeft :: ExecutionMode
pattern OriginUpperLeft = ExecutionMode 7

pattern OriginLowerLeft :: ExecutionMode
pattern OriginLowerLeft = ExecutionMode 8

pattern EarlyFragmentTests :: ExecutionMode
pattern EarlyFragmentTests = ExecutionMode 9

pattern PointMode :: ExecutionMode
pattern PointMode = ExecutionMode 10

pattern Xfb :: ExecutionMode
pattern Xfb = ExecutionMode 11

pattern DepthReplacing :: ExecutionMode
pattern DepthReplacing = ExecutionMode 12

pattern DepthGreater :: ExecutionMode
pattern DepthGreater = ExecutionMode 14

pattern DepthLess :: ExecutionMode
pattern DepthLess = ExecutionMode 15

pattern DepthUnchanged :: ExecutionMode
pattern DepthUnchanged = ExecutionMode 16

pattern LocalSize :: ExecutionMode
pattern LocalSize = ExecutionMode 17

pattern LocalSizeHint :: ExecutionMode
pattern LocalSizeHint = ExecutionMode 18

pattern InputPoints :: ExecutionMode
pattern InputPoints = ExecutionMode 19

pattern InputLines :: ExecutionMode
pattern InputLines = ExecutionMode 20

pattern InputLinesAdjacency :: ExecutionMode
pattern InputLinesAdjacency = ExecutionMode 21

pattern Triangles :: ExecutionMode
pattern Triangles = ExecutionMode 22

pattern InputTrianglesAdjacency :: ExecutionMode
pattern InputTrianglesAdjacency = ExecutionMode 23

pattern Quads :: ExecutionMode
pattern Quads = ExecutionMode 24

pattern Isolines :: ExecutionMode
pattern Isolines = ExecutionMode 25

pattern OutputVertices :: ExecutionMode
pattern OutputVertices = ExecutionMode 26

pattern OutputPoints :: ExecutionMode
pattern OutputPoints = ExecutionMode 27

pattern OutputLineStrip :: ExecutionMode
pattern OutputLineStrip = ExecutionMode 28

pattern OutputTriangleStrip :: ExecutionMode
pattern OutputTriangleStrip = ExecutionMode 29

pattern VecTypeHint :: ExecutionMode
pattern VecTypeHint = ExecutionMode 30

pattern ContractionOff :: ExecutionMode
pattern ContractionOff = ExecutionMode 31

pattern Initializer :: ExecutionMode
pattern Initializer = ExecutionMode 33

pattern Finalizer :: ExecutionMode
pattern Finalizer = ExecutionMode 34

pattern SubgroupSize :: ExecutionMode
pattern SubgroupSize = ExecutionMode 35

pattern SubgroupsPerWorkgroup :: ExecutionMode
pattern SubgroupsPerWorkgroup = ExecutionMode 36

pattern SubgroupsPerWorkgroupId :: ExecutionMode
pattern SubgroupsPerWorkgroupId = ExecutionMode 37

pattern LocalSizeId :: ExecutionMode
pattern LocalSizeId = ExecutionMode 38

pattern LocalSizeHintId :: ExecutionMode
pattern LocalSizeHintId = ExecutionMode 39

pattern NonCoherentColorAttachmentReadEXT :: ExecutionMode
pattern NonCoherentColorAttachmentReadEXT = ExecutionMode 4169

pattern NonCoherentDepthAttachmentReadEXT :: ExecutionMode
pattern NonCoherentDepthAttachmentReadEXT = ExecutionMode 4170

pattern NonCoherentStencilAttachmentReadEXT :: ExecutionMode
pattern NonCoherentStencilAttachmentReadEXT = ExecutionMode 4171

pattern SubgroupUniformControlFlowKHR :: ExecutionMode
pattern SubgroupUniformControlFlowKHR = ExecutionMode 4421

pattern PostDepthCoverage :: ExecutionMode
pattern PostDepthCoverage = ExecutionMode 4446

pattern DenormPreserve :: ExecutionMode
pattern DenormPreserve = ExecutionMode 4459

pattern DenormFlushToZero :: ExecutionMode
pattern DenormFlushToZero = ExecutionMode 4460

pattern SignedZeroInfNanPreserve :: ExecutionMode
pattern SignedZeroInfNanPreserve = ExecutionMode 4461

pattern RoundingModeRTE :: ExecutionMode
pattern RoundingModeRTE = ExecutionMode 4462

pattern RoundingModeRTZ :: ExecutionMode
pattern RoundingModeRTZ = ExecutionMode 4463

pattern EarlyAndLateFragmentTestsAMD :: ExecutionMode
pattern EarlyAndLateFragmentTestsAMD = ExecutionMode 5017

pattern StencilRefReplacingEXT :: ExecutionMode
pattern StencilRefReplacingEXT = ExecutionMode 5027

pattern CoalescingAMDX :: ExecutionMode
pattern CoalescingAMDX = ExecutionMode 5069

pattern MaxNodeRecursionAMDX :: ExecutionMode
pattern MaxNodeRecursionAMDX = ExecutionMode 5071

pattern StaticNumWorkgroupsAMDX :: ExecutionMode
pattern StaticNumWorkgroupsAMDX = ExecutionMode 5072

pattern ShaderIndexAMDX :: ExecutionMode
pattern ShaderIndexAMDX = ExecutionMode 5073

pattern MaxNumWorkgroupsAMDX :: ExecutionMode
pattern MaxNumWorkgroupsAMDX = ExecutionMode 5077

pattern StencilRefUnchangedFrontAMD :: ExecutionMode
pattern StencilRefUnchangedFrontAMD = ExecutionMode 5079

pattern StencilRefGreaterFrontAMD :: ExecutionMode
pattern StencilRefGreaterFrontAMD = ExecutionMode 5080

pattern StencilRefLessFrontAMD :: ExecutionMode
pattern StencilRefLessFrontAMD = ExecutionMode 5081

pattern StencilRefUnchangedBackAMD :: ExecutionMode
pattern StencilRefUnchangedBackAMD = ExecutionMode 5082

pattern StencilRefGreaterBackAMD :: ExecutionMode
pattern StencilRefGreaterBackAMD = ExecutionMode 5083

pattern StencilRefLessBackAMD :: ExecutionMode
pattern StencilRefLessBackAMD = ExecutionMode 5084

pattern QuadDerivativesKHR :: ExecutionMode
pattern QuadDerivativesKHR = ExecutionMode 5088

pattern RequireFullQuadsKHR :: ExecutionMode
pattern RequireFullQuadsKHR = ExecutionMode 5089

pattern OutputLinesEXT :: ExecutionMode
pattern OutputLinesEXT = ExecutionMode 5269

pattern OutputLinesNV :: ExecutionMode
pattern OutputLinesNV = ExecutionMode 5269

pattern OutputPrimitivesEXT :: ExecutionMode
pattern OutputPrimitivesEXT = ExecutionMode 5270

pattern OutputPrimitivesNV :: ExecutionMode
pattern OutputPrimitivesNV = ExecutionMode 5270

pattern DerivativeGroupQuadsNV :: ExecutionMode
pattern DerivativeGroupQuadsNV = ExecutionMode 5289

pattern DerivativeGroupLinearNV :: ExecutionMode
pattern DerivativeGroupLinearNV = ExecutionMode 5290

pattern OutputTrianglesEXT :: ExecutionMode
pattern OutputTrianglesEXT = ExecutionMode 5298

pattern OutputTrianglesNV :: ExecutionMode
pattern OutputTrianglesNV = ExecutionMode 5298

pattern PixelInterlockOrderedEXT :: ExecutionMode
pattern PixelInterlockOrderedEXT = ExecutionMode 5366

pattern PixelInterlockUnorderedEXT :: ExecutionMode
pattern PixelInterlockUnorderedEXT = ExecutionMode 5367

pattern SampleInterlockOrderedEXT :: ExecutionMode
pattern SampleInterlockOrderedEXT = ExecutionMode 5368

pattern SampleInterlockUnorderedEXT :: ExecutionMode
pattern SampleInterlockUnorderedEXT = ExecutionMode 5369

pattern ShadingRateInterlockOrderedEXT :: ExecutionMode
pattern ShadingRateInterlockOrderedEXT = ExecutionMode 5370

pattern ShadingRateInterlockUnorderedEXT :: ExecutionMode
pattern ShadingRateInterlockUnorderedEXT = ExecutionMode 5371

pattern SharedLocalMemorySizeINTEL :: ExecutionMode
pattern SharedLocalMemorySizeINTEL = ExecutionMode 5618

pattern RoundingModeRTPINTEL :: ExecutionMode
pattern RoundingModeRTPINTEL = ExecutionMode 5620

pattern RoundingModeRTNINTEL :: ExecutionMode
pattern RoundingModeRTNINTEL = ExecutionMode 5621

pattern FloatingPointModeALTINTEL :: ExecutionMode
pattern FloatingPointModeALTINTEL = ExecutionMode 5622

pattern FloatingPointModeIEEEINTEL :: ExecutionMode
pattern FloatingPointModeIEEEINTEL = ExecutionMode 5623

pattern MaxWorkgroupSizeINTEL :: ExecutionMode
pattern MaxWorkgroupSizeINTEL = ExecutionMode 5893

pattern MaxWorkDimINTEL :: ExecutionMode
pattern MaxWorkDimINTEL = ExecutionMode 5894

pattern NoGlobalOffsetINTEL :: ExecutionMode
pattern NoGlobalOffsetINTEL = ExecutionMode 5895

pattern NumSIMDWorkitemsINTEL :: ExecutionMode
pattern NumSIMDWorkitemsINTEL = ExecutionMode 5896

pattern SchedulerTargetFmaxMhzINTEL :: ExecutionMode
pattern SchedulerTargetFmaxMhzINTEL = ExecutionMode 5903

pattern MaximallyReconvergesKHR :: ExecutionMode
pattern MaximallyReconvergesKHR = ExecutionMode 6023

pattern FPFastMathDefault :: ExecutionMode
pattern FPFastMathDefault = ExecutionMode 6028

pattern StreamingInterfaceINTEL :: ExecutionMode
pattern StreamingInterfaceINTEL = ExecutionMode 6154

pattern RegisterMapInterfaceINTEL :: ExecutionMode
pattern RegisterMapInterfaceINTEL = ExecutionMode 6160

pattern NamedBarrierCountINTEL :: ExecutionMode
pattern NamedBarrierCountINTEL = ExecutionMode 6417

pattern MaximumRegistersINTEL :: ExecutionMode
pattern MaximumRegistersINTEL = ExecutionMode 6461

pattern MaximumRegistersIdINTEL :: ExecutionMode
pattern MaximumRegistersIdINTEL = ExecutionMode 6462

pattern NamedMaximumRegistersINTEL :: ExecutionMode
pattern NamedMaximumRegistersINTEL = ExecutionMode 6463