{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.AddressingModel where

import Data.Int (Int32)
import Foreign.Storable (Storable)

newtype AddressingModel = AddressingModel Int32
  deriving newtype (Eq, Ord, Storable)

instance Show AddressingModel where
  showsPrec p (AddressingModel v) = case v of
    0 -> showString "Logical"
    1 -> showString "Physical32"
    2 -> showString "Physical64"
    5348 -> showString "PhysicalStorageBuffer64"
    x -> showParen (p > 10) $ showString "AddressingModel " . showsPrec (p + 1) x

pattern Logical :: AddressingModel
pattern Logical = AddressingModel 0

pattern Physical32 :: AddressingModel
pattern Physical32 = AddressingModel 1

pattern Physical64 :: AddressingModel
pattern Physical64 = AddressingModel 2

pattern PhysicalStorageBuffer64 :: AddressingModel
pattern PhysicalStorageBuffer64 = AddressingModel 5348

pattern PhysicalStorageBuffer64EXT :: AddressingModel
pattern PhysicalStorageBuffer64EXT = AddressingModel 5348