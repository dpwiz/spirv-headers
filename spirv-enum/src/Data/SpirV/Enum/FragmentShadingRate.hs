{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Data.SpirV.Enum.FragmentShadingRate where

import Data.Bits (Bits, FiniteBits, (.|.))
import Data.Word (Word32)
import Foreign.Storable (Storable)

type FragmentShadingRate = FragmentShadingRateBits

newtype FragmentShadingRateBits = FragmentShadingRateBits Word32
  deriving newtype (Eq, Ord, Storable, Bits, FiniteBits)

instance Semigroup FragmentShadingRate where
  (FragmentShadingRateBits a) <> (FragmentShadingRateBits b) = FragmentShadingRateBits (a .|. b)

instance Monoid FragmentShadingRate where
  mempty = FragmentShadingRateBits 0

pattern Vertical2Pixels :: FragmentShadingRateBits
pattern Vertical2Pixels = FragmentShadingRateBits 0x00000001

pattern Vertical4Pixels :: FragmentShadingRateBits
pattern Vertical4Pixels = FragmentShadingRateBits 0x00000002

pattern Horizontal2Pixels :: FragmentShadingRateBits
pattern Horizontal2Pixels = FragmentShadingRateBits 0x00000004

pattern Horizontal4Pixels :: FragmentShadingRateBits
pattern Horizontal4Pixels = FragmentShadingRateBits 0x00000008